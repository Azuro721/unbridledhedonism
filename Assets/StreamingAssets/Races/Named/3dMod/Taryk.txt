{
    "$id": 0,
    "$type": "0|Assets.Scripts.UI.SavedPerson, Scripts",
    "CustomPersonality": null,
    "CustomAppearance": {
        "$id": 1,
        "$type": "1|PartList, Scripts",
        "Race": "Human",
        "Height": 66.14074,
        "Weight": 116.471748,
        "HairColor": "auburn",
        "HairLength": "short",
        "HairStyle": "a crew cut",
        "EyeColor": "hazel",
        "ShoulderDescription": "wide",
        "BreastSize": 0,
        "HipDescription": "wide",
        "DickSize": 3.4045577,
        "BallSize": 3.34269357,
        "Tags": {
            "$id": 2,
            "$type": "2|System.Collections.Generic.Dictionary`2[[System.String, mscorlib],[System.String, mscorlib]], mscorlib",
            "comparer": {
                "$id": 3,
                "$type": "3|System.Collections.Generic.GenericEqualityComparer`1[[System.String, mscorlib]], mscorlib"
            },
            "$rlength": 0,
            "$rcontent": [
            ]
        }
    },
    "Person": {
        "$id": 4,
        "$type": "4|Person, Scripts",
        "PartList": {
            "$id": 5,
            "$type": 1,
            "Race": "Oni",
            "Height": 56.9378128,
            "Weight": 97.98324,
            "HairColor": "black",
            "HairLength": "mid-length",
            "HairStyle": "tufts",
            "EyeColor": "hazel",
            "ShoulderDescription": "wide",
            "BreastSize": 0,
            "HipDescription": "narrow",
            "DickSize": 2.68125,
            "BallSize": 1.14375007,
            "Tags": {
                "$id": 6,
                "$type": 2,
                "comparer": {
                    "$id": 7,
                    "$type": 3
                },
                "$rlength": 2,
                "$rcontent": [
                    {
                        "$k": "Horn Color",
                        "$v": "brown"
                    },
                    {
                        "$k": "Horn Number",
                        "$v": "two"
                    }
                ]
            }
        },
        "FirstName": "Taryk",
        "LastName": "Deer",
        "Gender": 0,
        "Personality": {
            "$id": 8,
            "$type": "5|Personality, Scripts",
            "Charisma": 0.2385542,
            "Strength": 0.748563468,
            "Voracity": 0.5726061,
            "SexDrive": 0.647566557,
            "PredWillingness": 0.7803772,
            "PreyWillingness": 0.3471331,
            "Promiscuity": 0.651805043,
            "Voraphilia": 0.807419658,
            "PredLoyalty": 0.4097111,
            "PreyDigestionInterest": 0.922297359,
            "Extroversion": 0.5100944,
            "OralVoreInterest": 0.5025874,
            "UnbirthInterest": 0.127886355,
            "CockVoreInterest": 1,
            "AnalVoreInterest": 0.155684516,
            "CheatOnPartner": 1,
            "CheatAcceptance": 1,
            "PreferredClothing": 0,
            "VorePreference": 1,
            "EndoDominator": false
        },
        "Romance": {
            "$id": 9,
            "$type": "6|Romance, Scripts",
            "RomanticDesperation": 0,
            "Dating": null,
            "Self": $iref:4,
            "Orientation": 7
        },
        "Needs": {
            "$id": 10,
            "$type": "7|Needs, Scripts",
            "Self": $iref:4,
            "_hunger": 0.12025208,
            "_cleanliness": 0.0722553656,
            "_energy": 0.358243257,
            "_horniness": 0.13875
        },
        "Relationships": {
            "$id": 11,
            "$type": "8|System.Collections.Generic.List`1[[Relationship, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "Position": {
            "$type": "9|Vec2, Scripts",
            "x": 0,
            "y": 0
        },
        "VoreController": {
            "$id": 12,
            "$type": "10|VoreController, Scripts",
            "Self": $iref:4,
            "StomachDigestsPrey": false,
            "WombAbsorbsPrey": false,
            "BallsAbsorbPrey": false,
            "BowelsDigestPrey": false,
            "TotalDigestions": 0,
            "AllVoredTargets": {
                "$id": 13,
                "$type": "11|System.Collections.Generic.List`1[[VoreProgress, Scripts]], mscorlib",
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "StomachTargets": {
                "$id": 14,
                "$type": 11,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "BowelTargets": {
                "$id": 15,
                "$type": 11,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "WombTargets": {
                "$id": 16,
                "$type": 11,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "BallsTargets": {
                "$id": 17,
                "$type": 11,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "GeneralVoreCapable": true,
            "OralVoreCapable": true,
            "CockVoreCapable": true,
            "UnbirthCapable": false,
            "AnalVoreCapable": true
        },
        "VoreTracking": {
            "$id": 18,
            "$type": "12|System.Collections.Generic.List`1[[VoreTrackingRecord, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "ActiveSex": null,
        "ClothingStatus": 0,
        "BeingEaten": false,
        "EatenDuringSex": false,
        "AI": {
            "$id": 19,
            "$type": "13|AI, Scripts",
            "Self": $iref:4,
            "WaitTurns": 0,
            "StripTurns": 0,
            "Tasks": {
                "$id": 20,
                "$type": "14|System.Collections.Generic.List`1[[Assets.Scripts.AI.IGoal, Scripts]], mscorlib",
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "LastInteractedWith": null
        },
        "Events": {
            "$id": 21,
            "$type": "15|System.Collections.Generic.List`1[[Record, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "LibrarySatisfaction": 0,
        "GymSatisfaction": 0,
        "Traits": {
            "$id": 22,
            "$type": "16|System.Collections.Generic.List`1[[Traits, Scripts]], mscorlib",
            "$rlength": 3,
            "$rcontent": [
                4,
                17,
                1
            ]
        },
        "Quirks": {
            "$id": 23,
            "$type": "17|System.Collections.Generic.List`1[[Quirks, Scripts]], mscorlib",
            "$rlength": 1,
            "$rcontent": [
                23
            ]
        },
        "PredatorTier": 0,
        "Team": 0,
        "Race": "Oni",
        "TurnsSinceOrgasm": 0,
        "Picture": "Taryk3dMod",
        "StreamingAction": 0,
        "StreamingTarget": null,
        "StreamingSelfAction": 0,
        "StreamedTurns": 0,
        "MiscStats": {
            "$id": 24,
            "$type": "18|Assets.Scripts.People.MiscStats, Scripts",
            "TurnAdded": 0,
            "TotalWeightGain": 0,
            "TotalWeightLoss": 0,
            "TimesSwallowedOtherStart": 0,
            "TimesUnbirthedOtherStart": 0,
            "TimesAnalVoredOtherStart": 0,
            "TimesCockVoredOtherStart": 0,
            "TimesBeenSwallowedStart": 0,
            "TimesBeenUnbirthedStart": 0,
            "TimesBeenAnalVoredStart": 0,
            "TimesBeenCockVoredStart": 0,
            "TimesSwallowedOther": 0,
            "TimesUnbirthedOther": 0,
            "TimesAnalVoredOther": 0,
            "TimesCockVoredOther": 0,
            "TimesBeenSwallowed": 0,
            "TimesBeenUnbirthed": 0,
            "TimesBeenAnalVored": 0,
            "TimesBeenCockVored": 0,
            "TimesDigestedOther": 0,
            "TimesBeenDigested": 0
        },
        "MyRoom": {
            "$type": 9,
            "x": 0,
            "y": 0
        },
        "Health": 1000,
        "Disposals": {
            "$id": 25,
            "$type": "19|System.Collections.Generic.List`1[[DisposalData, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "Label": "",
        "ClothesInTile": 0,
        "Gone": false,
        "VoreImmune": false,
        "DigestionImmune": false,
        "LastSex": null,
        "LastSexTurn": 0
    },
    "SavedVersion": "8H",
    "Tags": "3dMod",
    "FirstName": "Taryk",
    "LastName": "Deer",
    "Gender": 0,
    "Race": "Human",
    "Orientation": 7,
    "Personality": 0,
    "CanVore": true
}