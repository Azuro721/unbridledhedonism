{
    "$id": 0,
    "$type": "0|Assets.Scripts.UI.SavedPerson, Scripts",
    "CustomPersonality": null,
    "CustomAppearance": {
        "$id": 1,
        "$type": "1|PartList, Scripts",
        "Race": "Kitsune",
        "Height": 77.75222,
        "Weight": 173.047012,
        "HairColor": "silver",
        "HairLength": "shoulder length",
        "HairStyle": "french braided",
        "EyeColor": "white",
        "ShoulderDescription": "broad",
        "BreastSize": 5.34153175,
        "HipDescription": "narrow",
        "DickSize": 0,
        "BallSize": 0,
        "Tags": {
            "$id": 2,
            "$type": "2|System.Collections.Generic.Dictionary`2[[System.String, mscorlib],[System.String, mscorlib]], mscorlib",
            "comparer": {
                "$id": 3,
                "$type": "3|System.Collections.Generic.GenericEqualityComparer`1[[System.String, mscorlib]], mscorlib"
            },
            "$rlength": 1,
            "$rcontent": [
                {
                    "$k": "Tip Color",
                    "$v": "golden"
                }
            ]
        }
    },
    "Person": {
        "$id": 4,
        "$type": "4|Person, Scripts",
        "PartList": {
            "$id": 5,
            "$type": 1,
            "Race": "Kitsune",
            "Height": 67.46499,
            "Weight": 154.073181,
            "HairColor": "maroon",
            "HairLength": "long",
            "HairStyle": "straight",
            "EyeColor": "pink",
            "ShoulderDescription": "narrow",
            "BreastSize": 2.02100873,
            "HipDescription": "wide",
            "DickSize": 0,
            "BallSize": 0,
            "Tags": {
                "$id": 6,
                "$type": 2,
                "comparer": {
                    "$id": 7,
                    "$type": 3
                },
                "$rlength": 1,
                "$rcontent": [
                    {
                        "$k": "Tip Color",
                        "$v": "maroon"
                    }
                ]
            }
        },
        "FirstName": "Allie",
        "LastName": "Foxtrot",
        "Gender": 1,
        "ID": 0,
        "Personality": {
            "$id": 8,
            "$type": "5|Personality, Scripts",
            "Charisma": 0.757690966,
            "Strength": 0.3811551,
            "Voracity": 0.5576257,
            "SexDrive": 0.532882452,
            "PredWillingness": 0.300852746,
            "PreyWillingness": 0.7847734,
            "Promiscuity": 0.7290543,
            "Voraphilia": 0.7845166,
            "PredLoyalty": 0.401497483,
            "PreyDigestionInterest": 0.724012434,
            "Extroversion": 0.6534241,
            "Dominance": 0.239978641,
            "Kindness": 0.360987246,
            "OralVoreInterest": 0.907003343,
            "UnbirthInterest": 0.6821586,
            "CockVoreInterest": 0.397654,
            "AnalVoreInterest": 0.855105,
            "CheatOnPartner": 1,
            "CheatAcceptance": 1,
            "PreferredClothing": 0,
            "VorePreference": 0,
            "EndoDominator": false
        },
        "Romance": {
            "$id": 9,
            "$type": "6|Romance, Scripts",
            "RomanticDesperation": 0,
            "Dating": null,
            "Self": $iref:4,
            "Orientation": 7
        },
        "Needs": {
            "$id": 10,
            "$type": "7|Needs, Scripts",
            "Self": $iref:4,
            "_hunger": 0,
            "_cleanliness": 0,
            "_energy": 0,
            "_horniness": 0
        },
        "Magic": {
            "$id": 11,
            "$type": "8|Magic, Scripts",
            "Self": $iref:4,
            "_maxmana": 3,
            "_mana": 3,
            "_manaregen": 1,
            "_potency": 0.7,
            "_proficiency": 0.6,
            "_willpower": 0.2,
            "_duration_passdoor": 0,
            "_duration_small1": 0,
            "_duration_small2": 0,
            "_duration_big1": 0,
            "_duration_big2": 0,
            "CanCast": true,
            "SelfCastSuccess": false
        },
        "Relationships": {
            "$id": 12,
            "$type": "9|System.Collections.Generic.List`1[[Relationship, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "Position": {
            "$type": "10|Vec2, Scripts",
            "x": 0,
            "y": 0
        },
        "VoreController": {
            "$id": 13,
            "$type": "11|VoreController, Scripts",
            "Self": $iref:4,
            "StomachDigestsPrey": false,
            "WombAbsorbsPrey": false,
            "BallsAbsorbPrey": false,
            "BowelsDigestPrey": false,
            "TotalDigestions": 0,
            "AllVoredTargets": {
                "$id": 14,
                "$type": "12|System.Collections.Generic.List`1[[VoreProgress, Scripts]], mscorlib",
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "StomachTargets": {
                "$id": 15,
                "$type": 12,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "BowelTargets": {
                "$id": 16,
                "$type": 12,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "WombTargets": {
                "$id": 17,
                "$type": 12,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "BallsTargets": {
                "$id": 18,
                "$type": 12,
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "GeneralVoreCapable": true,
            "OralVoreCapable": true,
            "CockVoreCapable": true,
            "UnbirthCapable": true,
            "AnalVoreCapable": true,
            "VorePower": 0
        },
        "VoreTracking": {
            "$id": 19,
            "$type": "13|System.Collections.Generic.List`1[[VoreTrackingRecord, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "ActiveSex": null,
        "ClothingStatus": 0,
        "BeingEaten": false,
        "EatenDuringSex": false,
        "AI": {
            "$id": 20,
            "$type": "14|AI, Scripts",
            "Self": $iref:4,
            "WaitTurns": 0,
            "StripTurns": 0,
            "Tasks": {
                "$id": 21,
                "$type": "15|System.Collections.Generic.List`1[[Assets.Scripts.AI.IGoal, Scripts]], mscorlib",
                "$rlength": 0,
                "$rcontent": [
                ]
            },
            "LastInteractedWith": null
        },
        "Events": {
            "$id": 22,
            "$type": "16|System.Collections.Generic.List`1[[Record, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "LibrarySatisfaction": 0,
        "GymSatisfaction": 0,
        "Traits": {
            "$id": 23,
            "$type": "17|System.Collections.Generic.List`1[[Traits, Scripts]], mscorlib",
            "$rlength": 3,
            "$rcontent": [
                35,
                40,
                7
            ]
        },
        "Quirks": {
            "$id": 24,
            "$type": "18|System.Collections.Generic.List`1[[Quirks, Scripts]], mscorlib",
            "$rlength": 4,
            "$rcontent": [
                24,
                12,
                38,
                43
            ]
        },
        "PredatorTier": 0,
        "Team": 0,
        "Race": "Kitsune",
        "TurnsSinceOrgasm": 0,
        "Picture": "RM_Allie",
        "StreamingAction": 0,
        "StreamingTarget": null,
        "StreamingSelfAction": 0,
        "StreamedTurns": 0,
        "MiscStats": {
            "$id": 25,
            "$type": "19|Assets.Scripts.People.MiscStats, Scripts",
            "TurnAdded": 0,
            "TotalWeightGain": 0,
            "TotalWeightLoss": 0,
            "TimesSwallowedOtherStart": 0,
            "TimesUnbirthedOtherStart": 0,
            "TimesAnalVoredOtherStart": 0,
            "TimesCockVoredOtherStart": 0,
            "TimesBeenSwallowedStart": 0,
            "TimesBeenUnbirthedStart": 0,
            "TimesBeenAnalVoredStart": 0,
            "TimesBeenCockVoredStart": 0,
            "TimesSwallowedOther": 0,
            "TimesUnbirthedOther": 0,
            "TimesAnalVoredOther": 0,
            "TimesCockVoredOther": 0,
            "TimesBeenSwallowed": 0,
            "TimesBeenUnbirthed": 0,
            "TimesBeenAnalVored": 0,
            "TimesBeenCockVored": 0,
            "TimesDigestedOther": 0,
            "TimesBeenDigested": 0
        },
        "MyRoom": {
            "$type": 10,
            "x": 0,
            "y": 0
        },
        "Health": 1000,
        "Disposals": {
            "$id": 26,
            "$type": "20|System.Collections.Generic.List`1[[DisposalData, Scripts]], mscorlib",
            "$rlength": 0,
            "$rcontent": [
            ]
        },
        "Label": "",
        "ClothesInTile": 0,
        "Gone": false,
        "Stunned": false,
        "VoreImmune": false,
        "DigestionImmune": false,
        "AlwaysReform": false,
        "LastSex": null,
        "LastSexTurn": 0
    },
    "SavedVersion": "RowdyMod 3A",
    "Tags": "rm_fem",
    "FirstName": "Allie",
    "LastName": "Foxtrot",
    "Gender": 1,
    "Race": "Kitsune",
    "Orientation": 7,
    "Personality": 6,
    "CanVore": true
}