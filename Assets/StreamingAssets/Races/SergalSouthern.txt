Parent Race: Sergal
Selectable: True
Named Character: False
//Southern sergals are more desert-colored, and they have cream belly & leg fur rather than white
Hair Color, brown, red, brick, faded red, cream, beige, golden, orange, pink, white, gray, sandy, brown, brown
Height, 63, 73
Weight, .85, 1.15