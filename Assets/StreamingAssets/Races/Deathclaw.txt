Parent Race: Reptile
Selectable: True
Named Character: False

Eye Color, brown, brown, yellow, orange, yellow, red, orange, yellow

Custom, Scale Color, brown, brown, tan, beige, albino, beige, brown, tan, tan, beige, brown
Custom, Tongue Size, average, long, short
Custom, Horn Style, curved, straight, curved, upwards, curved