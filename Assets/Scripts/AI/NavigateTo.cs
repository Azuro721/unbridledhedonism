﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.AI
{
    class NavigateTo : IGoal
    {
        [OdinSerialize]
        Person Self;
        [OdinSerialize]
        Vec2 Destination;

        public NavigateTo(Person self, Vec2 destination)
        {
            Self = self;
            Destination = destination;
        }

        public GoalReturn ExecuteStep()
        {
            if (Self.Position == Destination)
                return GoalReturn.GoalAlreadyDone;
            if (Self.AI.TryMove(Destination))
            {
                if (Self.Position == Destination)
                    return GoalReturn.CompletedGoal;
                return GoalReturn.DidStep;
            }
            return GoalReturn.AbortGoal;
        }

        public string ReportGoal()
        {
            return $"Moving to {Destination.x}, {Destination.y}";
        }
    }
}
