﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HelperFunctions;

namespace Assets.Scripts.AI
{
    class HuntForPrey : IGoal
    {
        [OdinSerialize]
        Person Target;
        [OdinSerialize]
        readonly Person Self;
        [OdinSerialize]
        Vec2 Destination;
        [OdinSerialize]
        int TurnsFollowed;
        [OdinSerialize]
        readonly Dictionary<Person, bool> Evaluated;
        [OdinSerialize]
        readonly bool Endo;
        [OdinSerialize]
        int TimesChanged = 0;

        public HuntForPrey(Person self, bool endo)
        {
            Self = self;
            Evaluated = new Dictionary<Person, bool>();
            Endo = endo;
            DebugManager.Log("HuntForPrey : " + (endo ? "Endo" : "Digest"));
        }
          
        public GoalReturn ExecuteStep()
        {

            if (Target != null)
            {
                if (Target.BeingEaten || Target.Dead)
                    return GoalReturn.AbortGoal;
                if (Self.VoreController.CapableOfVore() == false)
                    return GoalReturn.AbortGoal;
                if (TurnsFollowed > 7)
                    return GoalReturn.AbortGoal;
                Destination = Target.Position;
                if (Self.Position == Destination)
                {
                    DebugManager.Log("HuntForPrey Engage");
                    Self.AI.EngageVoreWithTarget(Target, Endo);
                    return GoalReturn.GoalAlreadyDone;
                }
                TurnsFollowed++;
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;

                // if trymove did not work, 50% to cast passdoor and try again
                // chance is 100% if char has room invader
                if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.Magic.Duration_Passdoor == 0 && Rand.Next(1) + (Self.HasTrait(Traits.RoomInvader) ? 1 : 0) >= 1)
                {
                    SelfActionList.List[SelfActionType.CastPassdoor].OnDo(Self);
                    return GoalReturn.DidStep;
                }
            }
            else
            {
                var closePeople = HelperFunctions.GetPeopleWithinXSquares(Self, 3);
                closePeople = closePeople.Where(s => Evaluated.ContainsKey(s) == false).ToList();
                if (CanUseMagic(Self) && Self.Magic.Mana >= 1 && Self.HasTrait(Traits.RoomInvader)) // search additionally for people in their own bedrooms with RoomInvader
                    closePeople = closePeople.Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(s)).ToList();
                else // search only areas where Self can reach
                    closePeople = closePeople.Where(s => State.World.Zones[s.Position.x, s.Position.y].Accepts(Self)).ToList();

                if (closePeople.Any())
                {
                    foreach (Person target in closePeople)
                    {
                        float weight = 1;
                        if (Self.HasTrait(Traits.PredRespect) && target.VoreController.CapableOfVore())
                            weight *= .5f;
                        if (Self.HasTrait(Traits.PredHunter) && target.VoreController.CapableOfVore())
                            weight *= 2;
                        
                        if (target.Health < Constants.HealthMax || target.Needs.Energy >= 1 || target.StreamingSelfAction == SelfActionType.Rest || target.StreamingSelfAction == SelfActionType.Masturbate || target.ActiveSex != null)
                        {
                            if (Self.HasTrait(Traits.Opportunist))
                                weight *= 3f;
                            else
                                weight *= 1.5f;
                        }

                        if (target.ClothingStatus == ClothingStatus.Nude)
                            weight *= 1.5f;
                        if (target.ClothingStatus == ClothingStatus.Underwear)
                            weight *= 1.2f;

                        if (Self.GetRelationshipWith(target).Vendetta)
                            weight *= 10;
                        if (Endo)
                        {
                            if (Self.VoreController.TargetVoreImmune(target, DigestionAlias.CanEndo) == false && Rand.NextFloat(0.10f, 1) < weight * Self.AI.Desires.DesireToForciblyEndoTarget(target))
                            {
                                Target = target;
                                return ExecuteStep();
                            }
                        }
                        else
                        {
                            if (Self.HasTrait(Traits.DoesNotForce) || State.World.Settings.ForcedPredWillingness < Rand.NextFloat(0, 0.2f))
                            {
                                if (Self.VoreController.TargetVoreImmune(target, DigestionAlias.CanVore) == false && Rand.NextFloat(0.15f, .9f) < weight * Self.AI.Desires.DesireToVoreAndDigestTarget(target))
                                {
                                    Target = target;
                                    return ExecuteStep();
                                }
                            }
                            else
                            {
                                if (Self.VoreController.TargetVoreImmune(target, DigestionAlias.CanVore) == false && Rand.NextFloat(0.10f, .9f) < weight * Self.AI.Desires.DesireToForciblyEatAndDigestTarget(target))
                                {
                                    Target = target;
                                    return ExecuteStep();
                                }
                            }


                        }

                        Evaluated.Add(target, true);
                    }
                }

                if (Self.Position == Destination)
                {
                    if (2 + Rand.Next(4) <= TimesChanged)
                        return GoalReturn.AbortGoal;
                    if (Self.Needs.Hunger > .95f && Rand.Next(3) == 0 && Self.HasTrait(Traits.PrefersLivingPrey) == false)
                    {
                        return GoalReturn.AbortGoal;
                    }
                    TimesChanged++;
                    Destination = Self.AI.RandomSquare();
                }
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;
                int rand = Rand.Next(10);
                if (rand < 2)
                    Destination = Self.AI.RandomAccessibleSquareOfType(ObjectType.Shower);
                else if (rand < 5)
                    Destination = Self.AI.RandomAccessibleSquareOfType(ObjectType.Food);
                else
                    Destination = Self.AI.RandomSquare();
                if (Self.AI.TryMove(Destination))
                    return GoalReturn.DidStep;
            }
            return GoalReturn.AbortGoal;
        }

        public string ReportGoal()
        {
            if (Target == null)
                return "Looking for prey.";
            return $"Advancing on {Target.FirstName} with the intent to eat.";
        }
    }
}
