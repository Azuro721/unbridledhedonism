﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class Desires
{
    private Person Self;

    public Desires(Person self)
    {
        Self = self;
    }


    float CleanlinessScore(Person target)
    {
        if (target.Needs.Cleanliness < .3f)
            return 1;
        return 2 / (2 + (target.Needs.Cleanliness - .3f));
    }

    internal float InterestInOralVore(Person target) => Self.Personality.OralVoreInterest;
    internal float InterestInUnbirth(Person target) => Self.Personality.UnbirthInterest;
    internal float InterestInCockVore(Person target) => Self.Personality.CockVoreInterest;
    internal float InterestInAnalVore(Person target) => Self.Personality.AnalVoreInterest;
    internal float InterestInUnfriendly(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        if (Self.HasTrait(Traits.Jerk))
            return 1.5f * (.4f - rel.FriendshipLevel);
        return -rel.FriendshipLevel;
    }

    internal float InterestInFriendly(Person target) => .25f + (Self.Personality.Extroversion + Self.GetRelationshipWith(target).FriendshipLevel) / 2;
    internal float InterestInRomanticRelations(Person target) => Self.GetRelationshipWith(target).RomanticLevel;
    internal float InterestInRomanceNow(Person target) => Self.GetRelationshipWith(target).RomanticLevel + Self.Personality.Promiscuity / 3 + Self.Needs.Horniness / 2;

    float MiddleOverallAttachment(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        return (2 + rel.FriendshipLevel + rel.RomanticLevel + target.Personality.Charisma / 3) / 4;
    }

    /// <summary>
    /// Higher friendship/relationship will drive this value lower
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    float MiddleCloseAttachment(Person target)
    {
        var rel = Self.GetRelationshipWith(target);
        var val = (1 - rel.FriendshipLevel * .99f) * (1 - rel.RomanticLevel * .99f);
        val = Mathf.Lerp(val, 1, 1 - Self.Personality.PredLoyalty);
        val = Mathf.Clamp(val, 0.025f, 1.5f);
        return val;

    }

    float MiddleInterestInForcingVore(Person target) => MiddleCloseAttachment(target) * State.World.Settings.ForcedPredWillingness * State.World.Settings.ForcedPredWillingness * MiddleInterestInVore(target) * (Self.HasTrait(Traits.DoesNotForce) ? 0 : 1);

    float MiddleInterestInVore(Person target)
    {
        if (Self.HasTrait(Traits.RomanticPred) && Self.Romance.Dating != target && Self.GetRelationshipWith(target).RomanticLevel < .6f)
            return 0;
        if (Self.HasTrait(Traits.OrientationFeeding) && Self.Romance.DesiresGender(target.GenderType) == false)
            return 0;
        float mult = 1;
        if (Self.HasTrait(Traits.ConfusedAppetites) && Self.Needs.Horniness > .7f)
            mult = 1.5f;
        return Mathf.Pow(Self.Personality.PredWillingness, .75f) * CleanlinessScore(target) * (.75f + Self.Personality.Voraphilia / 4 + Self.Personality.Voracity / 4) * target.Boosts.AttractsPreds * mult;
    }

    float MiddleInterestInDigestingPrey(Person target) => Mathf.Pow(Self.Personality.PredWillingness, .75f) * (Self.Needs.Hunger + (1 - Self.Personality.Kindness)) * MiddleCloseAttachment(target) * (State.World.Settings.NursesActive ? 1.5f : .85f) * (Self.Personality.EndoDominator ? 0.35f : 1) * State.World.Settings.DigestionBias * (Self.Personality.VorePreference == VorePreference.Endosoma ? 0 : 1);
    float MiddleInterestInBeingVored(Person target) => (Self.Personality.PreyWillingness + Self.Personality.Voraphilia) / 2 * MiddleOverallAttachment(target);
    float MiddleInterestInBeingVoredEndo(Person target) => (Self.Personality.PreyWillingness + Self.Personality.Voraphilia) / 2 * MiddleOverallAttachment(target) * (Self.HasTrait(Traits.PrefersAction) ? 0.2f : 1);
    float MiddleInterestInBeingDigested(Person target) => Mathf.Pow(Self.Personality.PreyWillingness, .75f) * 2.5f * Mathf.Pow(Self.Personality.PreyDigestionInterest, 2) * (Self.HasTrait(Traits.PrefersAction) ? 1.75f : 1);

    internal float DesireToForciblyEatAndDigestTarget(Person target) => MiddleInterestInForcingVore(target) * MiddleInterestInDigestingPrey(target);
    internal float DesireToForciblyEndoTarget(Person target) => MiddleInterestInForcingVore(target);

    internal float DesireToVoreTarget(Person target)
    {
        return MiddleInterestInVore(target) * State.World.Settings.AskPredWillingness;
    }

    internal float DesireToVoreTargetWithoutPredWillingness(Person target)
    {
        return MiddleInterestInVore(target);
    }

    internal float DesireToVoreAndDigestTarget(Person target)
    {
        return MiddleInterestInVore(target) * MiddleInterestInDigestingPrey(target) * State.World.Settings.AskPredWillingness;
    }

    internal float DesireToEndoTarget(Person target)
    {
        if (Self.Personality.VorePreference == VorePreference.Digestion)
            return 0;
        return MiddleInterestInVore(target) * State.World.Settings.EndoBias * State.World.Settings.AskPredWillingness;
    }

    internal float DesireToDigestTarget(Person target)
    {
        return MiddleInterestInDigestingPrey(target);
    }

    internal float PreyTargetWeight(Person target)
    {
        if (Self.HasTrait(Traits.SelectivelyWilling) && (Self.GetRelationshipWith(target).FriendshipLevel > .7f || Self.GetRelationshipWith(target).RomanticLevel > .7f) == false)
            return 0;
        if (Self.HasTrait(Traits.NeverWilling))
            return 0;
        return 1;
    }

    internal float DesireToBeVored(Person target) => MiddleInterestInBeingVored(target) * MiddleInterestInBeingDigested(target) * PreyTargetWeight(target);

    internal float DesireToBeVoredEndo(Person target) => MiddleInterestInBeingVoredEndo(target) * PreyTargetWeight(target);

    internal float DesireToBeDigested(Person target) => MiddleInterestInBeingDigested(target) * PreyTargetWeight(target);

    internal float AskedDesireToReleaseEndoPrey(Person target)
    {
        if (Self.VoreController.TargetIsBeingDigested(target))
            return 0;
        if (Self.Personality.EndoDominator && State.World.Settings.EndoDominatorsHoldForever)
            return 0;
        var prog = Self.VoreController.GetProgressOf(target);
        if (prog == null)
            return 0;
        int weight = -State.World.Settings.EndoPreferredTurns / 2;
        if (Self.Personality.EndoDominator)
            weight *= 2;
        if (Self.HasTrait(Traits.Greedy))
            weight *= 5;

        weight /= 3;

        weight += prog.Stage;

        if (weight < 0)
            return 0;

        return (float)weight / State.World.Settings.EndoPreferredTurns;
    }

    internal bool DesiresToReleaseEndoPrey(Person target)
    {
        if (Self.Personality.EndoDominator && State.World.Settings.EndoDominatorsHoldForever)
            return false;
        if (Self.HasTrait(Traits.Greedy))
            return false;
        var prog = Self.VoreController.GetProgressOf(target);
        if (prog == null)
            return false;
        int weight = -State.World.Settings.EndoPreferredTurns;
        if (Self.Personality.EndoDominator)
            weight *= 2;
        
        weight += prog.Stage;


        weight += 3 - Rand.Next(6);

        return weight > 0;
    }

}
