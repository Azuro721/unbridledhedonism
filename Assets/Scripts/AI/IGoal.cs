﻿using Assets.Scripts.AI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.AI
{
    interface IGoal
    {
        GoalReturn ExecuteStep();

        string ReportGoal();
    }
}

