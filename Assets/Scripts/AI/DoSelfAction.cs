﻿using OdinSerializer;
using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.AI
{
    class DoSelfAction : IGoal
    {
        [OdinSerialize]
        SelfActionType Type;
        [OdinSerialize]
        Person Self;

        public DoSelfAction(Person self, SelfActionType type)
        {
            Type = type;
            Self = self;
            DebugManager.Log("DoSelfAction : " + type);
        }

        public GoalReturn ExecuteStep()
        {
            if (Type == SelfActionType.Masturbate && Self.ClothingStatus != ClothingStatus.Nude)
            {
                SelfActionList.List[SelfActionType.Strip].OnDo(Self);
                return GoalReturn.DidStep;
            }

            var selfAction = SelfActionList.List[Type];
            if (selfAction.AppearConditional(Self))
            {
                selfAction.OnDo(Self);
                return GoalReturn.CompletedGoal;
            }
            else
                return GoalReturn.AbortGoal;

        }

        public string ReportGoal()
        {
            var selfAction = SelfActionList.List[Type];
            if (selfAction.StreamingDescription.IsNullOrWhitespace() == false)
                return selfAction.StreamingDescription;
            return $"Doing {Type}";
        }
    }
}
