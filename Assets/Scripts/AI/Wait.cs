﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.AI
{
    class Wait : IGoal
    {
        public GoalReturn ExecuteStep()
        {
            return GoalReturn.CompletedGoal;
        }

        public string ReportGoal()
        {
            return "Waiting around";
        }
    }
}
