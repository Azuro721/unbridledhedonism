﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.AI
{
    enum GoalReturn
    {
        DidStep,
        CompletedGoal,
        AbortGoal,       
        GoalAlreadyDone,
    }
}
