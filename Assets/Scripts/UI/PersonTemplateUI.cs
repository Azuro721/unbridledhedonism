﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PersonTemplateUI : MonoBehaviour
{

    public TMP_Dropdown Gender;
    public TMP_Dropdown Orientation;
    public TMP_Dropdown Personality;
    public TMP_Dropdown Race;
    public Slider VoreOddsSlider;
    public Slider WeightSlider;
    public Button CustomizePersonality;    

    public Button RemoveButton;
    public Button DuplicateButton;

    internal TemplatePersonality CustomPersonality;   

    public void PersonalityChanged()
    {
        CustomizePersonality.interactable = Personality.value == 6;
    }
      
    internal void SetUpRaces()
    {
        Race.AddOptions(RaceManager.PickableRaces);
    }

   
    private void Start()
    {
        RemoveButton.onClick.AddListener(() => State.GameManager.TemplateEditorScreen.RemoveTemplate(this));
        DuplicateButton.onClick.AddListener(() => State.GameManager.TemplateEditorScreen.DuplicateTemplate(this));
        if (CustomPersonality == null)
            CustomPersonality = new TemplatePersonality();

        CustomizePersonality.onClick.AddListener(() =>
        {
            State.GameManager.VariableEditor.Open(CustomPersonality, "");
            State.GameManager.VariableEditor.SetColumns(2);
        });
        

        //SaveButton.onClick.AddListener(() => State.GameManager.StartScreen.SavedCharacterScreen.SaveCharacter(this));

    }



}
