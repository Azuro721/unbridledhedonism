﻿using OdinSerializer;
using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class MidGameSavedCharacterScreen : MonoBehaviour
    {

        SavedPersonContainer Container;

        internal List<SavedPerson> Specials;

        public List<SavedPersonUI> PeopleObjects;
        public List<GameObject> OtherObjects;

        public TMP_InputField TagSearch;

        public TextMeshProUGUI PopCount;

        public GameObject PeopleObjectPrefab;
        public Transform PeopleObjectFolder;

        internal void Open()
        {
            var count = State.World.DormRooms.Count - State.World.GetAllPeople(true).Count;
            PopCount.text = $"World has {count} remaining dorm room{(count == 1 ? "" : "s")}";
            Container = State.SavedPersonController.Container;
            gameObject.SetActive(true);
            foreach (var person in State.SavedPersonController.Specials.OrderBy(s => s.LastName).ThenBy(s => s.FirstName))
            {
                var obj = Instantiate(PeopleObjectPrefab, PeopleObjectFolder).GetComponent<SavedPersonUI>();
                PeopleObjects.Add(obj);
                obj.AddToGame.onClick.AddListener(() => obj.AddToGame.interactable = false);
                obj.AddToGame.onClick.AddListener(() => LoadCharacter(person, obj));                
                obj.Tags.text = person.Tags;
                obj.Tags.interactable = false;
                obj.DeleteCharacter.gameObject.SetActive(false);
                if (State.SavedPersonController.Container.HiddenList.Contains(person.FirstName + person.LastName))
                    obj.gameObject.SetActive(false);
                obj.Name.text = $"<color=#ff881a>{person.FirstName} {person.LastName}</color>";
                obj.AddToGame.interactable = State.World.GetAllPeople(true).Where((s) => s.FirstName == person.FirstName && s.LastName == person.LastName).Any() == false;
            }
            foreach (var person in Container.List.OrderBy(s => s.LastName).ThenBy(s => s.FirstName))
            {
                var obj = Instantiate(PeopleObjectPrefab, PeopleObjectFolder).GetComponent<SavedPersonUI>();
                PeopleObjects.Add(obj);
                obj.AddToGame.onClick.AddListener(() => obj.AddToGame.interactable = false);
                obj.AddToGame.onClick.AddListener(() => LoadCharacter(person, obj));                
                obj.Tags.text = person.Tags;
                obj.Tags.interactable = false;
                obj.DeleteCharacter.gameObject.SetActive(false);
                obj.Name.text = $"{person.FirstName} {person.LastName}";
                obj.AddToGame.interactable = State.World.GetAllPeople(true).Where((s) => s.FirstName == person.FirstName && s.LastName == person.LastName).Any() == false;
            }


        }

        public void Close()
        {
            gameObject.SetActive(false);
            foreach (var obj in PeopleObjects.ToList())
            {
                PeopleObjects.Remove(obj);
                Destroy(obj.gameObject);
            }
            foreach (var obj in OtherObjects.ToList())
            {               
                Destroy(obj);
            }
        }


        public void AddCharactersMatchingTags()
        {
            foreach (var obj in PeopleObjects)
            {
                if (obj.AddToGame.interactable && obj.Tags.text.Contains(TagSearch.text))
                    obj.AddToGame.onClick.Invoke();
            }
            var count = State.World.DormRooms.Count - State.World.GetAllPeople(true).Count;
            PopCount.text = $"World has {count} remaining dorm room{(count == 1 ? "" : "s")}";
        }


        internal void LoadCharacter(SavedPerson person, SavedPersonUI sp)
        {            

            var obj = Instantiate(State.GameManager.StartScreen.PersonObjectPrefab, PeopleObjectFolder).GetComponent<StartPersonUI>();
            RefreshSexes();
            obj.FirstName.text = person.FirstName;
            obj.LastName.text = person.LastName;
            obj.Gender.value = person.Gender;
            obj.Orientation.value = person.Orientation;
            obj.Personality.value = person.Personality;
            obj.CanVore.isOn = person.CanVore;
            obj.CustomAppearance = person.CustomAppearance;
            obj.CustomPersonality = person.CustomPersonality;
            obj.Person = person.Person;
            obj.Tags = person.Tags;
            obj.SetUpRaces();
            if (person.Race.IsNullOrWhitespace())
                obj.Race.captionText.text = "Human";
            else
                TryToSetRace(obj, person.Race);

            obj.Gender.RefreshShownValue();
            obj.Orientation.RefreshShownValue();
            obj.Personality.RefreshShownValue();

            if (obj.Person != null)
            {
                obj.CustomizePersonality.gameObject.SetActive(false);
                obj.CustomizeAppearance.gameObject.SetActive(false);
                obj.Personality.gameObject.SetActive(false);
                obj.EditCharacter.gameObject.SetActive(true);
                obj.EditTraits.gameObject.SetActive(true);
                obj.GenCharacter.gameObject.SetActive(false);
                obj.Race.gameObject.SetActive(false);
                obj.CustomPersonality = null;
                obj.CustomAppearance = null;
            }

            var newPerson = StartScreen.CreatePerson(obj);
            OtherObjects.Add(obj.gameObject);
            obj.gameObject.SetActive(false);
            Destroy(obj);

            if (State.World.ManualAddPerson(newPerson) == false)
                sp.AddToGame.interactable = true;

            var count = State.World.DormRooms.Count - State.World.GetAllPeople(true).Count;
            PopCount.text = $"World has {count} remaining dorm room{(count == 1 ? "" : "s")}";

            void RefreshSexes()
            {
                int option = obj.Gender.value;
                obj.Gender.ClearOptions();
                var options = new List<string>()
                {
                    State.World.GenderList.List[0].Name,
                    State.World.GenderList.List[1].Name,
                    State.World.GenderList.List[2].Name,
                    State.World.GenderList.List[3].Name,
                    State.World.GenderList.List[4].Name,
                    State.World.GenderList.List[5].Name,
                };
                obj.Gender.AddOptions(options);
                obj.Gender.value = option;
                obj.Gender.RefreshShownValue();
            }

            void TryToSetRace(StartPersonUI start, string race)
            {
                var index = RaceManager.PickableRaces.IndexOf(race);
                if (index >= 0)
                {
                    start.Race.value = index;
                    start.Race.RefreshShownValue();
                }
                else if (RaceManager.GetRace(race)?.NamedCharacter ?? false)
                {
                    start.SetNamedRace(race);
                }
                else
                {
                    start.Race.value = 0;
                    start.Race.RefreshShownValue();
                }
            }

        }
    }


}
