﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class WindowSettingsPanel : MonoBehaviour
{
    public Slider LeftTextSize;
    public Slider LeftWindowSize;
    public Slider LeftPortraitSizeX;
    public Slider RightTextSize;
    public Slider RightWindowSize;
    public Slider RightPortraitSizeX;
    public Slider BottomTextSize;

    public void Open()
    {
        gameObject.SetActive(true);
        LeftTextSize.onValueChanged.RemoveAllListeners();
        LeftWindowSize.onValueChanged.RemoveAllListeners();
        LeftPortraitSizeX.onValueChanged.RemoveAllListeners();
        RightTextSize.onValueChanged.RemoveAllListeners();
        RightWindowSize.onValueChanged.RemoveAllListeners();
        RightPortraitSizeX.onValueChanged.RemoveAllListeners();
        BottomTextSize.onValueChanged.RemoveAllListeners();
        LeftTextSize.value = PlayerPrefs.GetFloat("LeftTextSize", 22);
        LeftWindowSize.value = PlayerPrefs.GetFloat("LeftWindowSize", 400);
        LeftPortraitSizeX.value = PlayerPrefs.GetFloat("LeftPortraitSize", 200);
        RightTextSize.value = PlayerPrefs.GetFloat("RightTextSize", 22);
        RightWindowSize.value = PlayerPrefs.GetFloat("RightWindowSize", 400);
        RightPortraitSizeX.value = PlayerPrefs.GetFloat("RightPortraitSize", 200);
        BottomTextSize.value = PlayerPrefs.GetFloat("BottomTextSize", 24);
        LeftTextSize.onValueChanged.AddListener((s) => UpdateAndSave());
        LeftWindowSize.onValueChanged.AddListener((s) => UpdateAndSave());
        LeftPortraitSizeX.onValueChanged.AddListener((s) => UpdateAndSave());
        RightTextSize.onValueChanged.AddListener((s) => UpdateAndSave());
        RightWindowSize.onValueChanged.AddListener((s) => UpdateAndSave());
        RightPortraitSizeX.onValueChanged.AddListener((s) => UpdateAndSave());
        BottomTextSize.onValueChanged.AddListener((s) => UpdateAndSave());
    }

    public void UpdateAndSave()
    {
        State.GameManager.PlayerText.fontSize = LeftTextSize.value;
        State.GameManager.PlayerText.transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(LeftWindowSize.value, 0);
        State.GameManager.PlayerIdealPortraitWidth = LeftPortraitSizeX.value;
        State.GameManager.TargetText.fontSize = RightTextSize.value;
        State.GameManager.TargetText.transform.parent.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(RightWindowSize.value, 0);
        State.GameManager.TargetIdealPortraitWidth = RightPortraitSizeX.value;
        State.GameManager.LogText.fontSize = BottomTextSize.value;        
        State.GameManager.LogText.transform.parent.parent.parent.GetComponent<RectTransform>().offsetMin = new Vector2(LeftWindowSize.value, 0);
        State.GameManager.LogText.transform.parent.parent.parent.GetComponent<RectTransform>().offsetMax = new Vector2(-RightWindowSize.value, 400);
        var scalingfactor = State.GameManager.HoveredPanel.transform.parent.localScale.x;
        State.GameManager.HoveredPanel.transform.position = new Vector3(LeftWindowSize.value * scalingfactor, State.GameManager.HoveredPanel.transform.position.y, 0);
        State.GameManager.ContinueActionPanel.GetComponent<RectTransform>().rect.Set(LeftWindowSize.value, RightWindowSize.value, 1920 - LeftWindowSize.value - RightWindowSize.value, 400);
        State.GameManager.ContinueActionPanel.GetComponent<RectTransform>().offsetMin = new Vector2(LeftWindowSize.value, 400);
        State.GameManager.ContinueActionPanel.GetComponent<RectTransform>().offsetMax = new Vector2(-RightWindowSize.value, 440);
        State.GameManager.UpdatePortraits();
        PlayerPrefs.SetFloat("LeftTextSize", LeftTextSize.value);
        PlayerPrefs.SetFloat("LeftWindowSize", LeftWindowSize.value);
        PlayerPrefs.SetFloat("LeftPortraitSize", LeftPortraitSizeX.value);
        PlayerPrefs.SetFloat("RightTextSize", RightTextSize.value);
        PlayerPrefs.SetFloat("RightWindowSize", RightWindowSize.value);
        PlayerPrefs.SetFloat("RightPortraitSize", RightPortraitSizeX.value);
        PlayerPrefs.SetFloat("BottomTextSize", BottomTextSize.value);

    }


}
