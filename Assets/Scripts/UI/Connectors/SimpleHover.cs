﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleHover : MonoBehaviour
{
    public TextMeshProUGUI InfoText;


    public static bool IsPointerOverGameObject(GameObject gameObject)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        return raycastResults.Any(x => x.gameObject == gameObject);
    }

    private void Update()
    {
        if (gameObject.activeInHierarchy == false)
            return;

        if (IsPointerOverGameObject(gameObject) == false)
            return;

        int wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, null);

        if (wordIndex > -1)
        {
            string[] words = new string[5];
            for (int i = 0; i < 5; i++)
            {
                if (wordIndex - 2 + i < 0 || wordIndex - 2 + i >= InfoText.textInfo.wordCount || InfoText.textInfo.wordInfo[wordIndex - 2 + i].characterCount < 1)
                {
                    words[i] = string.Empty;
                    continue;
                }
                words[i] = InfoText.textInfo.wordInfo[wordIndex - 2 + i].GetWord();
            }
            State.GameManager.HoveringTooltip.UpdateInformation(words);
        }
    }

}