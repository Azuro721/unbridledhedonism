﻿using UnityEngine;
using System.Collections;
using TMPro;
using System;

namespace Assets.Scripts.UI.Connectors
{
    public class TraitClickDismiss : MonoBehaviour
    {
        public TextMeshProUGUI InfoText;

        private void Update()
        {
            if (gameObject.activeInHierarchy == false)
                return;

            if (Input.GetMouseButtonDown(1) == false)
                return;

            int wordIndex = TMP_TextUtilities.FindIntersectingWord(InfoText, Input.mousePosition, null);

            if (wordIndex > -1)
            {
                string[] words = new string[5];
                for (int i = 0; i < 5; i++)
                {
                    if (wordIndex - 2 + i < 0 || wordIndex - 2 + i >= InfoText.textInfo.wordCount || InfoText.textInfo.wordInfo[wordIndex - 2 + i].characterCount < 1)
                    {
                        words[i] = string.Empty;
                        continue;
                    }
                    words[i] = InfoText.textInfo.wordInfo[wordIndex - 2 + i].GetWord();
                }
                RemoveClickedQuirk(words);
                RemoveClickedTrait(words);
                State.GameManager.TraitEditorScreen.RefreshText();
            }
        }
        void RemoveClickedTrait(string[] words)
        {
            if (Enum.TryParse(words[2], out Traits traitType))
            {
                State.GameManager.TraitEditorScreen.Person.Traits.Remove(traitType);
            }
            
        }

        void RemoveClickedQuirk(string[] words)
        {
            if (Enum.TryParse(words[2], out Quirks traitType))
            {
                State.GameManager.TraitEditorScreen.Person.Quirks.Remove(traitType);
            }
        }
    }
    
}


