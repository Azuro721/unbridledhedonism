﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using UnityEngine.UI;
using TMPro;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Assets.Scripts.UI.Connectors;

public class VariableEditor : MonoBehaviour
{
    List<object> EditingObjects = new List<object>();

    public Transform Folder;

    public GameObject Button;
    public GameObject Toggle;
    public GameObject InputField;
    public GameObject Slider;
    public GameObject Dropdown;

    public Button ExtraButton;
    public Button SecondExtraButton;

    public TextMeshProUGUI TooltipText;
    public TextMeshProUGUI TitleText;

    internal Dictionary<string, string> TempDictionary;
    internal Dictionary<Traits, int> TempDictionaryT;
    internal Dictionary<Quirks, int> TempDictionaryQ;

    internal const BindingFlags Bindings = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

    internal Action OnClose;

    [SerializeField]
    internal GridLayoutGroup GridLayout;
    // added that "internal" right there, hope it doesnt break anything - Luke

    internal void OpenAndProcessPerson(Person person)
    {
        string name = person.FirstName + " " + person.LastName;
        Open(person, $"{name}");
        var button = Instantiate(Button, Folder);
        button.GetComponent<Button>().onClick.AddListener(() => State.GameManager.CreateRaceChanger(person));       
        button.GetComponentInChildren<TextMeshProUGUI>().text = "Change Race";
        button.gameObject.AddComponent<VariableScreenTooltip>();
        button.GetComponent<VariableScreenTooltip>().Text = "Changes the Race, note this discards any currently changed options so it should be done first.";
        button.transform.SetSiblingIndex(0);
        Add(person.Personality, $"{name}'s Personality");
        Add(person.PartList, $"{name}'s Appearance");
        Add(person.Needs, $"{name}'s Needs");
        Add(person.Magic, $"{name}'s Magic Info");
        Add(person.Romance, $"{name}'s Romantic Info");
        Add(person.VoreController, $"{name}'s Vore Info");

        OnClose = new Action(() => UpdateRoomName(person));
    }

    void UpdateRoomName(Person person)
    {
        var room = State.World.GetZone(person.MyRoom);
        if (room != null)
        {
            room.Name = $"{person.FirstName}'s Room";
        }
    }

    internal void Open<T>(T obj, string titleText)
    {
        if (obj == null)
        {
            Debug.Log("Tried to open on null object!");
            return;
        }

        SetColumns(1);

        ExtraButton.onClick.RemoveAllListeners();
        ExtraButton.gameObject.SetActive(false);

        SecondExtraButton.onClick.RemoveAllListeners();
        SecondExtraButton.gameObject.SetActive(false);

        TitleText.text = titleText;

        gameObject.SetActive(true);
        EditingObjects.Clear();
        EditingObjects.Add(obj);
        

        int children = Folder.childCount;
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(Folder.GetChild(i).gameObject);
        }

        ProcessFields(obj);
        ProcessProperties(obj);
    }

    internal void OpenAlternate<T>(T obj)
    {
        if (obj == null)
        {
            Debug.Log("Tried to open on null object!");
            return;
        }

        gameObject.SetActive(true);
        //EditingObjects.Clear();
        //EditingObjects.Add(obj);

        int children = Folder.childCount;
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(Folder.GetChild(i).gameObject);
        }

        ProcessFields(obj);
        ProcessProperties(obj);
    }

    internal void SetColumns(int value)
    {
        GridLayout.constraintCount = value;
    }

    internal void Add<T>(T obj, string titleText)
    {
        if (obj == null)
        {
            Debug.Log("Tried to open on null object!");
            return;
        }

        var newTitle = Instantiate(TitleText, Folder);

        newTitle.text = titleText;

        EditingObjects.Add(obj);

        ProcessFields(obj);
        ProcessProperties(obj);
    }

    internal void SetExtraButton(string text, Action action)
    {
        ExtraButton.gameObject.SetActive(true);        
        ExtraButton.onClick.AddListener(SaveAndClose);
        ExtraButton.onClick.AddListener(() => action());
        ExtraButton.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }

    internal void SetSecondExtraButton(string text, Action action)
    {
        SecondExtraButton.gameObject.SetActive(true);
        SecondExtraButton.onClick.AddListener(() => action());
        SecondExtraButton.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }

    private void ProcessFields<T>(T obj)
    {
        FieldInfo[] fields = obj.GetType().GetFields(Bindings);
        foreach (FieldInfo field in fields)
        {
            if (field.Name.Contains("Backing") || field.Name.StartsWith("_") || field.CustomAttributes.Any(s => s.AttributeType == typeof(ObsoleteAttribute) || s.AttributeType == typeof(VariableEditorIgnores)))
                continue;
            if (field.FieldType == typeof(bool))
            {
                var newObj = Instantiate(Toggle, Folder);
                var toggle = newObj.GetComponent<Toggle>();
                newObj.name = field.Name;
                toggle.isOn = (bool)field.GetValue(obj);
                toggle.GetComponentInChildren<TextMeshProUGUI>().text = field.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        toggle.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        toggle.gameObject.AddComponent<VariableScreenTooltip>();
                        toggle.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }
            if (field.FieldType == typeof(string))
            {
                var newObj = Instantiate(InputField, Folder);
                var input = newObj.GetComponent<CombinedInputfield>();
                newObj.name = field.Name;
                input.Inputfield.text = (string)field.GetValue(obj);
                newObj.GetComponent<CombinedInputfield>().Text.text = field.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponent<CombinedInputfield>().Text.text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }
            if (field.FieldType == typeof(int))
            {
                var newObj = Instantiate(Slider, Folder);
                var slider = newObj.GetComponentInChildren<Slider>();
                newObj.name = field.Name;
                newObj.GetComponentInChildren<TextMeshProUGUI>().text = field.Name; //Designed to be overwritten by proper
                slider.wholeNumbers = true;
                foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                    if (attr is GenderDescriptionAttribute)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = "This is changed in a dumb way at the moment but will probably be improved later.\n" + State.World?.GenderList?.TextList() ?? State.GameManager?.StartScreen?.GenderList?.TextList() ?? State.BackupGenderList.TextList();
                    }
                    if (attr is FloatRangeAttribute range)
                    {
                        Debug.Log("float attribute used on integer");
                    }
                    if (attr is IntegerRangeAttribute intRange)
                    {
                        slider.minValue = intRange.Min;
                        slider.maxValue = intRange.Max;
                    }
                }
                var value = (int)field.GetValue(obj);
                if (value > slider.maxValue)
                    slider.maxValue = value;
                slider.value = (int)field.GetValue(obj); // Must be set after the min and max are set
            }
            if (field.FieldType == typeof(float))
            {
                var newObj = Instantiate(Slider, Folder);
                var slider = newObj.GetComponentInChildren<Slider>();
                newObj.name = field.Name;
                newObj.GetComponentInChildren<TextMeshProUGUI>().text = field.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                    if (attr is FloatRangeAttribute range)
                    {
                        slider.minValue = range.Min;
                        slider.maxValue = range.Max;
                    }
                    if (attr is IntegerRangeAttribute intRange)
                    {
                        Debug.Log("integer attribute used on float");
                        slider.wholeNumbers = true;
                        slider.minValue = intRange.Min;
                        slider.maxValue = intRange.Max;
                    }
                }
                var value = (float)field.GetValue(obj);
                if (value > slider.maxValue)
                    slider.maxValue = value;
                slider.value = (float)field.GetValue(obj); // Must be set after the min and max are set
            }
            if (field.FieldType.BaseType == typeof(Enum))
            {
                var newObj = Instantiate(Dropdown, Folder);
                var dropdown = newObj.GetComponentInChildren<TMP_Dropdown>();
                newObj.name = field.Name;
                Type enumType = field.FieldType;
                var values = Enum.GetValues(enumType);
                dropdown.ClearOptions();
                for (int i = 0; i < values.Length; i++)
                {
                    dropdown.options.Add(new TMP_Dropdown.OptionData(values.GetValue(i).ToString()));
                }
                dropdown.RefreshShownValue();
                dropdown.value = (int)field.GetValue(obj);
                newObj.GetComponentInChildren<TextMeshProUGUI>().text = field.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(field))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }
            if (field.FieldType == typeof(Dictionary<string, string>)) //Currently used for the race tags
            {
                TempDictionary = (Dictionary<string, string>)field.GetValue(obj);
                if (TempDictionary != null)
                {
                    foreach (var entry in TempDictionary)
                    {
                        var newObj = Instantiate(InputField, Folder);
                        var input = newObj.GetComponent<CombinedInputfield>();
                        newObj.name = $"UsingDictionaryR^{entry.Key}";
                        input.Inputfield.text = entry.Value;
                        newObj.GetComponent<CombinedInputfield>().Text.text = entry.Key;
                    }
                }
            }
            if (field.FieldType == typeof(Dictionary<Traits, int>)) //Currently used for the Trait weights
            {
                TempDictionaryT = (Dictionary<Traits, int>)field.GetValue(obj);
                if (TempDictionaryT != null)
                {
                    foreach (var entry in TempDictionaryT.OrderBy(s => s.Key.ToString()))
                    {
                        var newObj = Instantiate(InputField, Folder);
                        var input = newObj.GetComponent<CombinedInputfield>();
                        newObj.name = $"UsingDictionaryT^{entry.Key}";
                        input.Inputfield.text = entry.Value.ToString();
                        newObj.GetComponent<CombinedInputfield>().Text.text = entry.Key.ToString();
                        var tData = TraitList.GetTrait(entry.Key);
                        if (tData != null)
                        {
                            newObj.AddComponent<VariableScreenTooltip>();
                            newObj.GetComponent<VariableScreenTooltip>().Text = tData.Description;
                        }

                    }
                }
            }
            if (field.FieldType == typeof(Dictionary<Quirks, int>)) //Currently used for the Trait weights
            {
                TempDictionaryQ = (Dictionary<Quirks, int>)field.GetValue(obj);
                if (TempDictionaryQ != null)
                {
                    foreach (var entry in TempDictionaryQ.OrderBy(s => s.Key.ToString()))
                    {
                        var newObj = Instantiate(InputField, Folder);
                        var input = newObj.GetComponent<CombinedInputfield>();
                        newObj.name = $"UsingDictionaryQ^{entry.Key}";
                        input.Inputfield.text = entry.Value.ToString();
                        newObj.GetComponent<CombinedInputfield>().Text.text = entry.Key.ToString();
                        var qData = TraitList.GetTrait(entry.Key);
                        if (qData != null)
                        {
                            newObj.AddComponent<VariableScreenTooltip>();
                            newObj.GetComponent<VariableScreenTooltip>().Text = qData.Description;
                        }
                    }
                }
            }
        }
    }

    private void ProcessProperties<T>(T obj)
    {
        PropertyInfo[] properties = obj.GetType().GetProperties(Bindings);
        foreach (PropertyInfo property in properties)
        {
            if (property.CustomAttributes.Any(s => s.AttributeType == typeof(ObsoleteAttribute) || s.AttributeType == typeof(VariableEditorIgnores)))
                continue;
            if (property.PropertyType == typeof(bool))
            {
                var newObj = Instantiate(Toggle, Folder);
                var toggle = newObj.GetComponent<Toggle>();
                newObj.name = property.Name;
                toggle.isOn = (bool)property.GetValue(obj);
                toggle.GetComponentInChildren<TextMeshProUGUI>().text = property.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(property))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        toggle.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        toggle.gameObject.AddComponent<VariableScreenTooltip>();
                        toggle.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }
            if (property.PropertyType == typeof(string))
            {
                var newObj = Instantiate(InputField, Folder);
                var input = newObj.GetComponent<CombinedInputfield>();
                newObj.name = property.Name;
                input.Inputfield.text = (string)property.GetValue(obj);
                newObj.GetComponent<CombinedInputfield>().Text.text = property.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(property))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponent<CombinedInputfield>().Text.text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }
            //if (field.FieldType == typeof(int))
            //{
            //    var newObj = Instantiate(InputField, Folder);
            //    var input = newObj.GetComponent<CombinedInputfield>();
            //    newObj.name = field.Name;
            //    input.Inputfield.text = ((int)field.GetValue(obj)).ToString();
            //    foreach (Attribute attr in Attribute.GetCustomAttributes(field))
            //    {
            //        if (attr is ProperNameAttribute proper)
            //        {
            //            newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
            //        }
            //        if (attr is DescriptionAttribute desc)
            //        {
            //            newObj.gameObject.AddComponent<VariableScreenTooltip>();
            //            newObj.GetComponent<VariableScreenTooltip>().text = desc.Description;
            //        }
            //    }
            //}
            if (property.PropertyType == typeof(float))
            {
                var newObj = Instantiate(Slider, Folder);
                var slider = newObj.GetComponentInChildren<Slider>();
                if (property.Name == "Weight")
                {
                    var sliderDisplay = newObj.GetComponentInChildren<SliderDisplay>();
                    sliderDisplay.Weight = true;
                }
                if (property.Name == "Height")
                {
                    var sliderDisplay = newObj.GetComponentInChildren<SliderDisplay>();
                    sliderDisplay.Height = true;
                }
                newObj.name = property.Name;
                newObj.GetComponentInChildren<TextMeshProUGUI>().text = property.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(property))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                    if (attr is FloatRangeAttribute range)
                    {
                        slider.minValue = range.Min;
                        slider.maxValue = range.Max;
                    }
                }
                var value = (float)property.GetValue(obj);
                if (value > slider.maxValue)
                    slider.maxValue = value;
                slider.value = (float)property.GetValue(obj); // Must be set after the min and max are set
            }
            if (property.PropertyType.BaseType == typeof(Enum))
            {
                var newObj = Instantiate(Dropdown, Folder);
                var dropdown = newObj.GetComponentInChildren<TMP_Dropdown>();
                newObj.name = property.Name;
                Type enumType = property.PropertyType;
                var values = Enum.GetValues(enumType);
                dropdown.ClearOptions();
                for (int i = 0; i < values.Length; i++)
                {
                    dropdown.options.Add(new TMP_Dropdown.OptionData(values.GetValue(i).ToString()));
                }
                dropdown.RefreshShownValue();
                dropdown.value = (int)property.GetValue(obj);
                newObj.GetComponentInChildren<TextMeshProUGUI>().text = property.Name; //Designed to be overwritten by proper
                foreach (Attribute attr in Attribute.GetCustomAttributes(property))
                {
                    if (attr is ProperNameAttribute proper)
                    {
                        newObj.GetComponentInChildren<TextMeshProUGUI>().text = proper.Name;
                    }
                    if (attr is DescriptionAttribute desc)
                    {
                        newObj.gameObject.AddComponent<VariableScreenTooltip>();
                        newObj.GetComponent<VariableScreenTooltip>().Text = desc.Description;
                    }
                }
            }

        }
    }

    internal void ChangeToolTip(string text)
    {
        TooltipText.text = text;
    }

    public void SaveAndClose()
    {
        int children = Folder.childCount;
        for (int i = 0; i < children; i++)
        {
            var obj = Folder.GetChild(i).gameObject;
            var drop = obj.GetComponentInChildren<TMP_Dropdown>();
            if (drop != null)
            {
                foreach (var EditingObject in EditingObjects)
                {                   
                    EditingObject.GetType().GetField(obj.name, Bindings)?.SetValue(EditingObject, drop.value);
                    EditingObject.GetType().GetProperty(obj.name, Bindings)?.SetValue(EditingObject, drop.value);
                }
                continue;
            }
            var toggle = obj.GetComponentInChildren<Toggle>();
            if (toggle != null)
            {
                foreach (var EditingObject in EditingObjects)
                {
                    EditingObject.GetType().GetField(obj.name, Bindings)?.SetValue(EditingObject, toggle.isOn);
                    EditingObject.GetType().GetProperty(obj.name, Bindings)?.SetValue(EditingObject, toggle.isOn);
                }
                continue;
            }
            var slider = obj.GetComponentInChildren<Slider>();
            if (slider != null)
            {
                if (slider.wholeNumbers)
                {
                    foreach (var EditingObject in EditingObjects)
                    {
                        EditingObject.GetType().GetField(obj.name, Bindings)?.SetValue(EditingObject, (int)slider.value);
                        EditingObject.GetType().GetProperty(obj.name, Bindings)?.SetValue(EditingObject, (int)slider.value);
                    }
                }
                else
                {
                    foreach (var EditingObject in EditingObjects)
                    {
                        EditingObject.GetType().GetField(obj.name, Bindings)?.SetValue(EditingObject, slider.value);
                        EditingObject.GetType().GetProperty(obj.name, Bindings)?.SetValue(EditingObject, slider.value);
                    }
                }
                continue;
            }
            var input = obj.GetComponentInChildren<TMP_InputField>();
            if (input != null)
            {
                if (obj.name.Contains("UsingDictionary"))
                {
                    if (obj.name.Contains("UsingDictionaryR"))
                    {
                        var split = obj.name.Split('^');
                        TempDictionary[split[1]] = input.text;

                    }
                    if (obj.name.Contains("UsingDictionaryT"))
                    {
                        var split = obj.name.Split('^');
                        if (Enum.TryParse(split[1], out Traits trait))
                        {
                            if (int.TryParse(input.text, out int result))
                                TempDictionaryT[trait] = result;
                        }


                    }
                    if (obj.name.Contains("UsingDictionaryQ"))
                    {
                        var split = obj.name.Split('^');
                        if (Enum.TryParse(split[1], out Quirks trait))
                        {
                            if (int.TryParse(input.text, out int result))
                                TempDictionaryQ[trait] = result;
                        }

                    }
                }
                else
                {
                    foreach (var EditingObject in EditingObjects)
                    {
                        if (EditingObject.GetType().GetProperty(obj.name, Bindings)?.GetCustomAttribute<VariableEditorIgnores>() != null) 
                            continue;  //Patch to prevent it from setting Race in multiple places at once
                        EditingObject.GetType().GetField(obj.name, Bindings)?.SetValue(EditingObject, input.text);
                        EditingObject.GetType().GetProperty(obj.name, Bindings)?.SetValue(EditingObject, input.text);
                    }
                }

                continue;
            }
            if (obj.GetComponentInChildren<TextMeshProUGUI>() != null)
                continue; //Done to ignore the headings.
            Debug.LogWarning("Couldn't handle object!");
        }
        gameObject.SetActive(false);
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(Folder.GetChild(i).gameObject);
        }
        OnClose?.Invoke();
        OnClose = null;
    }

    public void Close()
    {
        gameObject.SetActive(false);
        int children = Folder.childCount;
        for (int i = children - 1; i >= 0; i--)
        {
            Destroy(Folder.GetChild(i).gameObject);
        }
        GridLayout.constraintCount = 1;
        OnClose = null;
    }


}
