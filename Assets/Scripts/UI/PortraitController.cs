using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

enum PortraitFlag {
    Dead,
    Alive,  // New, implied when dead isn't set.
    Eaten,
    Full,
    Both,  // Implies Belly/Balls.
    Belly,  // New.
    Balls,
    Unwilling,
    Weakened,
    Nude,
    Underwear,
    Disposal,
    NonDisposal,  // Implied when Disposal is not set.
    Horny,
    Absorbing,
    Hungry,
    CV,
    UB,
    Digesting,  // If this person is eaten and being digested.
    Safe,  // If this person is eaten and not being digested.
}

class PortraitController
{
    HashSet<string> PicturesUnloaded;
    HashSet<string> PicturesAny;
    Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>> Pictures;

    public PortraitController()
    {
        PicturesUnloaded = new HashSet<string>();
        Pictures = new Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>>();

        if (Directory.Exists(Application.streamingAssetsPath) == false)
            return;

        string[] files = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Pictures"), "*", SearchOption.AllDirectories);

        foreach (string file in files)
        {
            if (!File.Exists(file)) continue;



            if (file.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase) || file.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase) || file.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase))
            {
                if (file.ToLower().Contains("-"))
                {
                    var baseName = Path.GetFileNameWithoutExtension(file).Split('-')[0];
                    PicturesUnloaded.Add(baseName.ToLower());
                }
                else
                {
                    PicturesUnloaded.Add(Path.GetFileNameWithoutExtension(file).ToLower());
                }

            }
        }
        PicturesAny = new HashSet<string>(PicturesUnloaded);

        if (PicturesUnloaded.Contains("default"))
        {
            AttemptLoad("Default");
            PicturesUnloaded.Remove("default");
        }
    }

    /// Return the plain hash set of flags from a contiguous string of flag names without more processing.
    internal static HashSet<PortraitFlag> ParseStringToFlagsRaw(string str)
    {
        if (String.IsNullOrWhiteSpace(str)) return new HashSet<PortraitFlag>();
        str = str.ToLower();
        foreach (var flagName in Enum.GetNames(typeof(PortraitFlag)).OrderByDescending(x => x.Length)) {
            if (!str.StartsWith(flagName.ToLower())) continue;
            var flags = ParseStringToFlags(str.Substring(flagName.Length));
            flags.Add((PortraitFlag)Enum.Parse(typeof(PortraitFlag), flagName));
            return flags;
        }
        throw new Exception($"Can not parse next flag from {str}");
    }

    /// Return a hash set of flags from a contiguous string of flag names.
    internal static HashSet<PortraitFlag> ParseStringToFlags(string str)
    {
        var flags = ParseStringToFlagsRaw(str);
        if (flags.Count == 0) return flags;  // Default image.
        // Keep both/belly/balls flags consistent.
        if (flags.Contains(PortraitFlag.Both)) {
            flags.Add(PortraitFlag.Belly);
            flags.Add(PortraitFlag.Balls);
        }
        if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls)) flags.Add(PortraitFlag.Both);
        // Add alive when dead isn't set, so that alive images are not displayed when dead.
        if (!flags.Contains(PortraitFlag.Dead)) {
            flags.Add(PortraitFlag.Alive);
        }
        if (!flags.Contains(PortraitFlag.Disposal)) {
            flags.Add(PortraitFlag.NonDisposal);
        }
        return flags;
    }

    internal void AttemptLoad(string name)
    {
        string[] files = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Pictures"), "*", SearchOption.AllDirectories);

        foreach (string file in files)
        {
            if (!File.Exists(file)) continue;

            var splits = Path.GetFileNameWithoutExtension(file).Split('-');

            if (splits[0].ToLower() != name.ToLower())
                continue;

            if (file.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase) || file.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase) || file.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase))
            {
                name = name.ToLower();
                if (!Pictures.ContainsKey(name)) {
                    Pictures.Add(name, new Dictionary<HashSet<PortraitFlag>, Sprite>());
                }
                Pictures[name].Add(ParseStringToFlags(splits.ElementAtOrDefault(1)), LoadPNG(file));
            }
        }
    }

    /// Return the 'best' picture for `name` with `flags`.
    /// The best picture is the one with the most conditions where all conditions are fulfilled.
    Sprite GetBestMatch(string name, HashSet<PortraitFlag> flags) {
        name = name.ToLower();
        if (!Pictures.ContainsKey(name)) name = "default";
        if (!Pictures.ContainsKey(name)) return null;
        var bestSets = Pictures[name].Keys.Where(s => s.IsSubsetOf(flags)).OrderByDescending(s => s.Count);
        foreach (var best in bestSets) {
            if (Pictures[name].ContainsKey(best)) return Pictures[name][best];
        };
        Debug.LogError("No picture found.  Missing a picture with no conditions.");
        return null;
    }

    internal Sprite GetPicture(Person person)
    {
        string str = person.Picture;
        if (string.IsNullOrWhiteSpace(str)) return null;
        if (PicturesAny.Contains(str.ToLower()) == false && PicturesAny.Contains("default") == false) return null;
        if (PicturesUnloaded.Contains(str.ToLower())) {
            AttemptLoad(person.Picture);
            PicturesUnloaded.Remove(str.ToLower());
        }
        var flags = new HashSet<PortraitFlag>();

        // DEAD CHECKS
        if (person.Health <= 0) {
            flags.Add(PortraitFlag.Dead);
            var vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();
            if (vorelocation == "balls") flags.Add(PortraitFlag.CV);
            if (vorelocation == "womb") flags.Add(PortraitFlag.UB);
        } else {
            flags.Add(PortraitFlag.Alive);
        }

        // PREY CHECKS
        if (person.BeingEaten) {
            if (Config.AltBellyImage) {
                var parent = GetPicture(person.FindMyPredator());
                if (parent != null) return parent;
            }
            flags.Add(PortraitFlag.Eaten);
            if (person.FindMyPredator().VoreController.GetProgressOf(person).Willing == false) {
                flags.Add(PortraitFlag.Unwilling);
            }
            if (person.FindMyPredator().VoreController.TargetIsBeingDigested(person)) {
                flags.Add(PortraitFlag.Digesting);
            } else {
                flags.Add(PortraitFlag.Safe);
            }
        }

        // DISPOSAL CHECKS
        if (person.StreamingSelfAction == SelfActionType.CockDisposalBathroom || person.StreamingSelfAction == SelfActionType.CockDisposalFloor) {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.CV);
        }
        if (person.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom || person.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor) {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.UB);
        }
        if (person.StreamingSelfAction == SelfActionType.ScatDisposalBathroom || person.StreamingSelfAction == SelfActionType.ScatDisposalFloor) {
            flags.Add(PortraitFlag.Disposal);
        }
        if (!flags.Contains(PortraitFlag.Disposal)) flags.Add(PortraitFlag.NonDisposal);

        // PREDATOR CHECKS
        if (person.VoreController.HasPrey(VoreLocation.Any)) flags.Add(PortraitFlag.Full);

        if (person.VoreController.HasBellyPrey()) flags.Add(PortraitFlag.Belly);
        if (person.VoreController.HasPrey(VoreLocation.Balls)) flags.Add(PortraitFlag.Balls);
        if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls)) flags.Add(PortraitFlag.Both);

        if (person.VoreController.HasPrey(VoreLocation.Any)
            && person.VoreController.BellySize() < 0.7
            && person.VoreController.BallsSize() < 0.7) {
            flags.Add(PortraitFlag.Absorbing);
        }

        // GENERAL CHECKS (NOT VORE RELATED)
        if (person.Health < Constants.HealthMax / 1.5) flags.Add(PortraitFlag.Weakened);
        if (person.Needs.Horniness > .8f) flags.Add(PortraitFlag.Horny);
        if (person.Needs.Hunger > .6f) flags.Add(PortraitFlag.Hungry);
        switch (person.ClothingStatus) {
            case ClothingStatus.Nude: flags.Add(PortraitFlag.Nude); break;
            case ClothingStatus.Underwear: flags.Add(PortraitFlag.Underwear); break;
            case ClothingStatus.Normal: break;
            default: break;
        }
        return GetBestMatch(str, flags);
    }

    static Sprite LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.

        }
        if (tex == null)
            return null;
        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);
        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension);
        return sprite;
    }
}
