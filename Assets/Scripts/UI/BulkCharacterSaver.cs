﻿using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BulkCharacterSaver : MonoBehaviour
{

    public Toggle OverwriteTags;

    public TMP_InputField Tags;

    public Button SaveButton;

    public Button CancelButton;

    private void Start()
    {
        SaveButton.onClick.AddListener(Save);
        CancelButton.onClick.AddListener(Cancel);
    }

    void Save()
    {
        foreach (var obj in State.GameManager.StartScreen.PeopleObjects)
        {
            if (obj.FirstName.interactable)
                State.GameManager.StartScreen.SavedCharacterScreen.SaveCharacter(obj);
        }
        string replace = Tags.text ?? "";

        var container = State.SavedPersonController.Container;
        foreach (var obj in State.GameManager.StartScreen.PeopleObjects)
        {
            foreach (var savedPerson in container.List)
            {
                if (savedPerson.FirstName == obj.FirstName.text && savedPerson.LastName == obj.LastName.text)
                {
                    if (OverwriteTags.isOn)
                    {
                        savedPerson.Tags = replace;
                    }
                    else
                    {
                        if (replace.IsNullOrWhitespace() == false)
                        {
                            if (savedPerson.Tags.IsNullOrWhitespace())
                                savedPerson.Tags = replace;
                            else if (savedPerson.Tags.Contains(replace) == false)
                                savedPerson.Tags += " " + replace;
                        }                        
                    }
                    obj.Tags = savedPerson.Tags;
                    break;
                }
            }

        }

        State.SavedPersonController.SaveToFile();
        gameObject.SetActive(false);
    }

    void Cancel()
    {
        gameObject.SetActive(false);
    }

    //State.GameManager.StartScreen.SavedCharacterScreen.SaveCharacter(this)
}
