﻿using UnityEngine;
using System.Collections;
using System.IO;
using TMPro;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour
{
    public TextMeshProUGUI VersionNumber;

    public Button TestInteractionsButton;

    public Button ShowDirectoryButton;

    public Button ResumeActiveGame;

    public TMP_InputField DirectoryArea;

    private void Start()
    {
        VersionNumber.text = $"Version : {State.Version}";
        TestInteractionsButton.gameObject.AddComponent<HoveringTooltipDisplayer>().Text = 
            "Attempts to compile all interaction conditionals that are loaded.  The purpose of this is just to give content creators a way to check if there are any errors in their conditionals without having to start a game and wait for a specific interaction to occur";
        ShowDirectoryButton.gameObject.AddComponent<HoveringTooltipDisplayer>().Text = 
            "Shows the directory where the maps/saves/stored character data is stored.";
    }

    private void Update()
    {
        if (gameObject.activeSelf)
        {
            if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKey(KeyCode.F2))
            {
                Screen.SetResolution(640, 480, FullScreenMode.Windowed);
            }
        }
    }
    public void StartNewGame()
    {
        State.GameManager.StartScreen.Open();       
        gameObject.SetActive(false);
    }

    public void OpenLoadSave()
    {
        State.GameManager.SaveLoadScreen.Open();
    }

    public void ShowSaveDirectory()
    {
        Application.OpenURL($"file:///{Path.GetFullPath(State.StorageDirectory)}");
    }

    public void OpenMapEditor()
    {
        State.GameManager.MapEditor.Open(true);
        gameObject.SetActive(false);
    }

    public void TestInteractions()
    {
        MessageManager.BulkTest();
    }

}
