﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class Constants
{
    public const int HealthMax = 1000;
    public const int HealthPerTurnRegen = 8;
    public const int NurseHeal = 120;
    public const int DigestionDamage = 60;
    public const int ResurrectHealth = 400;
    public const int EndHealth = -800;
}

