﻿using Assets.Scripts.AI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HelperFunctions;
using static InteractionOddsFunctions;

static class InteractionFunctions
{
    internal static void OutragedAtSex(Person a, Person t)
    {
        var rel = a.GetRelationshipWith(t);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(rel.RomanticLevel, .8f);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, .3f);
        a.Romance.Dating = null;
        t.Romance.Dating = null;
    }

    internal static void UnexpectedDigestion(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).Willing = false;
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = false;
        var rel = a.GetRelationshipWith(t);
        rel.FriendshipLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).FriendshipLevel, .75f * State.World.Settings.VoreAnger);
        rel.RomanticLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).RomanticLevel, .5f * State.World.Settings.VoreAnger);
        rel.VoreBetrayed();
        if (a.VoreTracking.Any())
            a.VoreTracking.Last().Betrayed = true;
    }

    internal static void Meet(Person a, Person t)
    {
        a.GetRelationshipWith(t).Met = true;
        t.GetRelationshipWith(a).Met = true;
    }

    internal static void Compliment(Person a, Person t)
    {
        IncreaseFriendship(t, a, .005f);
        IncreaseFriendshipSymetric(a, t, .005f, .010f);
    }

    internal static void FriendlyHug(Person a, Person t)
    {
        IncreaseFriendshipSymetric(a, t, .01f, .01f);
    }


    internal static void AskIfSingle(Person a, Person t)
    {
        a.GetRelationshipWith(t).KnowledgeAbout.DatingTarget = t.Romance.Dating;
        a.GetRelationshipWith(t).KnowledgeAbout.IsDating = t.Romance.Dating != null;
    }

    internal static void AskOut(Person a, Person t)
    {
        a.Romance.Dating = t;
        t.Romance.Dating = a;
    }

    internal static void AskOutFail(Person a, Person t)
    {
        RomanticRejection(a, t);
        a.GetRelationshipWith(t).LastAskedAboutDating = 0;
        t.GetRelationshipWith(a).LastAskedAboutDating = 0;
    }

    internal static void ComplimentAppearance(Person a, Person t)
    {
        IncreaseRomantic(t, a, .0075f);
        IncreaseRomanticSymetric(a, t, .005f, .010f);
    }



    internal static void StartSex(Person a, Person t)
    {
        a.GetRelationshipWith(t).HaveHadSex = true;
        t.GetRelationshipWith(a).HaveHadSex = true;
        a.StreamingSelfAction = SelfActionType.None;
        t.StreamingSelfAction = SelfActionType.None;
        if (a.ActiveSex == null)
        {

            a.ActiveSex = new ActiveSex(a, t);
            t.ActiveSex = new ActiveSex(t, a);
        }
    }

    internal static void AskToBeVored(Person a, Person t, VoreType voreType)
    {
        if (t.VoreController.HasPrey(voreType) == false)
            t.VoreController.SetPartDigest(voreType, true);
        VoreProgress vore = new VoreProgress(t, a, false, 0, true);
        vore.Willing = true;
        a.GetRelationshipWith(t).VoreWilling();
        t.VoreController.AddPrey(vore, voreType);
        t.StreamingTarget = a;
        switch (voreType)
        {
            case VoreType.Oral:
                t.StreamingAction = InteractionType.OralVore;
                break;
            case VoreType.Anal:
                t.StreamingAction = InteractionType.AnalVore;
                break;
            case VoreType.Unbirth:
                t.StreamingAction = InteractionType.Unbirth;
                break;
            case VoreType.Cock:
                t.StreamingAction = InteractionType.CockVore;
                break;
        }
        a.BeingEaten = true;
        a.Needs.Cleanliness += .1f;

        VoreTrackingRecord VoreRecord = new VoreTrackingRecord(t, a, preyAskedToBeEaten: true, eatenDuringSex: t.EatenDuringSex, willing: vore.Willing);
        GetLocation(t, a, VoreRecord);
        a.VoreTracking.Add(VoreRecord);
    }

    private static void GetLocation(Person a, Person t, VoreTrackingRecord VoreRecord)
    {
        if (a.Position == a.MyRoom) { VoreRecord.EatenIn = "PredBedroom"; }
        else if (t.Position == t.MyRoom) { VoreRecord.EatenIn = "OwnBedroom"; }
        else if (a.ZoneContainsBed()) { VoreRecord.EatenIn = "VacentBedroom"; }
        else if (a.ZoneContainsShower()) { VoreRecord.EatenIn = "Shower"; }
        else if (a.ZoneContainsBathroom()) { VoreRecord.EatenIn = "Bathroom"; }
        else if (a.ZoneContainsFood()) { VoreRecord.EatenIn = "Cafeteria"; }
        else if (a.ZoneContainsGym()) { VoreRecord.EatenIn = "Gym"; }
        else if (a.ZoneContainsLibrary()) { VoreRecord.EatenIn = "Library"; }
        else if (a.ZoneContainsNurseOffice()) { VoreRecord.EatenIn = "NurseOffice"; }
        else { VoreRecord.EatenIn = "Public"; }
    }

    internal static void AskToBeVoredEndo(Person a, Person t, VoreType voreType)
    {
        //if (t.VoreController.HasPrey(voreType) == false)
        t.VoreController.SetPartDigest(voreType, false); //Always cancel to guarantee safety.  
        VoreProgress vore = new VoreProgress(t, a, true, 0, true);
        vore.Willing = true;
        a.GetRelationshipWith(t).VoreWilling();
        t.VoreController.AddPrey(vore, voreType);
        t.StreamingTarget = a;
        switch (voreType)
        {
            case VoreType.Oral:
                t.StreamingAction = InteractionType.OralVore;
                break;
            case VoreType.Anal:
                t.StreamingAction = InteractionType.AnalVore;
                break;
            case VoreType.Unbirth:
                t.StreamingAction = InteractionType.Unbirth;
                break;
            case VoreType.Cock:
                t.StreamingAction = InteractionType.CockVore;
                break;
        }
        a.BeingEaten = true;
        a.Needs.Cleanliness += .1f;

        VoreTrackingRecord VoreRecord = new VoreTrackingRecord(t, a, preyAskedToBeEaten: true, eatenDuringSex: t.EatenDuringSex, willing: vore.Willing);
        GetLocation(t, a, VoreRecord);
        a.VoreTracking.Add(VoreRecord);
    }

    internal static void AskToVore(Person a, Person t, VoreType voreType, bool digest, bool EatenDuringSex = false)
    {
        if (a.VoreController.HasPrey(voreType) == false || digest == false)
        {
            a.VoreController.SetPartDigest(voreType, digest);
        }
        if (a.VoreController.GetProgressOf(t) == null)
        {
            VoreProgress vore = new VoreProgress(a, t, !digest);
            a.VoreController.AddPrey(vore, voreType);
            t.GetRelationshipWith(a).VoreWilling();
            t.BeingEaten = true;
            vore.Willing = true;
            t.Needs.Cleanliness += .1f;

            VoreTrackingRecord VoreRecord = new VoreTrackingRecord(a, t, predAskedToEat: true, eatenDuringSex: EatenDuringSex, willing: vore.Willing);
            GetLocation(a, t, VoreRecord);
            t.VoreTracking.Add(VoreRecord);
        }
        else
        {
            UnityEngine.Debug.Log("I didn't think this was suppose to happen");
        }
    }

    internal static void Vore(Person a, Person t, VoreType voreType, bool EatenDuringSex = false)
    {
        if (a.VoreController.GetProgressOf(t) == null)
        {
            VoreProgress vore = new VoreProgress(a, t, false);
            a.VoreController.AddPrey(vore, voreType);
            if (a.VoreController.TargetIsBeingDigested(t) == false)
            {
                a.VoreController.GetProgressOf(t).ExpectingEndosoma = true;
            }
            t.BeingEaten = true;
            if (t.IsWillingPreyToTarget(a) || (t.HasTrait(Quirks.GutSlut) && t != State.World.ControlledPerson))
            {
                if (t.HasTrait(Quirks.GutSlut))
                    IncreaseFriendship(t, a, .3f);
                var selfAction = SelfActionList.List[SelfActionType.WillingScream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
                vore.Willing = true;
                t.GetRelationshipWith(a).VoreWilling();
            }
            else
            {

                if (a.VoreController.GetProgressOf(t).ExpectingEndosoma)
                {
                    t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).FriendshipLevel, .4f * State.World.Settings.VoreAnger);
                    t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).RomanticLevel, .3f * State.World.Settings.VoreAnger);
                    t.GetRelationshipWith(a).VoreBetrayed();
                }
                else
                {
                    t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).FriendshipLevel, .8f * State.World.Settings.VoreAnger);
                    t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).RomanticLevel, .6f * State.World.Settings.VoreAnger);
                    t.GetRelationshipWith(a).VoreBetrayed();
                }


                var selfAction = SelfActionList.List[SelfActionType.Scream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));

            }
            if (State.World.Settings.FlexibleStats)
                a.Personality.Voracity = Utility.PushTowardOne(a.Personality.Voracity, .05f);
            t.Needs.Cleanliness += .1f;

            VoreTrackingRecord VoreRecord = new VoreTrackingRecord(a, t, eatenDuringSex: EatenDuringSex, willing: vore.Willing);
            GetLocation(a, t, VoreRecord);
            t.VoreTracking.Add(VoreRecord);
        }
        else
        {
            if (a.VoreController.TargetIsBeingDigested(t) && a.VoreController.GetProgressOf(t).ExpectingEndosoma && t.IsWillingPreyToTarget(a) == false)
            {
                if (a.Dead)
                {
                    a.VoreController.GetProgressOf(t).ExpectingEndosoma = true;
                }
                else if (voreType == VoreType.Oral || voreType == VoreType.Anal)
                {
                    if (a.VoreController.GetProgressOf(t).Stage > 0)
                        InteractionList.List[InteractionType.TellPreyTheyWillBeDigested].OnSucceed(a, t, true);
                    InteractionList.List[InteractionType.UnexpectedDigestion].OnSucceed(t, a);
                }
                else
                {
                    if (a.VoreController.GetProgressOf(t).Stage > 0)
                        InteractionList.List[InteractionType.TellPreyTheyWillBeMelted].OnSucceed(a, t, true);
                    InteractionList.List[InteractionType.UnexpectedHeat].OnSucceed(t, a);
                }
            }
            if (a.VoreController.TargetIsBeingDigested(t))
                t.Needs.Cleanliness += .05f;
            else
                t.Needs.Cleanliness += .02f;

        }
    }

    internal static void VoreRefused(Person a, Person t)
    {
        if (t.IsWillingPreyToTarget(a) == false)
        {
            t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).FriendshipLevel, .4f * State.World.Settings.VoreAnger);
            t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).RomanticLevel, .3f * State.World.Settings.VoreAnger);
            t.GetRelationshipWith(a).VoreBetrayed();
        }
    }
    internal static void FreePrey(Person a, Person t, VoreLocation location)
    {
        if (t.VoreController.GetLiving(location) == null)
            return;
        var target = t.VoreController.CurrentSwallow(location);
        if (target == null)
            target = t.VoreController.GetLiving(location);
        IncreaseFriendship(target.Target, a, .4f);
        IncreaseFriendshipSymetric(a, target.Target, .05f, 0);
        DecreaseFriendship(t, a, .15f);
        a.Needs.ChangeEnergy(.03f);
        target.FreePrey(allowFreedMessage: false);
    }

    internal static void FailedFreePrey(Person a, Person t)
    {
        DecreaseFriendship(t, a, .04f);
        a.Needs.ChangeEnergy(.03f);
    }

    internal static void ReleasePrey(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        State.World.EndOfTurnCallBacks.Add(() => a.AI.SpitUpPreyEquivalent(progress));

    }

    internal static void AskIfCanDigest(Person a, Person t)
    {
        a.VoreController.GetProgressOf(t).ExpectingEndosoma = false;
        if (a.VoreController.GetAllProgress(a.VoreController.GetProgressOf(t).Location).Count == 1)
            a.VoreController.SetPartDigest(a.VoreController.GetProgressOf(t).Location, true);
        t.VoreTracking.Last().PredAskedToDigest = true;
    }

    internal static void MovePreyToStomach(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        if (progress == null)
            return;
        a.VoreController.MovePrey(progress, VoreLocation.Stomach);
    }

    internal static void MovePreyToBowels(Person a, Person t)
    {
        var progress = a.VoreController.GetProgressOf(t);
        if (progress == null)
            return;
        a.VoreController.MovePrey(progress, VoreLocation.Bowels);
    }

    internal static void ShoveInPrey(Person a, Person t)
    {
        var progress = t.VoreController.CurrentSwallow(VoreLocation.Any);
        if (progress == null)
            return;
        progress.AdvanceStage();
        IncreaseFriendship(t, a, .04f);
        if (progress.Willing == false)
            DecreaseFriendshipAndRomantic(progress.Target, a, .05f * State.World.Settings.VoreAnger);
    }

    internal static void Taste(Person a, Person t)
    {
        a.Needs.Horniness = Utility.PushTowardOne(a.Needs.Horniness, .05f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .05f);
        t.GetRelationshipWith(a).VoreTaste();
    }

    internal static void Masturbate(Person a, Person t)
    {
        if (a.ClothingStatus == ClothingStatus.Normal)
            a.Needs.Horniness += .02f;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.Needs.Horniness += .025f;
        else
            a.Needs.Horniness += .03f;
        if (a.Needs.Horniness > 1)
        {
            State.World.EndOfTurnCallBacks.Add(() => SelfActionList.List[SelfActionType.Orgasm].OnDo(a));
        }
    }

    internal static void FailedPreyAsk(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TurnsSinceLastAsk = 0;
    }

    internal static void ConvinceToDigest(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = false;
        t.VoreController.CheckIfShouldDigest(t.VoreController.GetProgressOf(a).Location);
        a.VoreTracking.Last().PreyAskedToBeDigested = true;
    }

    internal static void StopDigest(Person a, Person t)
    {
        t.VoreController.SetPartDigest(t.VoreController.GetProgressOf(a).Location, false);
    }
    internal static void Spare(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
        t.VoreController.SetPartDigest(t.VoreController.GetProgressOf(a).Location, false);
        t.VoreController.GetProgressOf(a).ExpectingEndosoma = true;
    }
    internal static void SpareFail(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
    }
    internal static void BegFail(Person a, Person t)
    {
        t.VoreController.GetProgressOf(a).TimesBegged++;
    }
    internal static void Struggle(Person a, Person t)
    {
        if (a.HasTrait(Quirks.SmartStruggle))
            a.Needs.ChangeEnergy(.02f);
        else
            a.Needs.ChangeEnergy(.04f);
        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.PreyEscapes].OnSucceed(t, a));
    }
    internal static void ViolentStruggle(Person a, Person t)
    {
        if (a.HasTrait(Quirks.SmartStruggle))
            a.Needs.ChangeEnergy(.075f);
        else
            a.Needs.ChangeEnergy(.15f);
        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.PreyEscapes].OnSucceed(t, a));
    }


    internal static void PreyEat(Person a, Person t, VoreType voreType, bool PredAskedToEat = false, bool PredAskedToDigest = false, bool PreyAskedToBeEaten = false, bool PreyAskedToBeDigested = false, bool EatenDuringSex = false)
    {
        if (a.VoreController.GetProgressOf(t) == null)
        {
            Person PreviousPred = t.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(t, VoreLocation.Any);
            VoreProgress vore = new VoreProgress(a, t, false);
            a.VoreController.AddPrey(vore, voreType);
            t.BeingEaten = true;
            if (t.IsWillingPreyToTarget(a))
            {
                var selfAction = SelfActionList.List[SelfActionType.WillingScream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
                vore.Willing = true;
                t.GetRelationshipWith(a).VoreWilling();
            }
            else
            {
                t.GetRelationshipWith(a).FriendshipLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).FriendshipLevel, .8f * State.World.Settings.VoreAnger);
                t.GetRelationshipWith(a).RomanticLevel = Utility.PushTowardsNegativeOne(t.GetRelationshipWith(a).RomanticLevel, .6f * State.World.Settings.VoreAnger);
                t.GetRelationshipWith(a).VoreBetrayed();
                var selfAction = SelfActionList.List[SelfActionType.Scream];
                State.World.EndOfTurnCallBacks.Add(() => selfAction.OnDo(t));
            }
            t.Needs.Cleanliness += .1f;

            // VoreTrackingRecord
            t.VoreTracking.Add(new VoreTrackingRecord(a, t, prevPred: PreviousPred, predAskedToEat: PredAskedToEat, predAskedToDigest: PredAskedToDigest, preyAskedToBeEaten: PreyAskedToBeEaten, preyAskedToBeDigested: PreyAskedToBeDigested, eatenDuringSex: EatenDuringSex, preyEatPrey: true, eatenIn: "InsidePred", willing: vore.Willing));
        }
    }
    internal static void PreyAskToVore(Person a, Person t, VoreType voreType, bool digest, bool PredAskedToEat = false, bool PredAskedToDigest = false, bool PreyAskedToBeEaten = false, bool PreyAskedToBeDigested = false, bool EatenDuringSex = false)
    {
        if (a.VoreController.HasPrey(voreType) == false || digest == false)
        {
            a.VoreController.SetPartDigest(voreType, digest);
        }
        if (a.VoreController.GetProgressOf(t) == null)
        {
            Person PreviousPred = t.FindMyPredator();
            PreviousPred.VoreController.RemovePrey(t, VoreLocation.Any);
            VoreProgress vore = new VoreProgress(a, t, !digest);
            a.VoreController.AddPrey(vore, voreType);
            t.BeingEaten = true;
            vore.Willing = true;
            t.GetRelationshipWith(a).VoreWilling();
            a.VoreController.SetPartDigest(voreType, digest);
            t.Needs.Cleanliness += .1f;

            // VoreTrackingRecord
            t.VoreTracking.Add(new VoreTrackingRecord(a, t, prevPred: PreviousPred, predAskedToEat: PredAskedToEat, predAskedToDigest: PredAskedToDigest, preyAskedToBeEaten: PreyAskedToBeEaten, preyAskedToBeDigested: PreyAskedToBeDigested, eatenDuringSex: EatenDuringSex, preyEatPrey: true, eatenIn: "InsidePred", willing: vore.Willing));
        }
    }
    internal static void BreakUp(Person a, Person t)
    {
        a.Romance.Dating = null;
        t.Romance.Dating = null;
    }

    internal static void Strip(Person a, bool asked = false)
    {
        if (asked)
            a.AI.StripTurns = 5;
        if (a.ClothingStatus == ClothingStatus.Normal)
        {
            a.ClothingStatus = ClothingStatus.Underwear;
            a.ClothesInTile = ClothingStatus.Normal;
        }
        else if (a.ClothingStatus == ClothingStatus.Underwear)
        {
            a.ClothingStatus = ClothingStatus.Nude;
            if (a.ClothesInTile != ClothingStatus.Normal)
                a.ClothesInTile = ClothingStatus.Underwear;
        }
    }

    internal static void Clothe(Person a)
    {
        if (a.ClothingStatus == ClothingStatus.Nude)
            a.ClothingStatus = ClothingStatus.Underwear;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.ClothingStatus = ClothingStatus.Normal;
    }

    internal static void Masturbate(Person a)
    {
        if (a.ClothingStatus == ClothingStatus.Normal)
            a.Needs.Horniness += .02f;
        else if (a.ClothingStatus == ClothingStatus.Underwear)
            a.Needs.Horniness += .025f;
        else
            a.Needs.Horniness += .03f;
        if (a.Needs.Horniness > 1)
        {
            SelfActionList.List[SelfActionType.Orgasm].OnDo(a);
        }
    }

    internal static void Exercise(Person a)
    {
        a.Needs.ChangeEnergy(.005f);
        a.Needs.Horniness -= .015f;
        a.GymSatisfaction += 16;

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Personality.Strength = Utility.PushTowardOne(a.Personality.Strength, .0015f);
    }


    internal static void ResearchCommunication(Person a)
    {
        a.LibrarySatisfaction += 16;
        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Personality.Charisma = Utility.PushTowardOne(a.Personality.Charisma, .0005f);
    }

    internal static void BrowseWeb(Person a)
    {
        a.LibrarySatisfaction += 16;
    }

    internal static void StudyArcane(Person a)
    {
        a.LibrarySatisfaction += 16;
        a.Magic.Mana += (0.05f * (a.Magic.ManaRegen + a.Boosts.ManaRegen));

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Magic.Proficiency = Utility.PushTowardOne(a.Magic.Proficiency, .002f);
    }

    internal static void Meditate(Person a)
    {
        a.GymSatisfaction += 16;
        a.Magic.Mana += (0.05f * (a.Magic.ManaRegen + a.Boosts.ManaRegen));

        if (State.World.Settings.FlexibleStats && a.Needs.Energy < 1)
            a.Magic.Willpower = Utility.PushTowardOne(a.Magic.Willpower, .002f);
    }

    internal static void AreaVoreTease(Person a)
    {
        foreach (var target in GetPeopleWithinXSquares(a, 3))
        {
            if (target == State.World.ControlledPerson || target.BeingEaten || target.VoreController.CurrentSwallow(VoreLocation.Any) != null)
                continue;
            if ((target.Position == a.Position || State.World.GetZone(a.Position).Accepts(target)) && target.StreamingTarget == null)
            {

                if (target.VoreController.CouldVoreTarget(a, VoreType.Oral, DigestionAlias.CanVore) || target.VoreController.CouldVoreTarget(a, VoreType.Anal, DigestionAlias.CanVore)
                || target.VoreController.CouldVoreTarget(a, VoreType.Cock, DigestionAlias.CanVore) || target.VoreController.CouldVoreTarget(a, VoreType.Unbirth, DigestionAlias.CanVore))
                {
                    if (target.AI.Desires.DesireToVoreAndDigestTarget(a) > Rand.NextFloat(0.3f, 1.4f) || target.AI.Desires.DesireToForciblyEatAndDigestTarget(a) > Rand.NextFloat(0.3f, 1.4f))
                    {
                        target.AI.HuntDownWillingPrey(a);
                        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.VoreTeased].OnSucceed(target, a));
                    }
                }
            }

        }
    }

    internal static void TreatWounds(Person a)
    {
        a.Health += Constants.NurseHeal;
        if (a.Health > Constants.HealthMax)
            a.Health = Constants.HealthMax;
    }

    internal static void Orgasm(Person a)
    {
        if (a.ActiveSex != null) 
        {
            if (a.ActiveSex.Other.HasTrait(Quirks.ManaVampire) || a.HasTrait(Quirks.ManaDonor))
                a.ActiveSex.Other.Magic.Mana += 1;
            if (a.ActiveSex.Other.HasTrait(Quirks.ManaVampire))
                a.Magic.Mana -= 1;
        }
        if (a.HasTrait(Quirks.Insatiable))
            a.Needs.Horniness = .5f;
        else
            a.Needs.Horniness = .2f;
        a.Needs.Cleanliness += .12f;
        a.TurnsSinceOrgasm = 0;
    }

    internal static void EndSex(Person a, Person t)
    {
        a.EndStreamingActions();
        t.EndStreamingActions();
    }

    internal static void MakeOut(Person a, Person t)
    {
        IncreaseRomanticSymetric(a, t, .01f, .01f);
        a.Needs.Horniness = Utility.PushTowardOne(a.Needs.Horniness, .01f);
        t.Needs.Horniness = Utility.PushTowardOne(t.Needs.Horniness, .01f);
    }

    internal static void AskRubBelly(Person a, Person t)
    {
        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.RubBelly].OnSucceed(t, a));
    }
    internal static void AskRubBalls(Person a, Person t)
    {
        State.World.EndOfTurnCallBacks.Add(() => InteractionList.List[InteractionType.RubBalls].OnSucceed(t, a));
    }

    internal static void SexEvent(Person a, Person t, float relationship, float horniness, float cleanliness)
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horniness;
        t.Needs.Horniness += horniness;
        a.Needs.Cleanliness += cleanliness;
        t.Needs.Cleanliness += cleanliness;
    }

    internal static void Breastfeed(Person a, Person t, float relationship, float horniness, float hunger)
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horniness;
        t.Needs.Horniness += horniness;
        t.Needs.Hunger -= hunger;

        if (t.HasTrait(Quirks.ManaVampire) || a.HasTrait(Quirks.ManaDonor))
            t.Magic.Mana += 0.05f;
        if (t.HasTrait(Quirks.ManaVampire))
            a.Magic.Mana -= 0.05f;
    }

    internal static void SexEventSplit(Person a, Person t, float relationship, float horninessA, float horninessT, float cleanlinessA, float cleanlinessT)
    {
        IncreaseRomanticSymetric(a, t, relationship, relationship);
        a.Needs.Horniness += horninessA;
        t.Needs.Horniness += horninessT;
        a.Needs.Cleanliness += cleanlinessA;
        t.Needs.Cleanliness += cleanlinessT;
    }

    internal static void SwitchPosition(Person a, Person t, SexPosition position)
    {
        a.ActiveSex.Position = position;
        t.ActiveSex.Position = position;
        a.ActiveSex.LastPositionChange = 0;
        t.ActiveSex.LastPositionChange = 0;
    }

    internal static void SwitchPosition(Person a, Person t, SexPosition actorPos, SexPosition targetPos)
    {
        a.ActiveSex.Position = actorPos;
        t.ActiveSex.Position = targetPos;
        a.ActiveSex.LastPositionChange = 0;
        t.ActiveSex.LastPositionChange = 0;
    }

    internal static void Dispose(Person a, VoreLocation location)
    {
        DisposalData disp;
        if (location == VoreLocation.Stomach)
            disp = a.Disposals.Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels).FirstOrDefault();
        else
            disp = a.Disposals.Where(s => s.Location == location).FirstOrDefault();
        if (disp == null)
            return;
        State.World.EndOfTurnCallBacks.Add(() =>
        {
            disp.TurnsDone++;
            if (disp.TurnsDone > 3)
            {
                a.EndStreamingActions();
                a.Disposals.Remove(disp);
            }
        });
    }

    // TARGET SPELLS

    internal static void CastDisrobe(Person a, Person t)
    {
        if (t.ClothingStatus == ClothingStatus.Normal)
        {
            if (a.Magic.Potency > 0.4)
                t.ClothingStatus = ClothingStatus.Nude;
            else
                t.ClothingStatus = ClothingStatus.Underwear;
        }
        else if (t.ClothingStatus == ClothingStatus.Underwear)
        {
            t.ClothingStatus = ClothingStatus.Nude;
        }

        CastDecreaseFriendship(t, a, 0.3f);
    }

    internal static void CastHeal(Person a, Person t)
    {
        t.Health += (Constants.HealthMax / 2) + ((int)a.Magic.Potency * (Constants.HealthMax / 2));

        if (t.BeingEaten == false || t.FindMyPredator().VoreController.GetProgressOf(t).Willing || t.FindMyPredator().VoreController.AreSisterPrey(t, a))
            IncreaseFriendship(t, a, 0.2f);
    }

    internal static void CastArouse(Person a, Person t)
    {
        float boost = 0.4f + (a.Magic.Potency * 0.6f);
        
        if (a.Romance.CanSafelyRomance(t) && (t.BeingEaten == false || t.FindMyPredator().VoreController.AreSisterPrey(t, a)))
            IncreaseRomantic(t, a, (0.1f + boost/5));

        if (t.Needs.Horniness > 0.9f)
            SelfActionList.List[SelfActionType.Orgasm].OnDo(t);
        else if (t.Needs.Horniness + boost > 0.95f)
            t.Needs.Horniness = 0.95f;
        else
            t.Needs.Horniness += boost;
    }

    internal static void CastCharm(Person a, Person t)
    {
        float boost = 0.2f + (a.Magic.Potency * 0.1f);

        IncreaseFriendship(t, a, boost);
        if (a.Romance.CanSafelyRomance(t))
            IncreaseRomantic(t, a, boost);
    }

    internal static void CastGrow(Person a, Person t)
    {
        // MODIFY HEIGHT/WEIGHT IF CHAR NOT ALREADY AFFECTED
        // HEIGHT/WEIGHT RESETTING HAPPENS IN Magic.cs
        if (t.Magic.Duration_Big1 == 0)
        {
            t.PartList.Height *= State.World.Settings.SizeChangeMod;
            t.PartList.Weight *= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.JustGrew].OnDo(t);
        }
        else if (t.Magic.Duration_Big2 == 0)
        {
            t.PartList.Height *= State.World.Settings.SizeChangeMod;
            t.PartList.Weight *= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.JustGrew].OnDo(t);
        }

        // INCREASES DURATION OF EFFECT
        int boost = (int)((12 + (a.Magic.Potency * 8)) * State.World.Settings.SpellDurationMod);

        if (t.Magic.Duration_Small2 > 0)
        {
            t.Magic.Duration_Small2 = 0;
            SelfActionList.List[SelfActionType.ResetShrink].OnDo(t);
        }
        else if (t.Magic.Duration_Small1 > 0)
        {
            t.Magic.Duration_Small1 = 0;
            SelfActionList.List[SelfActionType.ResetShrink].OnDo(t);
        }
        else if (t.Magic.Duration_Big1 > 10)
            t.Magic.Duration_Big2 += boost;
        else
            t.Magic.Duration_Big1 += boost;

        if (a != t && (t.BeingEaten == false || t.FindMyPredator().VoreController.AreSisterPrey(t, a)))
            IncreaseFriendship(t, a, 0.1f);
    }

    internal static void CastShrink(Person a, Person t)
    {
        // MODIFY HEIGHT/WEIGHT IF CHAR NOT ALREADY AFFECTED
        // HEIGHT/WEIGHT RESETTING HAPPENS IN Magic.cs
        if (t.Magic.Duration_Small1 == 0)
        {
            t.PartList.Height /= State.World.Settings.SizeChangeMod;
            t.PartList.Weight /= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.JustShrunk].OnDo(t);
        }
        else if (t.Magic.Duration_Small2 == 0)
        {
            t.PartList.Height /= State.World.Settings.SizeChangeMod;
            t.PartList.Weight /= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.JustShrunk].OnDo(t);
        }

        // INCREASES DURATION OF EFFECT
        int boost = (int)((12 + (a.Magic.Potency * 8)) * State.World.Settings.SpellDurationMod);

        if (t.Magic.Duration_Big2 > 0) 
        {
            t.Magic.Duration_Big2 = 0;
            SelfActionList.List[SelfActionType.ResetGrow].OnDo(t); 
        }
        else if (t.Magic.Duration_Big1 > 0)
        {
            t.Magic.Duration_Big1 = 0;
            SelfActionList.List[SelfActionType.ResetGrow].OnDo(t);
        }
        else if (t.Magic.Duration_Small1 > 10)
            t.Magic.Duration_Small2 += boost;
        else
            t.Magic.Duration_Small1 += boost;

        if (a != t && t.Personality.PreyWillingness < State.World.Settings.WillingThreshold && (t.BeingEaten == false || t.FindMyPredator().VoreController.AreSisterPrey(t, a)))
            CastDecreaseFriendship(t, a, 0.3f);
    }

    internal static void CastFreeze(Person a, Person t)
    {
        // INCREASES DURATION OF EFFECT
        int boost = (int)((4 + (a.Magic.Potency * 4)) * State.World.Settings.SpellDurationMod);
        t.Magic.Duration_Freeze += boost;

        CastDecreaseFriendship(t, a, 0.3f);
    }

    internal static void CastHunger(Person a, Person t)
    {
        float boost = 0.4f + (a.Magic.Potency * 0.6f);

        if (t.Needs.Hunger > 0.9f && t != State.World.ControlledPerson && t.VoreController.CapableOfVore())
        {
            // If target is AI, target Hunger is high already, and Hunger spell is cast again...
            // Gives long duration hidden "Unnatural Hunger" status effect which drastically raises vore odds and prevents AI from seeking cafeteria
            // Also has a chance of causing them to immediately hunt
            t.Needs.Hunger += boost/8;
            t.AI.HuntDownPrey();
            if (t.Magic.Duration_Hunger <= 20 + (int)(20 * boost))
                t.Magic.Duration_Hunger = 20 + (int)(20 * boost);
        }
        else if (t.Needs.Hunger + boost > 0.95f)
            t.Needs.Hunger = 0.95f;
        else
            t.Needs.Hunger += boost;
    }

    // SELF SPELLS

    internal static void CastPassdoor(Person a)
    {
        // INCREASES DURATION OF EFFECT
        int boost = (int)((7 + (a.Magic.Potency * 5)) * State.World.Settings.SpellDurationMod);
        a.Magic.Duration_Passdoor += boost;
    }

    internal static void CastHeal(Person a)
    {
        a.Health += (Constants.HealthMax / 2) + ((int)a.Magic.Potency * (Constants.HealthMax / 2));
    }

    internal static void CastGrow(Person a)
    {
        CastGrow(a, a);
    }

    internal static void CastShrink(Person a)
    {
        CastShrink(a, a);
    }

}

