﻿using GeneratedSexInteractions;
using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public enum SexPosition
{
    Standing,
    LyingDown,
    KneelingGiving,
    KneelingReceiving,
    SixtyNine,
    Missionary,
    BehindGiving,
    BehindReceiving,
    Scissoring

}

public class ActiveSex
{
    [OdinSerialize]
    internal Person Self;
    [OdinSerialize]
    internal Person Other;

    [OdinSerialize]
    internal SexPosition Position;
    [OdinSerialize]
    internal bool Receiving;

    [OdinSerialize]
    internal int LastPositionChange;

    [OdinSerialize]
    internal int Turns;

    //public bool Involves (Person person)
    //{
    //    return person == Self || person == Other;
    //}

    public ActiveSex(Person self, Person other)
    {
        Self = self;
        Other = other;
        Position = SexPosition.Standing;
        Receiving = false;
        LastPositionChange = 2;
    }

    internal void EndSex()
    {
        Self.EndStreamingActions();
        Other.EndStreamingActions();
    }

    public bool Scissoring => Position == SexPosition.Scissoring;

    public bool Penetrating => Position == SexPosition.Missionary || Position == SexPosition.BehindGiving;

    public bool BeingPenetrated => Position == SexPosition.Missionary || Position == SexPosition.BehindReceiving;




}

