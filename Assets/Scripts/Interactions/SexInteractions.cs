﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HelperFunctions;

public enum SexInteractionType
{
    StripSelf,
    StripThem,
    RubHair,
    CaressFace,
    Massage,
    DeepKiss,
    FondleBreasts,
    KissBreasts,
    FondleButt,
    KissBelly,
    KissBalls,
    BellySmother,
    BallsSmother,
    BreastsSmother,
    ButtSmother,
    Finger,
    Stroke,
    BlowJob,
    Cunnilingus,
    SelfMasturbate,
    Intercourse,
    Frotting,
    Navelfuck,
    Rimjob,
    Breastfeed,
    KissFeet,
    FootSmother,
    Footjob,
    KissArmpit,
    ArmpitSmother,
    SwitchPositionToStanding = 50,
    SwitchPositionToLyingDown,
    SwitchPositionToKneelingGiving,
    SwitchPositionToKneelingReceiving,
    SwitchPositionToSixtyNine,
    SwitchPositionToMissionary,
    SwitchPositionToBehindGiving,
    SwitchPositionToBehindReceiving,
    SwitchPositionToScissoring,
    EndSex = 70,
    KissVore = 90,
    SexAskToOralVore,
    SexAskToOralVoreDigest,
    SexUnbirth,
    SexAskToUnbirth,
    SexAskToUnbirthDigest,
    SexCockVore,
    SexAskToCockVore,
    SexAskToCockVoreDigest,
    SexAnalVore,
    SexAskToAnalVore,
    SexAskToAnalVoreDigest,
    SexAskToBeOralVored,
    SexAskToBeOralVoredEndo,
    SexAskToBeUnbirthed,
    SexAskToBeUnbirthedEndo,
    SexAskToBeCockVored,
    SexAskToBeCockVoredEndo,
    SexAskToBeAnalVored,
    SexAskToBeAnalVoredEndo,

}

static class SexInteractionList
{
    static internal SortedDictionary<SexInteractionType, SexInteractionBase> List;

    static internal SexInteractionBase GetRandomAvailable(Person actor, Person target)
    {
        List<SexInteractionBase> possibilities = new List<SexInteractionBase>();
        foreach (SexInteractionBase interaction in List.Values.Where(s => s.Type < SexInteractionType.SwitchPositionToStanding))
        {
            if (interaction.AppearConditional(actor, target) && interaction.PositionAllows(actor))
            {
                possibilities.Add(interaction);
            }
        }
        if (possibilities.Count == 0)
        {
            UnityEngine.Debug.Log("No sex possibilities");
            return List[SexInteractionType.DeepKiss];
        }
        return possibilities[Rand.Next(possibilities.Count)];
    }

    static internal SexInteractionBase GetRandomPosition(Person actor, Person target)
    {
        List<SexInteractionBase> possibilities = new List<SexInteractionBase>();
        foreach (SexInteractionBase interaction in List.Values.Where(s => s.Type >= SexInteractionType.SwitchPositionToStanding && s.Type <= SexInteractionType.SwitchPositionToScissoring))
        {
            if (interaction.AppearConditional(actor, target))
            {
                possibilities.Add(interaction);
            }
        }
        if (possibilities.Count == 0)
        {
            UnityEngine.Debug.Log("No stance possibilities");
            return List[SexInteractionType.DeepKiss];
        }
        int pick = Rand.Next(possibilities.Count);
        for (int i = 0; i < 5; i++)
        {
            if (Rand.Next(40) < Math.Min(actor.ActiveSex.Turns, 30))
            {
                pick++;
            }
        }
        return possibilities[Math.Min(pick, possibilities.Count - 1)];
    }

    static SexInteractionList()
    {
        var test = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
               .Where(x => typeof(SexInteractionBase).IsAssignableFrom(x) && !x.IsAbstract);
        List = new SortedDictionary<SexInteractionType, SexInteractionBase>();
        foreach (var obj in test)
        {
            SexInteractionBase instance = (SexInteractionBase)Activator.CreateInstance(obj);
            List[instance.Type] = instance;
        }


        //#warning temp code
        //        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //        foreach (var interaction in List)
        //        {
        //            var i = interaction.Value;
        //            sb.AppendLine($"{i.Name}#{i.Description}#{i.Type}#{i.SoundRange}");
        //        }
        //        System.IO.File.WriteAllText(UnityEngine.Application.dataPath + "\\testSexActions.csv", sb.ToString());

    }
}

class SexInteractionEventString
{
    internal Func<SexInteraction, string> GetString;
    internal Predicate<SexInteraction> Conditional;
    internal int Priority;

    public SexInteractionEventString(Func<SexInteraction, string> getString, int priority = 0, Predicate<SexInteraction> conditional = null)
    {
        Priority = priority;

        GetString = getString;
        if (conditional == null)
            Conditional = (s) => true;
        else
            Conditional = conditional;
    }
}


abstract class SexInteractionBase
{
    internal string Name = "Unnamed Action";
    internal string Description = "";
    internal SexInteractionType Type;
    internal int SoundRange = -1;
    //internal bool Streaming = false;
    //internal int MaxStreamLength = 999;
    internal Func<Person, Person, bool> AppearConditional = null;
    internal ClassType Class = ClassType.Friendly;
    internal Action<Person, Person> Effect = null;
    internal List<SexPosition> AllowedPositions;

    internal bool PositionAllows(Person actor)
    {
        if (AllowedPositions == null || AllowedPositions.Count == 0)
            return true;
        if (actor.ActiveSex == null)
            return false;
        return AllowedPositions.Contains(actor.ActiveSex.Position);
    }

    internal void OnDo(Person actor, Person target)
    {
        DebugManager.Log(Type);
        if (Type == SexInteractionType.EndSex)
        {
            var record = new Record(new SexInteraction(actor, target, Type));
            actor.AddEvent(record, Type);
            target.AddEvent(record, Type);
            CreateWitnesses(record, actor, target);
            Effect(actor, target);
        }
        else
        {
            if (Type >= SexInteractionType.KissVore && Type <= SexInteractionType.SexAskToBeAnalVoredEndo)
            { target.EatenDuringSex = true; }

            Effect(actor, target);
            var record = new Record(new SexInteraction(actor, target, Type));
            actor.AddEvent(record, Type);
            target.AddEvent(record, Type);
            CreateWitnesses(record, actor, target);
        }

    }

    void CreateWitnesses(Record record, Person actor, Person target)
    {
        var HeardSquares = Sound.GetAffectedSquares(actor.Position, SoundRange);
        foreach (Person person in State.World.GetPeople(true))
        {
            if (person == actor || person == target)
                continue;
            if (person.BeingEaten == false)
            {
                if (LOS.Check(person.Position, actor.Position))
                {
                    person.WitnessEvent(record, Type);
                    if (person.Dead)
                        continue;
                    if (person.Romance.DesiresGender(actor.GenderType) || person.Romance.DesiresGender(target.GenderType))
                    {
                        if (person.HasTrait(Quirks.Prude) == false)
                        {
                            person.Needs.Horniness += .02f;
                            SelfActionList.List[SelfActionType.TurnedOnBySex].OnDo(person);
                        }
                        else
                        {
                            var rel = person.GetRelationshipWith(actor);
                            rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, .01f);
                            rel = person.GetRelationshipWith(target);
                            rel.FriendshipLevel = Utility.PushTowardsNegativeOne(rel.FriendshipLevel, .01f);
                        }

                    }

                    if (person.Personality.CheatAcceptance != CheatingAcceptance.Everything && (person.Romance.Dating == target || person.Romance.Dating == target))
                    {
                        if (person != State.World.ControlledPerson)
                            InteractionList.List[InteractionType.OutragedAtSex].OnSucceed(person, person.Romance.Dating);
                    }
                    continue;
                }
            }
            foreach (var square in HeardSquares)
            {
                if (person.Position.x == square.x && person.Position.y == square.y)
                {
                    person.WitnessEvent(record, Type);
                    if (person.Personality.CheatAcceptance != CheatingAcceptance.Everything && (person.Romance.Dating == target || person.Romance.Dating == target))
                    {
                        //I've waffled back and forth on whether this should be a lesser penalty since it's more difficult to confirm.
                        if (person != State.World.ControlledPerson && person.Dead == false)
                            InteractionList.List[InteractionType.OutragedAtSex].OnSucceed(person, person.Romance.Dating);
                    }
                    continue;
                }
            }
            if (Config.DebugViewAllEvents && (person == State.World.ControlledPerson || (State.World.PlayerIsObserver() && person == State.World.VisionAttachedTo)))
            {
                person.WitnessEvent(record, Type);
                continue;
            }
        }

    }

}




