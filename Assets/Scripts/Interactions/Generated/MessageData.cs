using Assets.Scripts.TextGeneration;
using OdinSerializer;
using System.Collections.Generic;
using static TextGenerator;

class MessageData
{
    internal Dictionary<SexInteractionType, List<GenericEventString<SexInteraction>>> SexTexts;
    internal Dictionary<SelfActionType, List<GenericEventString<SelfAction>>> SelfTexts;
    internal Dictionary<InteractionType, List<GenericEventString<Interaction>>> InteractionTexts;
    internal Dictionary<VoreMessageType, List<GenericEventString<Interaction>>> VoreTexts;
    internal Dictionary<BodyPartDescriptionType, List<GenericEventString<Interaction>>> BodyPartTexts;

    public MessageData()
    {
        DialogueReader.DialogueReader reader = new DialogueReader.DialogueReader();
        reader.Read(this);
    }
}


