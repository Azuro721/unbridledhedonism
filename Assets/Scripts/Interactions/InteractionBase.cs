﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HelperFunctions;

internal enum ClassType
{
    Friendly,
    Unfriendly,
    Self,
    Romantic,
    Vore,
    VoreAskThem,
    VoreAskToBe,
    VoreConsuming,
    VoreUnwilling,
    Disposal,
    Event,
    CastTarget,
    CastSelf,
}

abstract class InteractionBase
{

    internal string Name = "Unnamed Action";
    internal string Description = "";
    internal InteractionType Type;
    internal int Range = 0;
    internal int SoundRange = -1;
    internal bool Streaming = false;
    internal string StreamingDescription = "";
    internal string AsksPlayerDescription = "";
    internal bool AsksPlayer = false;
    internal bool UsedOnPrey = false;
    internal bool Hostile = false;
    internal ClassType Class = ClassType.Friendly;
    internal Func<Person, Person, bool> AppearConditional = null;
    internal Func<Person, Person, float> SuccessOdds = null;
    internal Func<Person, Person, string> RomanticStatusCheck = null;
    internal Action<Person, Person> SuccessEffect = null;
    internal Action<Person, Person> FailEffect = null;

    internal bool RunCheck(Person actor, Person target, bool cleared = false, bool displayText = true, bool interruptOnFail = true)
    {
        DebugManager.Log(Type);
        if (Type != InteractionType.Meet && Type != InteractionType.PreyMeet)
            actor.QuickMeet(target, Hostile);
        if (cleared == false) //Dropped this in here as a backup way since I very frequently didn't use the AI Use interaction method
        {
            if (AsksPlayer && State.World.ControlledPerson == target)
            {
                State.World.AskPlayer(actor, this);
                return true;
            }
            else if (Class == ClassType.CastTarget && State.World.ControlledPerson == target && State.World.TestingMode == false)
            {
                State.World.AskPlayerWillingMagic(actor, this);
                return true;
            }
            else if (Class == ClassType.VoreConsuming && State.World.ControlledPerson == target && State.World.TestingMode == false)
            {
                State.World.AskPlayerWillingPrey(actor, this);
                return true;
            }
        }

        if (Class == ClassType.Romantic)
        {
            var status = GetRomanticRejections(actor, target);
            if (status != "")
            {
                Record record = new Record(new SimpleString(actor, target, status));
                OnFail(actor, target, interruptOnFail, createWitnesses: false); 
                if (displayText)
                {
                    actor.AddEvent(record);
                    target.AddEvent(record);
                }
                CreateWitnesses(record, actor, target, status, false, displayText: displayText);
                return false;
            }
        }

        if (Streaming && Hostile == false && (target.ActiveSex != null || target.VoreController.CurrentSwallow(VoreLocation.Any) != null))
        {
            OnFail(actor, target, interruptOnFail);
            string status = $"{target.FirstName} seems a little too busy at the moment to accept that request";
            Record record = new Record(new SimpleString(actor, target, status));
            if (displayText)
            {
                actor.AddEvent(record);
                target.AddEvent(record);
            }
            CreateWitnesses(record, actor, target, status, false);
            return false;
        }

        if (SuccessOdds(actor, target) >= Rand.NextDouble())
        {
            OnSucceed(actor, target, displayText: displayText);
            return true;
        }
        else
        {
            OnFail(actor, target, displayText: displayText, interruptOnFail);
            return false;
        }
    }

    internal void OnSucceed(Person actor, Person target, bool continued = false, int stage = 0, bool ignoreStream = false, bool displayText = true)
    {
        if (Class == ClassType.Event && actor.Dead)
        {
            return;
        }
        if (continued == false && Class != ClassType.Event)
        {
            actor.EndStreamingActions();
        }
        SuccessEffect?.Invoke(actor, target);
        Record record = new Record(new Interaction(actor, target, true, Type, stage));
        if (displayText)
        {
            if (record.ToString() != "{SUPPRESS}")
            {
                bool denied = CheckForSuppressedVore(actor, target, true);
                actor.AddEvent(record);
                if (denied == false)
                {                    
                    target.AddEvent(record);
                }
            }
        }
        if (Class == ClassType.Friendly || Class == ClassType.Romantic)
        {
            if (target.AI.LastInteractedWith == null)
                target.AI.LastInteractedWith = actor;
        }
        if (Class == ClassType.CastTarget && actor.HasTrait(Quirks.LimitlessMagic) == false)
        {
            actor.Magic.Mana -= 1;
        }
        CreateWitnesses(record, actor, target, true, stage, displayText: displayText);
        if (Streaming && ignoreStream == false)
        {
            if (target.VoreController.CurrentSwallow(VoreLocation.Any) != null || (target.ActiveSex != null && Type != InteractionType.StartSex))
                target.EndStreamingActions();
            if (continued == false)
            {
                actor.StreamedTurns = 0;
                target.StreamedTurns = 0;
            }
            else
            {
                DebugManager.Log(Type);
                actor.StreamedTurns += 1;
            }
            actor.StreamingAction = Type;
            actor.StreamingTarget = target;
            if (InteractionList.List[Type].Class != ClassType.VoreConsuming)
            {
                target.StreamingAction = Type;
                target.StreamingTarget = actor;
            }

        }
    }

    internal void OnFail(Person actor, Person target, bool displayText = true, bool interruptOnFail = true, bool createWitnesses = true)
    {
        if (interruptOnFail)
            actor.EndStreamingActions();
        FailEffect?.Invoke(actor, target);
        Record record = new Record(new Interaction(actor, target, false, Type));
        if (displayText)
        {
            bool denied = CheckForSuppressedVore(actor, target, false);
            actor.AddEvent(record);
            if (denied == false)
            {               
                target.AddEvent(record);
            }
        }
        if (Class == ClassType.CastTarget && State.World.Settings.FizzleMana && actor.HasTrait(Quirks.LimitlessMagic) == false)
        {
            actor.Magic.Mana -= 1;
        }
        if (createWitnesses)
            CreateWitnesses(record, actor, target, false, displayText: displayText);
        if (Class == ClassType.Romantic)
        {
            RomanticRejection(actor, target);
        }
    }

    bool CheckForSuppressedVore(Person actor, Person target, bool success)
        {        
            //This is only for actions that directly involve one of the participants, witnesses are handled in Person.cs
            if (InteractionList.List[Type].Class != ClassType.Vore)
                return false;
            switch (Type)
            {               
                case InteractionType.PreyBeg:
                case InteractionType.PreyStruggle:
                case InteractionType.PreyViolentStruggle:
                    if (success || actor == State.World.ControlledPerson)
                        return false;
                    break;

                case InteractionType.PreyWillingYell:
                case InteractionType.PreyWillingSquirm:
                case InteractionType.PreyWillingMasturbate:
                case InteractionType.PreyWillingBellyRub:
                case InteractionType.PreyWillingWombRub:
                case InteractionType.PreyWillingBallsRub:
                case InteractionType.PreyWillingAnusRub:
                case InteractionType.PreyWillingNap:
                case InteractionType.PreyWait:
                case InteractionType.PreyRecover:
                case InteractionType.PreyMasturbate:
                case InteractionType.PreyMeet:          
                    break;

                default:
                    return false;
            }
            return Config.SuppressVoreMessages > Rand.NextFloat(0, 1);
        }

    void CreateWitnesses(Record record, Person actor, Person target, bool success, int stage = 0, bool displayText = true)
    {
        var HeardSquares = Sound.GetAffectedSquares(actor.Position, SoundRange);
        foreach (Person person in State.World.GetPeople(true))
        {
            if (person == actor || person == target)
                continue;
            if (Config.DebugViewAllEvents && (person == State.World.ControlledPerson || (State.World.PlayerIsObserver() && person == State.World.VisionAttachedTo)))
            {
                person.WitnessEvent(record, actor, target, Type, false, success, displayText: displayText);
                continue;
            }
            if (person.BeingEaten == false)
            {
                if (LOS.Check(person.Position, actor.Position))
                {
                    person.WitnessEvent(record, actor, target, Type, false, success, displayText: displayText);
                    SawCheating(actor, target, person, success);
                    continue;
                }
            }
            foreach (var square in HeardSquares)
            {
                if (person.Position.x == square.x && person.Position.y == square.y)
                {
                    person.WitnessEvent(record, actor, target, Type, true, success, displayText: displayText);
                    SawCheating(actor, target, person, success);
                    continue;
                }
            }
        }

    }

    void CreateWitnesses(Record record, Person actor, Person target, string text, bool success, bool displayText = true)
    {
        var HeardSquares = Sound.GetAffectedSquares(actor.Position, SoundRange);
        foreach (Person person in State.World.GetPeople(true))
        {
            if (person == actor || person == target)
                continue;
            if (person.BeingEaten == false)
            {
                if (LOS.Check(person.Position, actor.Position))
                {
                    person.WitnessEvent(record, displayText: displayText);
                    SawCheating(actor, target, person, success);
                    continue;
                }
            }

            foreach (var square in HeardSquares)
            {
                if (person.Position.x == square.x && person.Position.y == square.y)
                {
                    person.WitnessEvent(record, displayText: displayText);
                    SawCheating(actor, target, person, success);
                    continue;
                }
            }
            if (Config.DebugViewAllEvents && (person == State.World.ControlledPerson || (State.World.PlayerIsObserver() && person == State.World.VisionAttachedTo)))
            {
                person.WitnessEvent(record, displayText: displayText);
                continue;
            }
        }

    }

    private void SawCheating(Person actor, Person target, Person person, bool success)
    {
        if (person.Dead)
            return;
        if (person.HasTrait(Traits.Possessive))
        {
            if (person.Romance.Dating == actor)
                person.GetRelationshipWith(target).Vendetta = true;
            else if (person.Romance.Dating == target)
                person.GetRelationshipWith(actor).Vendetta = true;
        }
        if (success)
            SawCheatingSucceedCheck(actor, target, person);
        else
            SawCheatingFailCheck(actor, target, person);                  
    }

    private void SawCheatingFailCheck(Person actor, Person target, Person person)
    {
        if (Class == ClassType.Romantic && person.Personality.CheatAcceptance == CheatingAcceptance.None && (person.Romance.Dating == actor || person.Romance.Dating == target))
        {
            if (person.Romance.Dating == actor)
            {
                SelfActionList.List[SelfActionType.SawAttemptedCheating].OnDo(person);
                DecreaseFriendshipAndRomantic(person, actor, .075f);
                DecreaseFriendshipAndRomantic(person, target, .050f);
            }
            if (person.Romance.Dating == target)
            {
                SelfActionList.List[SelfActionType.SawSORejectCheating].OnDo(person);
                DecreaseFriendshipAndRomantic(person, actor, .075f);
            }
        }
    }

    private void SawCheatingSucceedCheck(Person actor, Person target, Person person)
    {
        if (Class == ClassType.Romantic && person.Personality.CheatAcceptance == CheatingAcceptance.None && (person.Romance.Dating == actor || person.Romance.Dating == target))
        {
            SelfActionList.List[SelfActionType.SawCheating].OnDo(person);
            DecreaseFriendshipAndRomantic(person, actor, .125f);
            DecreaseFriendshipAndRomantic(person, target, .125f);
        }
    }

    public string GetRomanticRejections(Person actor, Person target)
    {

        if (Type == InteractionType.StartSex)
        {
            if (InPrivateRoom(target) == false && target.Personality.Promiscuity < .5f)
                return $"{actor.FirstName}, \"I'm not promiscous enough to do that here, we should go to a private space\" said {target.FirstName}";
        }
        if (target.Romance.DesiresGender(actor.GenderType) == false && target.GetRelationshipWith(actor).FriendshipLevel > 0.4f)
        {
            Relationship actorToTarget = actor.GetRelationshipWith(target);
            actorToTarget.KnowledgeAbout.KnowsOrientation = true;
            return $"{actor.FirstName}, \"Sorry, I'm {target.Romance.Orientation}, so it's not going to work out\" said {target.FirstName}";
        }
        if (target.Romance.Dating != null && target.Romance.Dating != actor && target.Personality.CheatOnPartner == ThreePointScale.Never)
        {
            return $"\"{actor.FirstName}, sorry, I'm dating {target.Romance.Dating.FirstName}, and I'm not going to cheat on {GPP.Him(target.Romance.Dating)}.\" said {target.FirstName}";
        }
        return "";
    }



}


