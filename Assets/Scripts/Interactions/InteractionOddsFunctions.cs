﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

static class InteractionOddsFunctions
{

    public static float FriendshipCheckOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        float successOdds = .25f + (actor.Personality.Charisma * 0.4f) + (target.GetRelationshipWith(actor).FriendshipLevel * 0.75f) - difficulty;
        if ((interaction.Type == InteractionType.Talk) && target.HasTrait(Traits.Talkative))
            successOdds += 0.3f;
        if ((interaction.Type == InteractionType.FriendlyHug) && target.HasTrait(Traits.LovesHugs))
            successOdds *= 1.3f;
        if (interaction.Type == InteractionType.AskToEnter)
            successOdds += (target.GetRelationshipWith(actor).RomanticLevel * 0.3f);
        if ((interaction.Type == InteractionType.AskToEnter) && (target.ActiveSex != null || target.StreamingSelfAction == SelfActionType.Masturbate) && (target.Personality.Promiscuity < 0.8 || target.HasTrait(Traits.LovesPrivateSex) || target.HasTrait(Quirks.Prude)) && target.HasTrait(Traits.Exhibitionist) == false)
            successOdds = 0;
        if (interaction.Type == InteractionType.AskToEnter && target.Romance.Dating == actor)
            successOdds = 1;
        
        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float BellyRubAskOdds(Person actor, Person target, float difficulty)
    {
        float worshipMod = 0;
        float successOdds;
        Relationship relationship = target.GetRelationshipWith(actor);
        if (target.HasTrait(Quirks.PredWorship))
            worshipMod += 0.5f;
        if (target.HasTrait(Traits.BellyFixation))
            worshipMod += 0.3f;
        if (actor.HasTrait(Traits.Intimidating))
            successOdds = 0.25f + worshipMod + (actor.Personality.Strength * 0.5f) + ((1 - target.Personality.Dominance) * 0.2f) + (relationship.GetRomanticLevelToTarget()) + (relationship.FriendshipLevel * 0.2f) + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.2f)) - 0.3f - difficulty;
        else
            successOdds = worshipMod + (actor.Personality.Charisma * 0.2f) + ((1 - target.Personality.Dominance) * 0.2f) + (relationship.GetRomanticLevelToTarget()) + (relationship.FriendshipLevel * 0.2f) + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.2f)) - 0.3f - difficulty;
        
        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float BellyRubOdds(Person actor, Person target, float difficulty)
    {
        float worshipMod = 0;
        float successOdds;
        Relationship relationship = target.GetRelationshipWith(actor);
        if (target.HasTrait(Traits.BellyFixation))
            worshipMod += 0.3f;
        if (target.HasTrait(Traits.DemandsWorship))
            return 1;
        else
            successOdds = (actor.Personality.Charisma * 0.2f) + (relationship.GetRomanticLevelToTarget()) + (relationship.FriendshipLevel * 0.2f) + (target.Personality.Voraphilia * (target.Needs.Horniness + 0.5f)) + worshipMod - difficulty;
        
        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }
    
    public static float StrengthCheckOdds(Person actor, Person target, float difficulty)
    {
        float otherMods = 0;
        float successOdds;

        if (target.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * target.Needs.Energy - .16f, .2f), 0);
        if (actor.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * actor.Needs.Energy - .16f, .2f), 0);
        successOdds = .5f + (actor.Personality.Strength * 0.6f) - (target.Personality.Strength * 0.6f) + otherMods - difficulty;
        
        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float SpellcastOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        if (actor.HasTrait(Quirks.PerfectSpells))
            return 1;

        float otherMods = 0;
        float successOdds;
        
        if (actor.HasTrait(Quirks.MagicalLust) && actor.Needs.Horniness >= 0.5f)
            otherMods += ((actor.Needs.Horniness - 0.5f) / 2);
        if (actor.HasTrait(Quirks.Transmuter) && ((interaction.Type == InteractionType.CastGrow) || (interaction.Type == InteractionType.CastShrink)))
            otherMods += 0.1f;
        if (actor.HasTrait(Quirks.Charmer) && (interaction.Type == InteractionType.CastCharm))
            otherMods += 0.1f;

        successOdds = 0.5f + (actor.Personality.Charisma * 0.6f) + (actor.Magic.Proficiency * 0.3f) - (target.Personality.Dominance * 0.6f) - (target.Magic.Willpower * 0.3f) + otherMods - difficulty;

        if (interaction.Type == InteractionType.CastGrow)
            successOdds += target.Personality.PredWillingness / 2;
        if (interaction.Type == InteractionType.CastShrink)
            successOdds += target.Personality.PreyWillingness / 2;
        if (actor.HasTrait(Quirks.Intelligent))
            successOdds *= 1.2f;
        if (target.HasTrait(Quirks.WeakWilled))
            successOdds *= 1.2f;

        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float SelfSpellcastOdds(SelfActionBase selfAction, Person actor, float difficulty)
    {
        if (actor.HasTrait(Quirks.PerfectSpells))
            return 1;

        float otherMods = 0;
        float successOdds;
        
        if (actor.HasTrait(Quirks.MagicalLust) && actor.Needs.Horniness >= 0.5f)
            otherMods += ((actor.Needs.Horniness - 0.5f) / 2);
        if (actor.HasTrait(Quirks.Transmuter) && ((selfAction.Type == SelfActionType.CastGrowSelf) || (selfAction.Type == SelfActionType.CastShrinkSelf)))
            otherMods += 0.1f;

        successOdds = 0.8f + ((actor.Magic.Proficiency) * 0.3f) + otherMods - difficulty;

        if (actor.HasTrait(Quirks.Intelligent))
            successOdds *= 1.2f;

        if (successOdds <= 0)
            successOdds = 0;
        return successOdds;
    }

    public static float VoreCheckOdds(Person pred, Person prey, VoreType type)
    {
        float otherMods = 0;
        if (prey.TurnsSinceOrgasm < 4)
            otherMods += .2f - prey.TurnsSinceOrgasm * .05f;
        if (prey.StreamingSelfAction != SelfActionType.None || prey.StreamingAction != InteractionType.None)
            otherMods += .1f;
        if (pred.HasTrait(Quirks.LoveBites) && pred.Needs.Horniness > .6f)
            otherMods += (pred.Needs.Horniness - .6f) / 4;
        if (pred.HasTrait(Quirks.GreatHunger) && pred.Needs.Hunger > .4f)
            otherMods += (pred.Needs.Hunger - .4f) / 6;
        if (pred.HasTrait(Quirks.ViciousPredator) && pred.GetRelationshipWith(prey).FriendshipLevel < 0)
            otherMods += .1f;
        if (pred.HasTrait(Quirks.BiggerFish) && prey.VoreController.CapableOfVore())
            otherMods += .1f;
        if (pred.HasTrait(Quirks.LowOnTheChain) && prey.VoreController.CapableOfVore() == false)
            otherMods += .1f;
        if (pred.HasTrait(Quirks.PredInPrivate) && pred.ZoneContainsBed())
            otherMods += .1f;
        if (prey.HasTrait(Quirks.SlipperyWhenWet) && prey.Needs.Horniness > .6f)
            otherMods -= Math.Max(Math.Min(.6f * pred.Needs.Energy - .36f, .2f), 0);
        if (pred.HasTrait(Quirks.PredInPrivate) && pred.ZoneContainsBed())
            otherMods += .1f;
        if (pred.HasTrait(Quirks.FearsomeReputation))
            otherMods += .008f * Math.Min(pred.MiscStats.TimesDigestedOther, 20);

        if (prey.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * prey.Needs.Energy - .16f, .2f), 0);
        if (pred.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * pred.Needs.Energy - .16f, .2f), 0);
        if (prey.ActiveSex != null)
            otherMods += .2f;

        if (prey.ClothingStatus == ClothingStatus.Nude)
            otherMods += .2f;
        if (prey.ClothingStatus == ClothingStatus.Underwear)
            otherMods += .1f;

        if (prey.StreamingSelfAction == SelfActionType.Rest)
            otherMods += .3f;
        if (prey.StreamingSelfAction == SelfActionType.Masturbate)
            otherMods += .1f;

        otherMods += pred.VoreController.VorePower * .04f;
        otherMods -= prey.VoreController.VorePower * .04f;
        var predSize = (float)Math.Pow(pred.PartList.Height / prey.PartList.Height, State.World.Settings.SizeFactor);
        otherMods += (float)Math.Log(predSize, 10.0f);

        var odds = .3f + (prey.GetRelationshipWith(pred).FriendshipLevel * .3f) + ((pred.Personality.Strength + 2 * pred.Personality.Voracity) / 3) - prey.Personality.Strength + otherMods;

        if (pred.Needs.Energy >= 1f)
            odds *= 0.5f;
        if (prey.Needs.Energy >= 1f)
            odds *= 1.5f;
        if (prey.Health <= Constants.HealthMax * 0.8f)
            odds *= 1.2f;

        switch (type)
        {
            case VoreType.Oral:
                odds /= State.World.Settings.OralVoreDifficulty;
                break;
            case VoreType.Unbirth:
                odds /= State.World.Settings.UnbirthDifficulty;
                break;
            case VoreType.Cock:
                odds /= State.World.Settings.CockVoreDifficulty;
                break;
            case VoreType.Anal:
                odds /= State.World.Settings.AnalVoreDifficulty;
                break;
        }
        if (pred.HasTrait(Quirks.PerfectAttack))
        {
            if (prey.HasTrait(Quirks.PerfectDefense) == false)
                odds = 1;
        }
        else if (prey.HasTrait(Quirks.PerfectDefense))
            odds = 0;

        if (odds <= 0)
            odds = 0;

        return odds;

    }

    public static float AllowDigestOdds(Person pred, Person prey)
    {
        return prey.AI.Desires.DesireToBeDigested(pred);
    }

    public static float VoreEscapeCheckOdds(Person prey, Person pred, float multiplier)
    {
        float successOdds;
        float otherMods = 0;

        if (prey.Needs.Energy > .4f)
            otherMods -= Math.Max(Math.Min(.4f * prey.Needs.Energy - .16f, .2f), 0);
        if (pred.Needs.Energy > .4f)
            otherMods += Math.Max(Math.Min(.4f * pred.Needs.Energy - .16f, .2f), 0);
        if (pred.HasTrait(Quirks.SkilledEscape))
        {
            if (prey.Needs.Energy > .5f)
                otherMods -= Math.Max(Math.Min(.5f * prey.Needs.Energy - .2f, .2f), 0);
            if (prey.Needs.Energy < .5f)
                otherMods += Math.Max(Math.Min(.5f * pred.Needs.Energy - .2f, .2f), 0);
        }
        multiplier *= prey.Boosts.EscapeSkill;
        multiplier *= State.World.Settings.EscapeRate;

        otherMods += prey.VoreController.VorePower * .02f;
        otherMods -= pred.VoreController.VorePower * .02f;
        var preySize = (float)Math.Pow(prey.PartList.Height / pred.PartList.Height, State.World.Settings.SizeFactor);
        multiplier *= preySize;

        if (prey.HasTrait(Quirks.PerfectEscape))
        {
            if (pred.HasTrait(Quirks.PerfectReflexes) == false)
                return 1;
        }
        else if (pred.HasTrait(Quirks.PerfectReflexes))
            return 0;

        successOdds = multiplier * (.05f - ((pred.Personality.Strength + 2 * pred.Personality.Voracity) / 60) + prey.Personality.Strength / 20 + otherMods / 8);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float BegCheckOdds(Person prey, Person pred)
    {
        float successOdds;

        if (pred.Personality.EndoDominator && State.World.Settings.EndoDominatorsHoldForever && pred.VoreController.TargetIsBeingDigested(prey) == false)
            return 0;
        if (pred.HasTrait(Traits.BlackoutPred) && pred.ZoneContainsBed() && !prey.BeingSwallowed && pred.VoreController.TargetIsBeingDigested(prey))
            return 0;
        if (pred.HasTrait(Traits.Unyielding))
            return 0;
        
        successOdds = 0.35f * ((pred.Personality.Kindness * 2) - 1) * (1 - State.World.Settings.PredConviction) / (0.15f + pred.AI.Desires.DesireToVoreTargetWithoutPredWillingness(prey)) / (float)Math.Pow(1 + pred.VoreController.GetProgressOf(prey).TimesBegged, 2) / (pred.HasTrait(Traits.Traitor) ? 1.5f : 1);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float SpareCheckOdds(Person prey, Person pred)
    {
        float successOdds;
        
        if (pred.VoreController.GetAllProgress(pred.VoreController.GetProgressOf(prey).Location).Count > 1)
            return 0;
        if (pred.Personality.VorePreference == VorePreference.Digestion)
            return 0;
        if (pred.HasTrait(Traits.BlackoutPred) && pred.ZoneContainsBed() && !prey.BeingSwallowed && pred.VoreController.TargetIsBeingDigested(prey))
            return 0;
        if (pred.HasTrait(Traits.Unyielding))
            return 0;
        successOdds = 0.325f * ((pred.Personality.Kindness * 2) - 1) * (1 - State.World.Settings.PredConviction) / (0.15f + pred.AI.Desires.DesireToDigestTarget(prey)) / (float)Math.Pow(1 + pred.VoreController.GetProgressOf(prey).TimesBegged, 2) / (pred.HasTrait(Traits.Traitor) ? 1.5f : 1);

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float WillingVoreDigestCheckOdds(Person actor, Person target, VoreType type)
    {
        float finalOdds;
        float successOdds = target.AI.Desires.DesireToBeVored(actor) - .35f;
        float interest = 2;
        switch (type)
        {
            case VoreType.Oral:
                interest *= target.Personality.OralVoreInterest;
                break;
            case VoreType.Unbirth:
                interest *= target.Personality.UnbirthInterest;
                break;
            case VoreType.Cock:
                interest *= target.Personality.CockVoreInterest;
                break;
            case VoreType.Anal:
                interest *= target.Personality.AnalVoreInterest;
                break;
        }
        interest -= 1;
        float interestMod = (successOdds * interest) / 5;
        finalOdds = successOdds + interestMod;

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float WillingVoreCheckOdds(Person actor, Person target, VoreType type)
    {
        float difficulty = .25f;
        float finalOdds;
        if (State.World.Settings.EndoisOnlyRomantic)
        {
            if (target.Romance.DesiresGender(actor.GenderType) == false)
                return 0;
        }
        Relationship relationship = target.GetRelationshipWith(actor);
        float successOdds;
        float personalitymod;
        var trust = target.GetRelationshipWith(actor).Trust;
        if (trust < 0)
            trust *= 2;
        trust *= .25f;

        if (State.World.Settings.EndoisOnlyRomantic)
        {
            personalitymod = (actor.Personality.Charisma * 0.2f) + ((target.Personality.PreyWillingness / 2) - 0.3f) + trust + relationship.GetRomanticLevelToTarget() + (target.Personality.Promiscuity / 3) + target.AI.Desires.DesireToBeVoredEndo(actor) * .35f - difficulty;
            successOdds = personalitymod * (target.Personality.PreyWillingness + 0.1f);
        }
         
        else
        {
            personalitymod = (actor.Personality.Charisma * 0.2f) + ((target.Personality.PreyWillingness / 2) - 0.3f) + trust + (relationship.GetRomanticLevelToTarget() * 0.7f) + (relationship.FriendshipLevel * 0.3f) + (target.Personality.Promiscuity / 3) + target.AI.Desires.DesireToBeVoredEndo(actor) * .35f - difficulty;
            successOdds = personalitymod * (target.Personality.PreyWillingness + 0.1f);
        }

        if (State.World.Settings.EndoisOnlyRomantic)
        {
            if (target.Romance.CanSafelyRomance(actor) == false)
                successOdds = successOdds * .8f - .15f;
        }

        float typeinterest = 2;
        switch (type)
        {
            case VoreType.Oral:
                typeinterest *= target.Personality.OralVoreInterest;
                break;
            case VoreType.Unbirth:
                typeinterest *= target.Personality.UnbirthInterest;
                break;
            case VoreType.Cock:
                typeinterest *= target.Personality.CockVoreInterest;
                break;
            case VoreType.Anal:
                typeinterest *= target.Personality.AnalVoreInterest;
                break;
        }
        typeinterest -= 1;
        float interestMod = (successOdds * typeinterest) / 5;
        finalOdds = successOdds + interestMod;

        if (finalOdds <= 0)
            finalOdds = 0;

        return finalOdds;
    }

    public static float RomanticCheckOdds(InteractionBase interaction, Person actor, Person target, float difficulty)
    {
        float hornymod = target.Needs.Horniness;
        if (target.Romance.CanSafelyRomance(actor) == false)
            return 0;
        if (interaction.Type == InteractionType.AskOut && target.Romance.IsDating)
            return 0;
        if (interaction.Type == InteractionType.StartSex && target.HasTrait(Traits.LovesPrivateSex) && target.ZoneContainsBed())
            return 1;
        if (interaction.Type == InteractionType.AskOut)
            hornymod = target.Romance.RomanticDesperation;
        Relationship relationship = target.GetRelationshipWith(actor);
        float successOdds = (actor.Personality.Charisma * 0.2f) + (target.Romance.RomanticDesperation * 0.25f) + relationship.GetRomanticLevelToTarget() + (relationship.FriendshipLevel * 0.2f) + (target.Personality.Promiscuity * hornymod) - difficulty;
        if (target.HasTrait(Traits.TentativeRomance))
            successOdds *= 1.4f;
        if ((interaction.Type == InteractionType.Kiss || interaction.Type == InteractionType.MakeOut || interaction.Type == InteractionType.PreyKissOtherPrey) && target.HasTrait(Traits.LovesKissing))
            successOdds *= 1.3f;
        if ((interaction.Type == InteractionType.Hug) && target.HasTrait(Traits.LovesHugs))
            successOdds *= 1.3f;
        if ((interaction.Type == InteractionType.StartSex) && target.HasTrait(Traits.SexAddict))
            successOdds *= 1.3f;

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    public static float AskToBeDigestedOdds(Person actor, Person target)
    {
        if (target.Personality.VorePreference == VorePreference.Endosoma)
            return 0;
        if (State.World.Settings.CheckDigestion(target, DigestionAlias.CanVore))
        {
            return target.AI.Desires.DesireToDigestTarget(actor);
        }           
        return 0;
    }

    public static float StripCheckOdds(Person actor, Person target, float difficulty)
    {
        float otherOdds = 0;
        float successOdds;
        if (target.IsWillingPreyToTarget(actor))
            otherOdds += .6f;
        if (target.Personality.Promiscuity > .5f)
            otherOdds += (target.Personality.Promiscuity - .5f) / 2;
        if (target.Personality.PreferredClothing != ClothingStatus.Normal)
            otherOdds += .15f;
        if (target.HasTrait(Traits.EasyShow)  || target.HasTrait(Traits.Exhibitionist))
            otherOdds += .2f + target.GetRelationshipWith(actor).FriendshipLevel;
        if (HelperFunctions.InPrivateRoom(actor))
            otherOdds += .2f;
        else
            otherOdds -= .2f;
        
        successOdds = - .2f + target.GetRelationshipWith(actor).RomanticLevel * .5f + otherOdds;

        if (successOdds <= 0)
            successOdds = 0;

        return successOdds;
    }

    internal static float GetFreeingOdds(Person actor, Person target, VoreLocation loc)
    {
        var consume = target.VoreController.CurrentSwallow(loc);
        if (consume != null)
            return consume.AttemptFreeOdds(actor);
        consume = target.VoreController.GetLiving(loc);
        if (consume != null)
            return consume.AttemptFreeOdds(actor);
        return 0;
    }
}
