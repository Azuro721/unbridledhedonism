﻿using Assets.Scripts.TextGeneration;
using DialogueReader;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static TextGenerator;

public enum SuccessRequirement
{
    Either,
    Success,
    Fail
}

class GenericEventString<T>
{
    internal Func<T, string> GetString;
    internal Predicate<T> Conditional;
    internal int Priority;

    internal int TempPriority;

    string ActorRace;
    string TargetRace;

    string Text;
    string Cond;

    internal float FallThrough;

    internal SuccessRequirement SuccessRequirement;

    public GenericEventString(string text, string actorRace, string targetRace, int priority = 0, float fallthrough = 0, string cond = "", SuccessRequirement successRequirement = SuccessRequirement.Either)
    {
        Priority = priority;

        Text = text;
        Cond = cond;

        FallThrough = fallthrough;

        if (string.IsNullOrWhiteSpace(actorRace))
            ActorRace = "Any";
        else
            ActorRace = actorRace;

        if (string.IsNullOrWhiteSpace(targetRace))
            TargetRace = "Any";
        else
            TargetRace = targetRace;

        GetString = (s) => ReadyString(s, Text);
        Conditional = (s) => ConditionalProcessor.ProcessConditional(s, Cond, ActorRace, TargetRace);
        SuccessRequirement = successRequirement;
    }

    internal void CalculateTempPriority(string actorRace, string targetRace)
    {
        TempPriority = Priority - 1900 * RaceManager.RaceDescendants(actorRace, ActorRace) - 2000 * RaceManager.RaceDescendants(targetRace, TargetRace);
        if (Rand.NextFloat(0, 1) < FallThrough)
            TempPriority -= 800000;
        if (actorRace == ActorRace)
            TempPriority += 20500;
        if (targetRace == TargetRace)
            TempPriority += 20000;

    }

    internal string TestGetActor => ActorRace;
    internal string TestGetTarget => TargetRace;

    //public GenericEventString(Func<T, string> getString, int priority = 0, Predicate<T> conditional = null, SuccessRequirement successRequirement = SuccessRequirement.Either)
    //{
    //    Priority = priority;

    //    GetString = getString;
    //    if (conditional == null)
    //        Conditional = (s) => true;
    //    else
    //        Conditional = conditional;
    //    SuccessRequirement = successRequirement;
    //}

    string ReadyString(T action, string text)
    {
        if (action is Interaction i)
        {
            return CleanText(i.Actor, i.Target, text);
        }
        else if (action is SexInteraction iSx)
        {
            return CleanText(iSx.Actor, iSx.Target, text);
        }
        else if (action is SelfAction iSe)
        {
            return CleanText(iSe.Actor, iSe.Actor, text);
        }
        UnityEngine.Debug.LogWarning("Fall through...");
        return "";
    }

    string CleanText(Person actor, Person target, string text)
    {
        if (text.Contains("[ActorCustom-"))
        {
            if (actor.PartList.Tags != null)
            {
                foreach (var customType in actor.PartList.Tags)
                {
                    text = text.Replace($"[ActorCustom-{customType.Key}]", customType.Value);
                }
            }

        }
        if (text.Contains("[TargetCustom-"))
        {
            if (target.PartList.Tags != null)
            {
                foreach (var customType in target.PartList.Tags)
                {
                    text = text.Replace($"[TargetCustom-{customType.Key}]", customType.Value);
                }
            }

        }
        return text.Replace("[ActorName]", $"{actor.GetFirstNameWithLink()}").
            Replace("[TargetName]", $"{target.GetFirstNameWithLink()}").
            Replace("[ActorHeIs]", $"{GPP.HeIs(actor)}").
            Replace("[!ActorHeIs]", $"{GPP.HeIs(actor, true)}").
            Replace("[ActorHeHas]", $"{GPP.HeHas(actor)}").
            Replace("[!ActorHeHas]", $"{GPP.HeHas(actor, true)}").
            Replace("[ActorHis]", $"{GPP.His(actor)}").
            Replace("[!ActorHis]", $"{GPP.His(target).ToTitleCase()}").
            Replace("[ActorHisCapitalized]", $"{GPP.His(target).ToTitleCase()}").  // Old style tag support
            Replace("[ActorHim]", $"{GPP.Him(actor)}").
            Replace("[ActorHimself]", $"{GPP.Himself(actor)}").
            Replace("[ActorHe]", $"{GPP.He(actor)}").
            Replace("[!ActorHe]", $"{GPP.He(actor).ToTitleCase()}").
            Replace("[ActorBastard]", $"{GPP.Bastard(actor)}").
            Replace("[TargetHeIs]", $"{GPP.HeIs(target)}").
            Replace("[!TargetHeIs]", $"{GPP.HeIs(target, true)}").
            Replace("[TargetHeHas]", $"{GPP.HeHas(target)}").
            Replace("[!TargetHeHas]", $"{GPP.HeHas(target, true)}").
            Replace("[TargetHis]", $"{GPP.His(target)}").
            Replace("[!TargetHis]", $"{GPP.His(target).ToTitleCase()}").
            Replace("[TargetHim]", $"{GPP.Him(target)}").
            Replace("[TargetHimself]", $"{GPP.Himself(target)}").
            Replace("[TargetHe]", $"{GPP.He(target)}").
            Replace("[!TargetHe]", $"{GPP.He(target).ToTitleCase()}").
            Replace("[TargetBastard]", $"{GPP.Bastard(target)}").
            Replace("[SIfActorSingular]", $"{GPP.SIfSingular(actor)}").
            Replace("[ESIfActorSingular]", $"{GPP.ESIfSingular(actor)}").
            Replace("[IESIfActorSingular]", $"{GPP.IESIfSingular(actor)}").
            Replace("[ActorBoobSize]", $"{GetWord.BreastSize(actor.PartList.BreastSize)}").
            Replace("[TargetBoobSize]", $"{GetWord.BreastSize(target.PartList.BreastSize)}").
            Replace("[ActorDickSize]", $"{GetWord.DickSize(actor.PartList.DickSize)}").
            Replace("[TargetDickSize]", $"{GetWord.DickSize(target.PartList.DickSize)}").
            Replace("[ActorBallSize]", $"{GetWord.BallSize(actor.PartList.BallSize)}").
            Replace("[TargetBallSize]", $"{GetWord.BallSize(target.PartList.BallSize)}").
            Replace("[PredatorOfTarget]", $"{target.FindMyPredator()?.FirstName ?? "[No Pred]"}").
            Replace("[PredOfTarget]", $"{target.FindMyPredator()?.FirstName ?? "[No Pred]"}").
            Replace("[PredatorOfActor]", $"{actor.FindMyPredator()?.FirstName ?? "[No Pred]"}").
            Replace("[PredOfActor]", $"{actor.FindMyPredator()?.FirstName ?? "[No Pred]"}").
            Replace("[NemesisOfTarget]", $"{target.FindNemesis()?.FirstName ?? "[No Nemesis]"}").
            Replace("[NemesisOfActor]", $"{actor.FindNemesis()?.FirstName ?? "[No Nemesis]"}").
            Replace("[BestieOfTarget]", $"{target.FindBestFriend()?.FirstName ?? "[No Bestie]"}").
            Replace("[BestieOfActor]", $"{actor.FindBestFriend()?.FirstName ?? "[No Bestie]"}").
            Replace("[VendettaOfTarget]", $"{target.FindVendetta()?.FirstName ?? "[No Vendetta]"}").
            Replace("[VendettaOfActor]", $"{actor.FindVendetta()?.FirstName ?? "[No Vendetta]"}").
            Replace("[CrushOfTarget]", $"{target.FindCrush()?.FirstName ?? "[No Crush]"}").
            Replace("[CrushOfActor]", $"{actor.FindCrush()?.FirstName ?? "[No Crush]"}").
            Replace("[DateOfTarget]", $"{target.Romance.Dating?.FirstName ?? "[No Date]"}").
            Replace("[DateOfActor]", $"{actor.Romance.Dating?.FirstName ?? "[No Date]"}").
            Replace("[Breasts]", $"{BreastsSyn}").
            Replace("[Pussy]", $"{PussySyn}").
            Replace("[Pussies]", $"{PussiesSyn}").
            Replace("[Dick]", $"{DickSyn}").
            Replace("[Balls]", $"{BallsSyn}").
            Replace("[Clit]", $"{ClitSyn}").
            Replace("[Clits]", $"{ClitsSyn}").
            Replace("[Anus]", $"{AnusSyn}").
            Replace("[Ass]", $"{AssSyn}").
            Replace("[Belly]", $"{BellySyn}").
            Replace("[Soft]", $"{SoftSyn}").
            Replace("[Firm]", $"{FirmSyn}").
            Replace("[DigestAdj]", $"{DigestAdjSyn}").
            Replace("[FullAdj]", $"{FullAdjSyn}").
            Replace('�', '\''). //Replaces the funny ' with the proper '
            Replace("[TargetEyeColor]", $"{target.PartList.EyeColor}").
            Replace("[TargetHairColor]", $"{target.PartList.HairColor}").
            Replace("[TargetHairLength]", $"{target.PartList.HairLength}").
            Replace("[TargetHairStyle]", $"{target.PartList.HairStyle}").
            Replace("[ActorLocationInPred]", $"{actor.GetMyVoreProgress()?.Location.ToString().ToLower() ?? "insides"}").
            Replace("[TargetLocationInPred]", $"{target.GetMyVoreProgress()?.Location.ToString().ToLower() ?? "insides"}").
            Replace("[ShoulderAdjective]", $"{target.PartList.ShoulderDescription}").
            Replace("[WaistAdjective]", $"{target.PartList.HipDescription}");
        //Replace("[", "\"%\"");
    }
}

internal static class MessageManager
{

    static readonly MessageData Data = new MessageData();

    static internal string GetText(SexInteraction action)
    {
        if (Data.SexTexts.ContainsKey(action.Type))
        {
            var messages = Data.SexTexts[action.Type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Position} {action.Type}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";
    }
    static internal string GetText(SelfAction action)
    {
        if (action.Type >= SelfActionType.ScatDisposalBathroom && action.Type <= SelfActionType.UnbirthDisposalFloor)
        {
            return DisposalText(action);
        }
        else if (Data.SelfTexts.ContainsKey(action.Type))
        {
            var messages = Data.SelfTexts[action.Type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Type}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, "Any");
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";
    }
    static internal string GetText(Interaction action)
    {
        if (InteractionList.List[action.Type].Class == ClassType.VoreConsuming && (InteractionList.List[action.Type].Type < InteractionType.KissVore || InteractionList.List[action.Type].Type > InteractionType.SexAskToAnalVoreDigest))
        {
            return VoreText(action);
        }
        if (action.Type == InteractionType.MakeOut && action.Success == true)
        {
            SexInteraction sex = new SexInteraction(action.Actor, action.Target, SexInteractionType.DeepKiss);
            return GetText(sex);
        }

        if (Data.InteractionTexts.ContainsKey(action.Type))
        {
            var messages = Data.InteractionTexts[action.Type].Where(s => s.Conditional(action) &&
            (s.SuccessRequirement == SuccessRequirement.Either ||
            (s.SuccessRequirement == SuccessRequirement.Success && action.Success) || (s.SuccessRequirement == SuccessRequirement.Fail && action.Success == false))).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {action.Type}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";
    }
    static internal string GetText(Interaction action, VoreMessageType voreMessageType)
    {
        if (Data.VoreTexts.ContainsKey(voreMessageType))
        {
            var messages = Data.VoreTexts[voreMessageType].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                //Disregard optional texts
                if (voreMessageType != VoreMessageType.VoreCleanliness && voreMessageType != VoreMessageType.VoreClothing && voreMessageType != VoreMessageType.VoreHunger)
                    UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {voreMessageType}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";
    }
    static internal string GetText(Interaction action, BodyPartDescriptionType bodyMessageType)
    {
        if (Data.BodyPartTexts.ContainsKey(bodyMessageType))
        {
            var messages = Data.BodyPartTexts[bodyMessageType].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                //UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {bodyMessageType}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";
    }

    static string DisposalText(SelfAction self)
    {
        InteractionType type = InteractionType.EventDisposalBathroom;
        DisposalData data = null;

        if (self.Type == SelfActionType.ScatDisposalBathroom)
        {
            type = InteractionType.EventDisposalBathroom;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels).FirstOrDefault();
        }
        if (self.Type == SelfActionType.ScatDisposalFloor)
        {
            type = InteractionType.EventDisposalGround;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Stomach || s.Location == VoreLocation.Bowels).FirstOrDefault();
        }
        else if (self.Type == SelfActionType.CockDisposalBathroom)
        {
            type = InteractionType.EventDisposalCockBathroom;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Balls).FirstOrDefault();
        }
        else if (self.Type == SelfActionType.CockDisposalFloor)
        {
            type = InteractionType.EventDisposalCockGround;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Balls).FirstOrDefault();
        }
        else if (self.Type == SelfActionType.UnbirthDisposalBathroom)
        {
            type = InteractionType.EventDisposalUnbirthBathroom;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Womb).FirstOrDefault();
        }
        else if (self.Type == SelfActionType.UnbirthDisposalFloor)
        {
            type = InteractionType.EventDisposalUnbirthGround;
            data = self.Actor.Disposals.Where(s => s.Location == VoreLocation.Womb).FirstOrDefault();
        }
        if (data == null)
        {
            UnityEngine.Debug.Log("Disposal Error");
            return "";
        }

        Interaction action = new Interaction(self.Actor, data.Person, true, type, data.TurnsDone);

        if (Data.InteractionTexts.ContainsKey(type))
        {
            var messages = Data.InteractionTexts[type].Where(s => s.Conditional(action)).ToList();
            if (messages.Any() == false)
            {
                UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} {action.Type}");
                return "";
            }
            foreach (var message in messages)
            {
                message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
            }
            int priority = messages.Max(s => s.TempPriority);
            var array = messages.Where(s => s.TempPriority == priority).ToArray();
            return array[Rand.Next(array.Length)].GetString(action);
        }
        return "";


    }

    static string VoreText(Interaction action)
    {
        if (action.Type == InteractionType.Unbirth || action.Type == InteractionType.SexUnbirth)
            return VoreUnbirth(action);
        if (action.Type == InteractionType.CockVore || action.Type == InteractionType.SexCockVore)
            return VoreCock(action);
        if (action.Type == InteractionType.AnalVore || action.Type == InteractionType.SexAnalVore)
            return VoreAnal(action);
        //if (action.Type == InteractionType.OralVore)
        return VoreOral(action);
    }

    private static string VoreOral(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.OralBegin);
            else if (action.Stage == 1)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.OralVore)) + GetText(action, VoreMessageType.VoreCleanliness);
            else if (action.Stage == 2 && action.Actor.VoreController.TargetIsBeingDigested(action.Target))
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.OralVore)) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.OralVore)) + GetText(action, VoreMessageType.VoreClothing);
            else if (action.Stage == 4 && action.Actor.LastSex == action.Target && action.Actor.LastSexTurn >= State.World.Turn - 6)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.OralVore)) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.OralVore));
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InStomachEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InStomach);
        }
        else
        {
            return GetText(action, VoreMessageType.OralVoreRefused);
        }
    }

    private static string VoreCock(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.CockVoreBegin);
            else if (action.Stage == 1)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.CockVore));
            else if (action.Stage == 2 && action.Actor.VoreController.TargetIsBeingDigested(action.Target))
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.CockVore)) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.CockVore)) + GetText(action, VoreMessageType.VoreClothing);
            else if (action.Stage == 4 && action.Actor.LastSex == action.Target && action.Actor.LastSexTurn >= State.World.Turn - 6)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.CockVore)) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.CockVore));
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InBallsEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InBalls);
        }
        else
        {
            return GetText(action, VoreMessageType.CockVoreRefused);
        }
    }

    private static string VoreUnbirth(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.UnbirthBegin);
            else if (action.Stage == 1)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.Unbirth));
            else if (action.Stage == 2 && action.Actor.VoreController.TargetIsBeingDigested(action.Target))
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.Unbirth)) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.Unbirth)) + GetText(action, VoreMessageType.VoreClothing);
            else if (action.Stage == 4 && action.Actor.LastSex == action.Target && action.Actor.LastSexTurn >= State.World.Turn - 6)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.Unbirth)) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.Unbirth));
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InWombEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InWomb);
        }
        else
        {
            return GetText(action, VoreMessageType.UnbirthRefused);
        }
    }

    private static string VoreAnal(Interaction action)
    {
        if (action.Success)
        {
            if (action.Stage == 0)
                return GetText(action, VoreMessageType.AnalVoreBegin);
            else if (action.Stage == 1)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.AnalVore));
            else if (action.Stage == 2 && action.Actor.VoreController.TargetIsBeingDigested(action.Target))
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.AnalVore)) + GetText(action, VoreMessageType.VoreHunger);
            else if (action.Stage == 3)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.AnalVore)) + GetText(action, VoreMessageType.VoreClothing);
            else if (action.Stage == 4 && action.Actor.LastSex == action.Target && action.Actor.LastSexTurn >= State.World.Turn - 6)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.AnalVore)) + GetText(action, VoreMessageType.VoreAfterSex);
            else if (action.Stage <= action.Target.PartList.Parts.Count)
                return GetText(action, action.Target.PartList.Parts[action.Stage - 1].GetBPD(BodyPartDescription.AnalVore));
            else if (action.Actor.VoreController.GetProgressOf(action.Target).Location == VoreLocation.Stomach)
                return VoreOral(action); //Divert to stomach messages if in stomach
            else if (action.Actor.VoreController.TargetIsBeingDigested(action.Target) == false)
            {
                if (Rand.NextFloat(0, 1) > Config.SuppressEndoMessages)
                    return GetText(action, VoreMessageType.InAnusEndo);
                else
                    return "{SUPPRESS}";
            }
            else
                return GetText(action, VoreMessageType.InAnus);
        }
        else
        {
            return GetText(action, VoreMessageType.AnalVoreRefused);
        }
    }

    static internal string GetPopUpText(Interaction action)
    {
        if (Enum.TryParse($"PlayerPop_{action.Type.ToString()}", out InteractionType PlayerPopType))
        {
            if (Data.InteractionTexts.ContainsKey(PlayerPopType))
            {
                var messages = Data.InteractionTexts[PlayerPopType].Where(s => s.Conditional(action) &&
                (s.SuccessRequirement == SuccessRequirement.Either ||
                (s.SuccessRequirement == SuccessRequirement.Success && action.Success) || (s.SuccessRequirement == SuccessRequirement.Fail && action.Success == false))).ToList();
                if (messages.Any() == false)
                {
                    //UnityEngine.Debug.LogWarning($"Fell out of block with no available messages {action.Actor.FirstName} {action.Target.FirstName} {action.Success} PlayerPop_{action.Type}");
                    return "";
                }
                foreach (var message in messages)
                {
                    message.CalculateTempPriority(action.Actor.Race, action.Target.Race);
                }
                int priority = messages.Max(s => s.TempPriority);
                var array = messages.Where(s => s.TempPriority == priority).ToArray();
                return array[Rand.Next(array.Length)].GetString(action);
            }
        }
        return "";
    }

    internal static void BulkTest()
    {
        Person A = new Person("A", "First", 0, Orientation.All, true, new Vec2(0, 0));
        Person B = new Person("B", "Second", 0, Orientation.All, true, new Vec2(0, 0));

        SexInteraction sexInter = new SexInteraction(A, B, SexInteractionType.CaressFace);
        foreach (var value in Data.SexTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(sexInter);
            }
        }
        SelfAction selfInter = new SelfAction(A, SelfActionType.Rest);
        foreach (var value in Data.SelfTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(selfInter);
            }
        }
        Interaction action = new Interaction(A, B, true, InteractionType.Talk);
        foreach (var value in Data.InteractionTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }
        foreach (var value in Data.VoreTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }
        foreach (var value in Data.BodyPartTexts)
        {
            foreach (var entry in value.Value)
            {
                A.Race = entry.TestGetActor;
                B.Race = entry.TestGetTarget;
                entry.Conditional(action);
            }
        }

        if (UnityEngine.Object.FindObjectOfType<MessageBox>() == null)
            State.GameManager.CreateMessageBox("No errors found");
    }

}


