﻿using Assets.Scripts.UI;
using OdinSerializer;
using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class TemplateController
{
    private TemplateContainer container;

    string filename;

    internal TemplateContainer Container
    {
        get
        {
            if (State.GameManager.StartScreen.gameObject.activeSelf)
            {
                if (container == null)
                {
                    LoadFromFile();
                    if (container == null)
                    {
                        container = new TemplateContainer();
                        container.List = new List<SavedTemplate>();
                    }
                }
                return container;
            }
            else
            {
                if (State.World.TemplateContainer.List == null)
                    State.World.TemplateContainer.List = new List<SavedTemplate>();
                return State.World.TemplateContainer;
            }

        }
        set => container = value;
    }


    internal void LoadFromFile()
    {
        var num = PlayerPrefs.GetInt("CurrentTemplate", 0);
        if (num == 0)
            filename = Path.Combine(State.StorageDirectory, "SavedTemplates.dat");
        else
            filename = Path.Combine(State.StorageDirectory, $"SavedTemplates{num + 1}.dat");

        if (!File.Exists(filename))
        {
            return;
        }
        try
        {

            byte[] bytes = File.ReadAllBytes(filename);
            Container = SerializationUtility.DeserializeValue<TemplateContainer>(bytes, DataFormat.Binary);

        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            State.GameManager.CreateMessageBox("Encountered an error when trying to load characters");
            return;
        }
    }



    internal void SaveToFile()
    {
        Container.LastSavedVersion = State.Version;
        try
        {
            byte[] bytes = SerializationUtility.SerializeValue(Container, DataFormat.Binary);
            File.WriteAllBytes(filename, bytes);
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            State.GameManager.CreateMessageBox("Couldn't save saved characters");
        }
    }

    internal class TemplateContainer
    {
        [OdinSerialize]
        internal List<SavedTemplate> List;
        [OdinSerialize]
        internal string LastSavedVersion;

        internal SavedTemplate GetRandom()
        {
            WeightedList<SavedTemplate> list = new WeightedList<SavedTemplate>();
            foreach (var entry in List)
            {
                list.Add(entry, entry.Weight);
            }
            return list.GetResult();
        }
    }

    internal class SavedTemplate
    {
        [OdinSerialize]
        internal TemplatePersonality CustomPersonality;

        [OdinSerialize]
        internal string SavedVersion;

        [OdinSerialize]
        public float VoreOdds;

        [OdinSerialize]
        public int Gender;
        [OdinSerialize]
        public string Race;
        [OdinSerialize]
        public int Orientation;
        [OdinSerialize]
        public int Personality;

        [OdinSerialize]
        public int Weight;

    }

}

