﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public sealed class VariableEditorIgnores : Attribute
{

}
public sealed class ProperNameAttribute : Attribute
{
    public string Name;

    public ProperNameAttribute(string name)
    {
        Name = name;
    }
}

public sealed class DescriptionAttribute : Attribute
{
    public string Description;

    public DescriptionAttribute(string description)
    {
        Description = description;
    }
}

public sealed class GenderDescriptionAttribute : Attribute
{

}

public sealed class FloatRangeAttribute : Attribute
{
    public float Min;
    public float Max;

    public FloatRangeAttribute(float min, float max)
    {
        Min = min;
        Max = max;
    }
}

public sealed class IntegerRangeAttribute : Attribute
{
    public float Min;
    public float Max;

    public IntegerRangeAttribute(int min, int max)
    {
        Min = min;
        Max = max;
    }
}


