﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


static class DebugManager
{
    static Dictionary<InteractionType, int> interactions = new Dictionary<InteractionType, int>();
    static Dictionary<SelfActionType, int> selfActions = new Dictionary<SelfActionType, int>();
    static Dictionary<SexInteractionType, int> sexActions = new Dictionary<SexInteractionType, int>();
    static Dictionary<string, int> other = new Dictionary<string, int>();

#if UNITY_EDITOR

    internal static void Log(string type)
    {
        if (other.ContainsKey(type))
            other[type]++;
        else
            other[type] = 1;
    }

    internal static void Log(InteractionType type)
    {
        if (interactions.ContainsKey(type))
            interactions[type]++;
        else
        interactions[type] = 1;
    }
    
    internal static void Log(SelfActionType type)
    {
        if (selfActions.ContainsKey(type))
            selfActions[type]++;
        else
            selfActions[type] = 1;
    }
    internal static void Log(SexInteractionType type)
    {
        if (sexActions.ContainsKey(type))
            sexActions[type]++;
        else
            sexActions[type] = 1;
    }

    internal static void WriteLog()
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("Interactions");
        foreach (var key in interactions)
        {
            sb.AppendLine($"{key.Key, -30}{key.Value}");
        }
        sb.AppendLine("Self Actions");
        foreach (var key in selfActions)
        {
            sb.AppendLine($"{key.Key,-30}{key.Value}");
        }
        sb.AppendLine("Sex Actions");
        foreach (var key in sexActions)
        {
            sb.AppendLine($"{key.Key,-30}{key.Value}");
        }
        foreach (var key in other)
        {
            sb.AppendLine($"{key.Key,-30}{key.Value}");
        }


        File.WriteAllText("test.txt", sb.ToString());

    }

#else
    internal static void Log<T>(T type)
    {

    }
    internal static void WriteLog()
    {

    }
#endif

}
