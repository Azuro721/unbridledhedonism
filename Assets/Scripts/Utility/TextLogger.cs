﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;
using OdinSerializer.Utilities;

class TextLogger
{
    readonly StringBuilder SB = new StringBuilder();
    Record lastRecord;
    internal void AddText(Record record)
    {
        if (lastRecord != null && record.ToString() == lastRecord.ToString()) //Gets duplicate interactions, and excess ------ from multiple idle turns
            return;
        lastRecord = record;      
        SB.AppendLine(record.ToString());

        if (SB.Length > 10000)
        {
            WriteOut();
        }
    }

    internal void WriteOut()
    {
        if (SB.Length == 0)
            return;
        if (State.World.Settings.LogAllText == false)
        {
            SB.Clear();
            return;
        }
        try
        {
            var logDir = Path.Combine(Application.streamingAssetsPath, "Logs");
            Directory.CreateDirectory(logDir);
            string fileName = State.World.Settings.LogFileName;
            if (fileName.IsNullOrWhitespace())
                fileName = "LogMissingTitle";
            fileName = Path.ChangeExtension(fileName, ".txt");
            File.AppendAllText(Path.Combine(logDir, fileName), SB.ToString());
        }
        catch (Exception e)
        {
            Debug.LogException(e);
            State.GameManager.CreateMessageBox("Error Exporting Log - Check the file name or turn off saving the log");
        }
        SB.Clear();
    }

}
