﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


static class GetWord
{
    internal static string Hunger(float value)
    {
        if (value > .9f)
            return "starving";
        else if (value > .6f)
            return "hungry";
        else if (value > .4f)
            return "peckish";
        else if (value > .2f)
            return "neutral";
        else if (value > .1f)
            return "full";
        else
            return "stuffed";
    }

    internal static string Extroversion(float value)
    {
        if (value > .9f)
            return "very outgoing";
        else if (value > .65f)
            return "moderate extrovert";
        else if (value > .35f)
            return "ambivert";
        else if (value > .10f)
            return "moderate introvert";
        else
            return "extreme introvert";
    }

    internal static string Dominance(float value)
    {
        if (value > .9f)
            return "very dominant";
        else if (value > .65f)
            return "fairly dominant";
        else if (value > .35f)
            return "switch";
        else if (value > .10f)
            return "fairly submissive";
        else
            return "very submissive";
    }

    internal static string Kindness(float value)
    {
        if (value > .9f)
            return "purely selfless";
        else if (value > .65f)
            return "kindhearted";
        else if (value > .35f)
            return "average";
        else if (value > .10f)
            return "selfish";
        else
            return "extremely selfish";
    }

    internal static string DigestionWillingness(float value)
    {
        if (value > .9f)
            return "digest me";
        else if (value > .75f)
            return "high";
        else if (value > .55f)
            return "sometimes";
        else if (value > .10f)
            return "rarely";
        else
            return "absolutely not";
    }

    internal static string Tiredness(float value)
    {
        if (value > .9f)
            return "sleep deprived";
        else if (value > .7f)
            return "tired";
        else if (value > .5f)
            return "weary";
        else if (value > .3f)
            return "rested";
        else
            return "well rested";
    }

    internal static string Cleanliness(float value)
    {
        if (value > .9f)
            return "dripping with sweat";
        else if (value > .7f)
            return "sweaty";
        else if (value > .4f)
            return "acceptable";
        else if (value > .1f)
            return "fresh";
        else
            return "squeaky clean";
    }

    internal static string Horniness(float value)
    {
        if (value > .95f)
            return "near climax";
        else if (value > .8f)
            return "panting";
        else if (value > .5f)
            return "aroused";
        else if (value > .2f)
            return "warmed up";
        else if (value > .05f)
            return "minimal";
        else
            return "none";
    }

    internal static string Healthiness(int value)
    {
        if (value > .8f * Constants.HealthMax)
            return "excellent";
        else if (value > .6f * Constants.HealthMax)
            return "injured";
        else if (value > .2f * Constants.HealthMax)
            return "badly wounded";
        else if (value > 0)
            return "critical shape";
        else
            return "dead";
    }

    internal static string Charisma(float value)
    {
        if (value > .8f)
            return "charming";
        else if (value > .5f)
            return "smooth";
        else if (value > .2f)
            return "rough";
        else
            return "poor";
    }

    internal static string SexDrive(float value)
    {
        if (value > .75f)
            return "lustful";
        else if (value > .5f)
            return "warm";
        else if (value > .25f)
            return "neutral";
        else
            return "cold";
    }

    internal static string Promiscuity(float value)
    {
        if (value > .85f)
            return "sex addict";
        else if (value > .7f)
            return "pleasure driven";
        else if (value > .4f)
            return "tease";
        else
            return "prude";
    }

    internal static string Strength(float value)
    {
        if (value > .8f)
            return "peak shape";
        else if (value > .6f)
            return "strong";
        else if (value > .4f)
            return "average";
        else if (value > .2f)
            return "weak";
        else
            return "pitiful";
    }

    internal static string Voracity(float value)
    {
        if (value > .85f)
            return "peak predator";
        else if (value > .65f)
            return "veteran predator";
        else if (value > .45f)
            return "experienced predator";
        else if (value > .25f)
            return "adequate predator";
        else
            return "newbie predator";
    }

    internal static string PredWillingness(float value)
    {
        if (value > .85f)
            return "sees people as cattle";
        else if (value > .7f)
            return "frequent predator";
        else if (value > .4f)
            return "occasional predator";
        else
            return "reluctant predator";
    }
    
    internal static string PredLoyalty(float value)
    {
        if (value > .85f)
            return "friends aren't food";
        else if (value > .7f)
            return "values friends";
        else if (value > .4f)
            return "thinks twice";
        else
            return "everyone is food";
    }

    internal static string PreyWillingness(float value)
    {
        if (value > .7f)
            return "fully willing";
        else if (value > .5f)
            return "partially willing";
        else if (value > .3f)
            return "hesitant";
        else
            return "fight to the end";
    }

    internal static string Voraphilia(float value)
    {
        if (value > .85f)
            return "loves vore";
        else if (value > .65f)
            return "really likes vore";
        else if (value > .45f)
            return "reasonably interested";
        else if (value > .25f)
            return "slightly interested";
        else
            return "yucky.";
    }

    internal static string Friendship(float value)
    {
        if (value > .85f)
            return "close friends";
        else if (value > .7f)
            return "good buddies";
        else if (value > .3f)
            return "friendly";
        else if (value > 0)
            return "warm";
        else if (value > -.25f)
            return "cold";
        else if (value > -.5f)
            return "disliked";
        else if (value > -.75f)
            return "intense dislike";
        else
            return "absolute enemies";
    }

    internal static string Romantic(float value)
    {
        if (value > .85f)
            return "lovers";
        else if (value > .4f)
            return "like them";
        else if (value > .15f)
            return "curious";
        else if (value > -.15f)
            return "neutral";
        else if (value > -.45f)
            return "disinterested";
        else if (value > -.7f)
            return "disgusted";
        else
            return "repulsive";
    }

    internal static string DickSize(float size)
    {
        if (size > 4.5f)
            return "massive";
        else if (size > 3.5f)
            return "huge";
        else if (size > 2.5f)
            return "large";
        else if (size > 1.5f)
            return "sizeable";
        else if (size > .5f)
            return "small";
        else if (size >= 0)
            return "tiny";
        return "";
    }

    internal static string BallSize(float size)
    {
        if (size > 4.5f)
            return "massive";
        else if (size > 3.5f)
            return "huge";
        else if (size > 2.5f)
            return "large";
        else if (size > 1.5f)
            return "sizeable";
        else if (size > .5f)
            return "small";
        else if (size >= 0)
            return "tiny";
        return "";
    }

    internal static string BreastSize(float size)
    {
        if (size > 4.5f)
            return "massive";
        else if (size > 3.5f)
            return "huge";
        else if (size > 2.5f)
            return "large";
        else if (size > 1.5f)
            return "sizeable";
        else if (size > .5f)
            return "small";
        else if (size >= 0)
            return "flat";
        return "";
    }
}

