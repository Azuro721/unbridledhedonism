﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


static class TestingWorld
{
    static internal void CreateTest()
    {
        List<Person> people = new List<Person>();
        Settings settings = new Settings
        {
            VoreAnger = 0,
            VoreKnowledge = true,
            FriendshipMultiplier = 1,
            RomanticMultiplier = 1,
            DigestType = DigestType.Both
        };

        Person first = new Person("First", "A", 1, Orientation.All, true, new Vec2(0,0));
        Person second = new Person("Second", "B", 1, Orientation.All, true, new Vec2(0,0));
        Person third = new Person("Third", "C", 1, Orientation.All, true, new Vec2(0,0));
        Person fourth = new Person("Fourth", "D", 1, Orientation.All, true, new Vec2(0,0));

        people.Add(first);
        people.Add(second);       
        people.Add(third);       
        //people.Add(fourth);       

        State.World = new World(people, settings, null, false);
        State.GameManager?.TileManager.DrawWorld();
        State.GameManager.ClickedPerson = State.World.ControlledPerson;
        State.GameManager.DisplayInfo();
        State.GameManager.CenterCameraOnTile(State.World.ControlledPerson.Position);

        foreach (Person i in people)
        {
            foreach (Person j in people)
            {
                if (i == j)
                    continue;
                SetHighRelation(i.GetRelationshipWith(j));
            }
            i.Position = new Vec2(9, 9);
        }


        //first.Position = new Vec2(9, 9);
        //second.Position = new Vec2(9, 9);
        //third.Position = new Vec2(9, 9);
        //fourth.Position = new Vec2(9, 9);


        first.Needs.ChangeEnergy(4);
        first.Personality.Voracity = 1;

        //Current Testing bit


        //InteractionList.List[InteractionType.StartSex].OnSucceed(first, second);
        //State.World.CurrentTurn = second;
        //State.World.AskPlayer(second, InteractionList.List[InteractionType.AskToOralVore]);



        first.AI.UseInteractionOnTarget(second, InteractionType.AskToUnbirth);
        first.VoreController.GetProgressOf(second).Stage = 2;
        first.Personality.Extroversion = 50000;

        State.World.ControlledPerson = first;

        third.AI.UseInteractionOnTarget(first, InteractionType.AskToBeUnbirthed);

        //first.AI.UseInteractionOnTarget(third, InteractionType.AskToOralVore);
        //first.VoreController.GetProgressOf(third).Stage = 3;
        //second.AI.UseInteractionOnTarget(fourth, InteractionType.AskToOralVore);
        //second.VoreController.GetProgressOf(fourth).Stage = 20;
        //second.Personality.Strength = 10000;
        //second.AI.UseInteractionOnTarget(first, InteractionType.PreyViolentStruggle);
        //first.VoreController.GetProgressOf(second).FreePrey();

        //first.AI.UseInteractionOnTarget(second, InteractionType.AskToOralVore);       
        //first.VoreController.GetProgressOf(second).Stage = 20;
        //first.AI.UseInteractionOnTarget(third, InteractionType.AskToOralVore);
        //first.VoreController.GetProgressOf(third).Stage = 20;
        //first.AI.UseInteractionOnTarget(fourth, InteractionType.AskToOralVore);
        //first.VoreController.GetProgressOf(fourth).Stage = 20;
        //first.VoreController.StomachDigestsPrey = true;
        //second.AI.UseInteractionOnTarget(third, InteractionType.PreyAskToOralVore);
        //second.VoreController.GetProgressOf(third).Stage = 20;
        //second.AI.UseInteractionOnTarget(fourth, InteractionType.PreyAskToOralVore);
        //second.VoreController.GetProgressOf(fourth).Stage = 20;
        //State.World.ControlledPerson = third;
        //second.Health = -600;

        //first.VoreController.SetPartDigest(first.VoreController.GetProgressOf(second).Location, true);

        //second.Health = 20;


        //End current testing bit

        //first.VoreController.StomachDigestsPrey = true;
        //second.AI.UseInteractionOnTarget(first, InteractionType.PreyConvinceToSpare);

        State.World.RunWaitingCallbacks();
        State.World.UpdatePlayerUI();

        //fourth.AI.UseInteractionOnTarget(first, InteractionType.Vore);

        void SetHighRelation(Relationship relation)
        {
            relation.Met = true;
            relation.FriendshipLevel = .99f;
            relation.RomanticLevel = .99f;
        }
    }
}
