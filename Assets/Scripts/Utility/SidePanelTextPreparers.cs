﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using static HelperFunctions;

internal enum PanelType
{
    MainPage,
    InfoPage,
    VorePage,
    MiscPage,
    CastTargetPage,
}
class SidePanelTextPreparer
{
    public const string ColorInteraction = "#ff881a";
    public const string ColorOutOfRange = "#ffdbb3";
    public const string ColorHasMana = "#e6b3ff";
    public const string ColorOutOfMana = "#8c8c8c";
    public const string ColorInitiateVore = "#ff5c33";
    public const string ColorSexInteractionOfferSelf = "#ff6699";
    public const string ColorSexInteraction = "#df9fbf";
    public const string ColorOfferSelf = "#ff6699";
    public const string ColorSpellEffect = "#e6b3ff";
    public const string ColorCriticalStatus = "#ff5c33";

    public const string ColorDefaultPreyColor = "yellow";
    public const string ColorStomach = "red";
    public const string ColorWomb = "#ff99ffff";
    public const string ColorBalls = "#ffff99ff";
    public const string ColorBowels = "#00cc00ff";

    public const string ColorTimeFrozen = "red";

    public const int MaxPreyDepthIndentation = 5;
    public const int IndentationSpaceAmount = 3;

    public Dictionary<SexInteractionType, InteractionType> SexInteractiondsOddsTypeMap = new Dictionary<SexInteractionType, InteractionType>(){
        { SexInteractionType.SexAskToBeOralVored, InteractionType.AskToBeOralVored },
        { SexInteractionType.SexAskToBeOralVoredEndo, InteractionType.AskToBeOralVoredEndo },
        { SexInteractionType.SexAskToBeUnbirthed, InteractionType.AskToBeUnbirthed },
        { SexInteractionType.SexAskToBeUnbirthedEndo, InteractionType.AskToBeUnbirthedEndo },
        { SexInteractionType.SexAskToBeCockVored, InteractionType.AskToBeCockVored },
        { SexInteractionType.SexAskToBeCockVoredEndo, InteractionType.AskToBeCockVoredEndo },
        { SexInteractionType.SexAskToBeAnalVored, InteractionType.AskToBeAnalVored },
        { SexInteractionType.SexAskToBeAnalVoredEndo, InteractionType.AskToBeAnalVoredEndo },
    };


    TextMeshProUGUI PlayerText => State.GameManager.PlayerText;
    TextMeshProUGUI TargetText => State.GameManager.TargetText;
    Person ClickedPerson => State.GameManager.ClickedPerson;
    Person PlayerControlled => State.World.ControlledPerson;

    internal void InfoForSquare(int x, int y)
    {
        if (Config.PlayerVisionActive() && LOS.Check(State.World.ControlledPerson, new Vec2(x, y)) == false)
            return;
        List<Person> peopleInSquare = new List<Person>();
        foreach (var person in State.World.GetPeople(false))
        {
            if (person.Position.x == x && person.Position.y == y && person == State.World.ControlledPerson)
            {
                peopleInSquare.AddRange(person.VoreController.GetAllSubPrey().Select(s => s.Target));
            }
            else if (person.Position.x == x && person.Position.y == y)
            {
                peopleInSquare.Add(person);
                peopleInSquare.AddRange(person.VoreController.GetAllSubPrey().Select(s => s.Target));
            }
        }
        if (peopleInSquare.Count == 1)
        {
            State.GameManager.ClickedPerson = peopleInSquare[0];
        }
        else if (peopleInSquare.Count > 1)
        {
            State.GameManager.ClickedPerson = null;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Multiple people in square, select intended target:");
            foreach (Person person in State.World.GetPeople(false).Where(s => s.Position.x == x && s.Position.y == y))
            {
                if (person == State.World.ControlledPerson)
                {
                    if (person.VoreController.HasPrey(VoreLocation.Any) == false)
                        continue;
                    sb.AppendLine($"{person.GetFullName()} (you)");
                }
                else
                {
                    sb.AppendLine($"{person.GetFullNameWithLink()}");
                }

                if (person.VoreController.HasPrey(VoreLocation.Any))
                {
                    sb.Append($"<color={ColorDefaultPreyColor}>");
                    foreach (var progress in person.VoreController.GetAllProgress(VoreLocation.Any))
                    {
                        AddPrey(ref sb, progress.Target, 1);
                    }
                    sb.Append("</color>");
                }
            }
            TargetText.text = sb.ToString();

        }
    }

    internal void ListAllPeople()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("List of all people, select intended target:");
        foreach (Person person in State.World.GetPeople(false))
        {
            if (person == State.World.ControlledPerson)
            {
                if (person.VoreController.HasPrey(VoreLocation.Any) == false)
                    continue;
                sb.AppendLine($"{person.GetFullName()} (you)");
            }
            else
            {
                sb.AppendLine(person.GetFullNameWithLink());
            }

            if (person.VoreController.HasPrey(VoreLocation.Any))
            {
                foreach (var progress in person.VoreController.GetAllProgress(VoreLocation.Any))
                {
                    AddPrey(ref sb, progress.Target, 1);
                }
            }
        }
        TargetText.text = sb.ToString();

    }

    internal void AddPrey(ref StringBuilder sb, Person person, int level)
    {
        int displayLevel = Math.Min(level, MaxPreyDepthIndentation);
        string color = $"<color={ColorDefaultPreyColor}>";
        if (Config.ColoredPreyNames)
        {
            var loc = person.GetMyVoreProgress().Location;
            switch (loc)
            {
                case VoreLocation.Stomach:
                    color = $"<color={ColorStomach}>";
                    break;
                case VoreLocation.Womb:
                    color = $"<color={ColorWomb}>";
                    break;
                case VoreLocation.Balls:
                    color = $"<color={ColorBalls}>";
                    break;
                case VoreLocation.Bowels:
                    color = $"<color={ColorBowels}>";
                    break;
            }
        }
        if (person == State.World.ControlledPerson)
            sb.AppendLine($"{new string(' ', IndentationSpaceAmount * displayLevel)}{color}{person.GetFullName()}(you){"</color>"}");
        else
            sb.AppendLine($"{new string(' ', IndentationSpaceAmount * displayLevel)}{color}{person.GetFullNameWithLink()}{"</color>"}");
        foreach (var progress in person.VoreController.GetAllProgress(VoreLocation.Any))
        {
            AddPrey(ref sb, progress.Target, level + 1);
        }
    }

    internal void GetInfo()
    {
        if (State.World.ControlledPerson != null && State.World.PlayerIsObserver())
        {
            StringBuilder sb = new StringBuilder();
            bool pureObserver = State.World.ControlledPerson.FirstName == "Observer" && State.World.ControlledPerson.LastName == "----";
            if (pureObserver)
            {
                sb.AppendLine("You are currently just an observer and thus can't reform directly, but can only swap into a character or continue to observe.");
            }
            else
            {
                sb.AppendLine($"<b>Controlled:</b> {State.World.ControlledPerson.GetFullNameWithLink()}");
                sb.AppendLine("You are currently dead and gone, you can continue to observe, or reform even if the options disallow it.");
            }
            if (State.World.Settings.WorldFrozen)
                sb.AppendLine($"<color={ColorTimeFrozen}>The world is currently frozen in the game settings, time isn't passing.</color>");
            sb.AppendLine();
            sb.AppendLine("To keep skipping time, use wait, or the ai skip options.");
            sb.AppendLine($"Your vision (controlling what text you see) is attached to {State.World.VisionAttachedTo.GetFullNameWithLink()}.");
            if (pureObserver == false)
                sb.AppendLine($"<color={ColorInteraction}>Reform</color>");
            sb.AppendLine($"<color={ColorInteraction}>SwapToVisionedCharacter</color>");
            PlayerText.text = sb.ToString();
        }
        else if (State.World.ControlledPerson != null && State.World.ControlledPerson.Dead == false)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"<b>Controlled:</b> {State.World.ControlledPerson.GetFullNameWithLink()}");
            if (State.World.Settings.WorldFrozen)
                sb.AppendLine($"<color={ColorTimeFrozen}>The world is currently frozen in the game settings, time isn't passing.</color>");
            
            if (Config.ActionsFirstInSidebar == false)
            {
                AddStatus(State.World.ControlledPerson, ref sb);
                AddNeeds(State.World.ControlledPerson, ref sb);
            }
                
            sb.AppendLine($" ");
            sb.AppendLine($"<b>Actions:</b>");
            MakeInteractionLines(sb, SelfActionList.List.Values.Where(i => i.Class != ClassType.CastSelf));
            if (State.World.ControlledPerson.ZoneContainsBed() && State.World.GetZone(State.World.ControlledPerson.Position).AllowedPeople.Any() == false)
            {
                sb.AppendLine($"<color={ColorInteraction}>Claim Room</color>");
            }

            if (CanUseMagic(State.World.ControlledPerson))
            {
                sb.AppendLine($" ");
                sb.AppendLine($"<b>Magic:</b> {GetManaAmountText()}");
                MakeInteractionLines(sb, SelfActionList.List.Values.Where(i => i.Class == ClassType.CastSelf), Color: GetManaColor());
            }

            sb.AppendLine($" ");

            if (State.World.Settings.CheckDigestion(true, DigestionAlias.CanSwitch))
            {
                if (State.World.ControlledPerson.VoreController.CapableOfVore())
                {
                    if (State.World.ControlledPerson.VoreController.OralVoreCapable && State.World.Settings.OralVoreEnabled)
                    {
                        if (State.World.ControlledPerson.VoreController.StomachDigestsPrey)
                        {
                            sb.AppendLine($"You're currently set to digest prey in your stomach, you can switch to <color={ColorInteraction}>Stomach Non-Fatal</color> mode");
                        }
                        else
                        {
                            sb.AppendLine($"You're currently set to hold prey in your stomach and not digest them, you can switch to <color={ColorInteraction}>Stomach Digestion</color> mode");
                        }
                    }
                    if (State.World.ControlledPerson.VoreController.AnalVoreCapable && State.World.Settings.AnalVoreEnabled && State.World.Settings.AnalVoreGoesDirectlyToStomach == false)
                    {
                        if (State.World.ControlledPerson.VoreController.BowelsDigestPrey)
                        {
                            sb.AppendLine($"You're currently set to digest prey in your bowels, you can switch to <color={ColorInteraction}>Bowels Non-Fatal</color> mode");
                        }
                        else
                        {
                            sb.AppendLine($"You're currently set to hold prey in your bowels and not digest them, you can switch to <color={ColorInteraction}>Bowels Digestion</color> mode");
                        }
                    }
                    if (State.World.ControlledPerson.VoreController.UnbirthCapable && State.World.Settings.UnbirthEnabled && State.World.ControlledPerson.GenderType.HasVagina)
                    {
                        if (State.World.ControlledPerson.VoreController.WombAbsorbsPrey)
                        {
                            sb.AppendLine($"You're currently set to melt prey in your womb, you can switch to <color={ColorInteraction}>Womb Non-Fatal</color> mode");
                        }
                        else
                        {
                            sb.AppendLine($"You're currently set to hold prey in your womb and not melt them, you can switch to <color={ColorInteraction}>Womb Melting</color> mode");
                        }
                    }
                    if (State.World.ControlledPerson.VoreController.CockVoreCapable && State.World.Settings.CockVoreEnabled && State.World.ControlledPerson.GenderType.HasDick)
                    {
                        if (State.World.ControlledPerson.VoreController.BallsAbsorbPrey)
                        {
                            sb.AppendLine($"You're currently set to melt prey in your balls, you can switch to <color={ColorInteraction}>Balls Non-Fatal</color> mode");
                        }
                        else
                        {
                            sb.AppendLine($"You're currently set to hold prey in your balls and not melt them, you can switch to <color={ColorInteraction}>Balls Melting</color> mode");
                        }
                    }


                }
            }

            AddStats(State.World.ControlledPerson, ref sb);
            if (Config.ActionsFirstInSidebar)
                AddNeeds(State.World.ControlledPerson, ref sb);
            AddPersonality(State.World.ControlledPerson, ref sb);
            GetDescription(State.World.ControlledPerson, ref sb);
            AddMisc(State.World.ControlledPerson, ref sb);
            sb.AppendLine($"<color={ColorInteraction}>Observer Mode</color>");
            PlayerText.text = sb.ToString();


        }
        else if (State.World.ControlledPerson != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"<b>Controlled:</b> {State.World.ControlledPerson.GetFullNameWithLink()}");
            if (State.World.Settings.WorldFrozen)
                sb.AppendLine($"<color={ColorTimeFrozen}>The world is currently frozen in the game settings, time isn't passing.</color>");

            sb.AppendLine("You are currently dead and in the process of being absorbed.");
            sb.AppendLine($"<color={ColorInteraction}>Observer Mode</color>");

            AddStats(State.World.ControlledPerson, ref sb);
            AddNeeds(State.World.ControlledPerson, ref sb);

            //sb.AppendLine("SwapToNewPerson");
            //sb.AppendLine("EndGame");
            PlayerText.text = sb.ToString();
        }
        else if (State.World.ControlledPerson == null)
        {
            StringBuilder sb = new StringBuilder();
            if (State.World.Settings.WorldFrozen)
                sb.AppendLine($"<color={ColorTimeFrozen}>The world is currently frozen in the game settings, time isn't passing.</color>");
            UnityEngine.Debug.LogWarning("No player exists");
            sb.AppendLine("You don't have a person selected, but you can still swap to a person by clicking on them, or end the game.");
            sb.AppendLine($"<color={ColorInteraction}>SwapToVisionedCharacter</color>");
            PlayerText.text = sb.ToString();
        }
        else
            PlayerText.text = "";

        if (ClickedPerson == null)
            TargetText.text = "";
        else if (State.World.ControlledPerson == null || State.World.PlayerIsObserver())
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("You're not controlling someone alive, but you can still view stats and switch to this person");
            sb.AppendLine($"<b>Target:</b> {ClickedPerson.GetFullNameWithLink()}");

            if (Config.ActionsFirstInSidebar == false)
                AddStatus(ClickedPerson, ref sb);

            sb.AppendLine($" ");
            sb.AppendLine($"<color={ColorInteraction}>SwitchVisionToThisCharacter</color>");
            sb.AppendLine($"<color={ColorInteraction}>SwapControlledCharacter</color>");

            AddStats(ClickedPerson, ref sb);
            AddNeeds(ClickedPerson, ref sb);
            AddPersonality(ClickedPerson, ref sb);
            GetDescription(ClickedPerson, ref sb);
            TargetText.text = sb.ToString();
        }
        else if (ClickedPerson == State.World.ControlledPerson)
        {
            TargetText.text = "";
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"<b>Target:</b> {ClickedPerson.GetFullNameWithLink()}");

            bool sidePage = false;

            if (State.GameManager.PanelInfoType == PanelType.MiscPage)
            {
                sidePage = true;
                AddMisc(ClickedPerson, ref sb);
            }
            else if (State.GameManager.PanelInfoType == PanelType.InfoPage)
            {
                sidePage = true;
                AddPersonality(ClickedPerson, ref sb);
            }
            else if (State.GameManager.PanelInfoType == PanelType.VorePage)
            {
                sidePage = true;
                if (ClickedPerson != null && State.World.ControlledPerson.ActiveSex?.Other == ClickedPerson)
                {
                    sb.AppendLine($"Current Sexual Position: {State.World.ControlledPerson.ActiveSex.Position}");
                    MakeInteractionLines(sb, SexInteractionList.List.Values.Where(i => i.Type >= SexInteractionType.SexAskToBeOralVored));
                }
                else
                    MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.Class == ClassType.VoreAskToBe));

            }
            else if (State.GameManager.PanelInfoType == PanelType.CastTargetPage)
            {
                sidePage = true;
                sb.AppendLine($"<b>Magic:</b> {GetManaAmountText()}");
                MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.Class == ClassType.CastTarget), Color: GetManaColor());
            }
            if (sidePage)
            {
                sb.AppendLine(" ");
                sb.AppendLine($"<color={ColorInteraction}>Main Page</color>");
                TargetText.text = sb.ToString();
                return;
            }

            if (State.World.ControlledPerson.Dead && ClickedPerson != State.World.ControlledPerson.FindMyPredator())
            {
                sb.AppendLine("You're dead, you can only perform the prey wait action on your predator.");

                if (ClickedPerson.Dead == false)
                {
                    if (Config.ActionsFirstInSidebar == false)
                    {
                        AddStatus(ClickedPerson, ref sb);
                        sb.AppendLine($" ");
                    }
                    AddStats(ClickedPerson, ref sb);
                    AddNeeds(ClickedPerson, ref sb);
                }

                sb.AppendLine($" ");
                sb.AppendLine($"<color={ColorInteraction}>Personality Info</color>");
                sb.AppendLine($"<color={ColorInteraction}>Miscellaneous Info</color>");
                sb.AppendLine($"<color={ColorInteraction}>SwapControlledCharacter</color>");
                TargetText.text = sb.ToString();
                return;
            }
            if (ClickedPerson.BeingEaten)
            {
                if (ClickedPerson.Dead)
                {
                    sb.AppendLine("They're dead, you can't perform any actions on them.");
                    TargetText.text = sb.ToString();
                    return;
                }
                bool EatenPlayer = ClickedPerson.VoreController.ContainsPerson(State.World.ControlledPerson, VoreLocation.Any);
                bool EatenByPlayer = State.World.ControlledPerson.VoreController.ContainsPerson(ClickedPerson, VoreLocation.Any);
                if (EatenByPlayer)
                {
                    if (State.World.ControlledPerson.VoreController.CurrentSwallow(VoreLocation.Any)?.Target == ClickedPerson)
                    {
                        sb.AppendLine("You're still in the process of swallowing them, you can't perform any actions on them.");

                        if (Config.ActionsFirstInSidebar == false)
                            AddStatus(ClickedPerson, ref sb);
                    }
                    else
                    {
                        sb.AppendLine("They are inside of you, you can only perform limited actions with them.");
                        if (State.World.ControlledPerson.VoreController.TargetIsBeingDigested(ClickedPerson) == false && State.World.ControlledPerson.VoreController.GetProgressOf(ClickedPerson).ExpectingEndosoma == false)
                            sb.AppendLine("They have consented to be digested");

                        if (Config.ActionsFirstInSidebar == false)
                            AddStatus(ClickedPerson, ref sb);


                        sb.AppendLine($" ");
                        sb.AppendLine($"<b>Actions:</b>");
                        MakeInteractionLines(sb, InteractionList.List.Values.Where(s => s.UsedOnPrey && s.Class != ClassType.CastTarget));
                        if (CanUseMagic(State.World.ControlledPerson))
                        {
                            sb.AppendLine($" ");
                            sb.AppendLine($"<b>Magic:</b> {GetManaAmountText()}");
                            MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.UsedOnPrey && i.Class == ClassType.CastTarget), Color: GetManaColor());
                        }
                    }
                }
                else if (EatenPlayer)
                {
                    if (Config.ActionsFirstInSidebar == false)
                        AddStatus(ClickedPerson, ref sb);

                    MakeInteractionLines(sb, InteractionList.List.Values.Where(s => s.Type >= InteractionType.PreyWillingYell && s.Type <= InteractionType.PreyBecomeWilling));
                }
                else
                {

                    if (State.World.ControlledPerson.BeingEaten && State.World.ControlledPerson.FindMyPredator().VoreController.AreSisterPrey(State.World.ControlledPerson, ClickedPerson))
                    {
                        sb.AppendLine("You're within the same body cavity as them.");

                        if (Config.ActionsFirstInSidebar == false)
                            AddStatus(ClickedPerson, ref sb);

                        MakeInteractionLines(sb, InteractionList.List.Values
                            .Where(i => i.UsedOnPrey && (i.Type < InteractionType.PreyEatOtherPrey || i.Type > InteractionType.PreyAskToAnalVoreDigest))
                            .Where(i => i.Class != ClassType.CastTarget)
                            .Where(i => !(i.Type >= InteractionType.TalkWithPrey && i.Type <= InteractionType.TauntPrey))
                        );
                        if (CanUseMagic(State.World.ControlledPerson))
                        {
                            sb.AppendLine($" ");
                            sb.AppendLine($"<b>Magic:</b> {GetManaAmountText()}");
                            MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.UsedOnPrey && i.Class == ClassType.CastTarget), Color: GetManaColor());
                        }
                        MakeSameTileLine(sb, "Initiate Vore", Color: ColorInitiateVore);
                    }
                    else
                    {
                        sb.AppendLine("Person is inside of someone, you can only perform limited actions with them.");
                        MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.UsedOnPrey && i.Class != ClassType.CastTarget));
                        if (CanUseMagic(State.World.ControlledPerson))
                        {
                            sb.AppendLine($" ");
                            sb.AppendLine($"<b>Magic:</b> {GetManaAmountText()}");
                            MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.UsedOnPrey && i.Class == ClassType.CastTarget), Color: GetManaColor());
                        }
                    }

                }

                AddStats(ClickedPerson, ref sb);
                AddNeeds(ClickedPerson, ref sb);

                sb.AppendLine($" ");
                sb.AppendLine($"<b>Relationships:</b>");
                HelperFunctions.GetRelationshipsPeople(ref sb, State.World.ControlledPerson, ClickedPerson);
                HelperFunctions.GetRelationships(ref sb, State.World.ControlledPerson, ClickedPerson);

                sb.AppendLine($" ");
                sb.AppendLine($"<color={ColorInteraction}>Personality Info</color>");
                sb.AppendLine($"<color={ColorInteraction}>Miscellaneous Info</color>");
                sb.AppendLine($"<color={ColorInteraction}>SwapControlledCharacter</color>");
                TargetText.text = sb.ToString();
                return;
            }

            if (Config.ActionsFirstInSidebar == false)
                AddStatus(ClickedPerson, ref sb);

            sb.AppendLine($" ");

            sb.AppendLine($"<b>Actions:</b>");
            if (State.World.ControlledPerson.Position != State.GameManager.ClickedPerson.Position && ClickedPerson.Dead == false && State.World.ControlledPerson.BeingEaten == false)
                if (PathFinder.GetPath(State.World.ControlledPerson.Position, State.GameManager.ClickedPerson.Position, State.World.ControlledPerson) != null)
                    sb.AppendLine($"<color={ColorInteraction}>Move Towards</color>");

            ClickedPerson.GetStreamingActions(ref sb);
            if (ClickedPerson != null && State.World.ControlledPerson.ActiveSex?.Other == ClickedPerson)
            {
                sb.AppendLine($"Your Current Sexual Position: {State.World.ControlledPerson.ActiveSex.Position}");
                MakeInteractionLines(sb, SexInteractionList.List.Values.Where(i => i.Type < SexInteractionType.KissVore), Color: ColorSexInteraction);

                sb.AppendLine(" ");
                MakeSameTileLine(sb, "Initiate Vore", Color: ColorInitiateVore);
                MakeSameTileLine(sb, "Offer Self", Color: ColorOfferSelf);

                if (CanUseMagic(State.World.ControlledPerson))
                {
                    if (State.World.ControlledPerson.Magic.Mana >= 1)
                        sb.AppendLine($"<color={ColorHasMana}>Cast Spell</color>");
                    else
                        sb.AppendLine($"<color={ColorOutOfMana}>Cast Spell</color> (out of mana)");

                }
            }
            else if (ClickedPerson != null && ClickedPerson.VoreController.ContainsPerson(State.World.ControlledPerson, VoreLocation.Any))
            {
                MakeInteractionLines(sb, InteractionList.List.Values.Where(i => i.Type >= InteractionType.PreyWillingYell && i.Type <= InteractionType.PreyBecomeWilling));
            }
            else if (State.World.ControlledPerson.BeingEaten == false)
            {

                ClassType[] SkipTypes = { ClassType.VoreConsuming, ClassType.VoreAskThem, ClassType.VoreAskToBe, ClassType.CastTarget };
                MakeInteractionLines(sb, InteractionList.List.Values.Where(i => SkipTypes.Contains(i.Class) == false));

                sb.AppendLine(" ");
                MakeSameTileLine(sb, "Initiate Vore", Color: ColorInitiateVore);
                MakeSameTileLine(sb, "Offer Self", Color: ColorOfferSelf);

                if (CanUseMagic(State.World.ControlledPerson))
                {
                    if (State.World.ControlledPerson.Magic.Mana >= 1)
                        sb.AppendLine($"<color={ColorHasMana}>Cast Spell</color>");
                    else
                        sb.AppendLine($"<color={ColorOutOfMana}>Cast Spell</color> (out of mana)");
                }

            }
            else
                sb.AppendLine($"You're being eaten, you can only interact with your predator, or other prey in that predator.");

            //bool canSee = LOS.Check(State.World.ControlledPerson.Position, ClickedPerson.Position);
            //sb.AppendLine($"Can see {canSee}");

            AddStats(ClickedPerson, ref sb);
            AddNeeds(ClickedPerson, ref sb);

            sb.AppendLine($" ");
            sb.AppendLine($"<b>Relationships:</b>");
            GetRelationshipsPeople(ref sb, State.World.ControlledPerson, ClickedPerson);
            GetRelationships(ref sb, State.World.ControlledPerson, ClickedPerson);

            GetDescription(ClickedPerson, ref sb);

            sb.AppendLine($" ");
            sb.AppendLine($"<color={ColorInteraction}>Personality Info</color>");
            sb.AppendLine($"<color={ColorInteraction}>Miscellaneous Info</color>");
            sb.AppendLine($"<color={ColorInteraction}>SwapControlledCharacter</color>");
            TargetText.text = sb.ToString();

        }

        void AddStatus(Person person, ref StringBuilder sb) 
        {
            person.GetStreamingActions(ref sb);
            if (Config.DebugViewGoals && (State.World.ControlledPerson != person || State.World.LastPlayerTurnWasAI))
                person.AI.GetGoals(ref sb);
            AddSpellEffects(person, ref sb);
        }

        void AddStats(Person person, ref StringBuilder sb)
        {
            sb.AppendLine($" ");
            sb.AppendLine($"<b>About:</b>");

            if (Config.ActionsFirstInSidebar)
                AddStatus(person, ref sb);

            var race = RaceManager.GetRace(person.Race);
            if (race.NamedCharacter)
                sb.AppendLine($"Race: {person.Race} ({race.ParentRaceString})");
            else
                sb.AppendLine($"Race: {person.Race}");
            sb.AppendLine($"Sex: {person.GenderType.Name}");
            sb.AppendLine($"Orientation: {person.Romance.Orientation}");
            if (person.BeingEaten)
            {
                var pred = person.FindMyPredator();
                if (pred != null)
                {
                    var progress = pred.VoreController.GetProgressOf(person);
                    if (progress != null) //Just to be safe
                    {
                        if (progress.IsSwallowing())
                        {
                            sb.AppendLine($"Is being taken into the {progress.Location} of {pred.GetFullNameWithLink()}");
                        }
                        else
                        {
                            sb.AppendLine($"Is in the {progress.Location} of {pred.GetFullNameWithLink()}");
                        }
                    }
                }
                else
                {
                    sb.AppendLine($"Is Being Eaten!");
                }
            }

            if (person.VoreController.HasPrey(VoreLocation.Any))
            {
                person.VoreController.GetPredInfo(ref sb);
            }

            sb.AppendLine($"Clothes: {person.ClothingStatus}");
        }

        void AddNeeds(Person person, ref StringBuilder sb)
        {
            sb.AppendLine($" ");
            sb.AppendLine($"<b>Needs:</b>");
            if (Config.DebugViewPreciseValues)
            {
                if (person.Health < Constants.HealthMax)
                    sb.AppendLine($"Health: {GetWord.Healthiness(person.Health)} ({person.Health})");
                if (CanUseMagic(person) && State.World.ControlledPerson != person)
                    sb.AppendLine($"Magic: {Math.Round(person.Magic.Mana, 3)}/{Math.Floor(person.Magic.MaxMana + person.Boosts.MaxMana)} mana");
                sb.AppendLine($"Hunger: {GetWord.Hunger(person.Needs.Hunger)} ({Math.Round(1 - person.Needs.Hunger, 3)})");
                sb.AppendLine($"Energy: {GetWord.Tiredness(person.Needs.Energy)} ({Math.Round(1 - person.Needs.Energy, 3)})");
                sb.AppendLine($"Cleanliness: {GetWord.Cleanliness(person.Needs.Cleanliness)} ({Math.Round(1 - person.Needs.Cleanliness, 3)})");
                sb.AppendLine($"Horniness: {GetWord.Horniness(person.Needs.Horniness)} ({Math.Round(person.Needs.Horniness, 3)})");

            }
            else
            {
                if (person.Health < Constants.HealthMax)
                    sb.AppendLine($"Health: {GetWord.Healthiness(person.Health)}");
                if (CanUseMagic(person) && State.World.ControlledPerson != person)
                    sb.AppendLine($"Magic: {Math.Floor(person.Magic.Mana)}/{Math.Floor(person.Magic.MaxMana + person.Boosts.MaxMana)} mana");
                sb.AppendLine($"Hunger: {GetWord.Hunger(person.Needs.Hunger)}");
                sb.AppendLine($"Tiredness: {GetWord.Tiredness(person.Needs.Energy)}");
                sb.AppendLine($"Cleanliness: {GetWord.Cleanliness(person.Needs.Cleanliness)}");
                sb.AppendLine($"Horniness: {GetWord.Horniness(person.Needs.Horniness)}");
            }


        }

        void AddSpellEffects(Person person, ref StringBuilder sb)
        {
            if (person.Needs.Hunger >= 1)
                sb.AppendLine($"<color={ColorCriticalStatus}>Starving</color>");

            if (person.Needs.Energy >= 1)
                sb.AppendLine($"<color={ColorCriticalStatus}>Exhausted</color>");

            if (person.Magic.Duration_Passdoor > 0)
                sb.AppendLine($"<color={ColorSpellEffect}>Phasing</color> ({person.Magic.Duration_Passdoor})");

            if (person.Magic.Duration_Freeze > 0)
                sb.AppendLine($"<color={ColorSpellEffect}>Frozen</color> ({person.Magic.Duration_Freeze})");

            if (person.Magic.Duration_Hunger > 0)
                sb.AppendLine($"<color={ColorSpellEffect}>Unnatural Hunger</color> ({person.Magic.Duration_Hunger})");

            if (State.World.Settings.PermaSizeChange)
            {
                if (person.Magic.Duration_Big2 > 0 && person.Magic.Duration_Big1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Giant</color>");
                else if (person.Magic.Duration_Big1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Large</color>");
                if (person.Magic.Duration_Small1 > 0 && person.Magic.Duration_Small2 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Tiny</color>");
                else if (person.Magic.Duration_Small1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Small</color>");
            }
            else
            {
                if (person.Magic.Duration_Big2 > 0 && person.Magic.Duration_Big1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Giant</color> ({person.Magic.Duration_Big2})");
                else if (person.Magic.Duration_Big1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Large</color> ({person.Magic.Duration_Big1})");
                if (person.Magic.Duration_Small1 > 0 && person.Magic.Duration_Small2 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Tiny</color> ({person.Magic.Duration_Small2})");
                else if (person.Magic.Duration_Small1 > 0)
                    sb.AppendLine($"<color={ColorSpellEffect}>Small</color> ({person.Magic.Duration_Small1})");
            }
        }


        void AddPersonality(Person person, ref StringBuilder sb)
        {
            sb.AppendLine($" ");
            sb.AppendLine($"<b>Personality:</b>");
            if (Config.DebugViewPreciseValues)
            {
                sb.AppendLine($"Charisma: {GetWord.Charisma(person.Personality.Charisma)} ({Math.Round(person.Personality.Charisma, 3)})");
                sb.AppendLine($"Extroversion: {GetWord.Extroversion(person.Personality.Extroversion)} ({Math.Round(person.Personality.Extroversion, 3)})");
                sb.AppendLine($"Strength: {GetWord.Strength(person.Personality.Strength)} ({Math.Round(person.Personality.Strength, 3)})");
                sb.AppendLine($"Dominance: {GetWord.Dominance(person.Personality.Dominance)} ({Math.Round(person.Personality.Dominance, 3)})");
                sb.AppendLine($"Kindness: {GetWord.Kindness(person.Personality.Kindness)} ({Math.Round(person.Personality.Kindness, 3)})");
                sb.AppendLine($"Promiscuity: {GetWord.Promiscuity(person.Personality.Promiscuity)} ({Math.Round(person.Personality.Promiscuity, 3)})");
                sb.AppendLine($"Sex Drive: {GetWord.SexDrive(person.Personality.SexDrive)} ({Math.Round(person.Personality.SexDrive, 3)})");

                sb.AppendLine($" ");

            }
            else
            {
                sb.AppendLine($"Charisma: {GetWord.Charisma(person.Personality.Charisma)}");
                sb.AppendLine($"Extroversion: {GetWord.Extroversion(person.Personality.Extroversion)}");
                sb.AppendLine($"Strength: {GetWord.Strength(person.Personality.Strength)}");
                sb.AppendLine($"Dominance: {GetWord.Dominance(person.Personality.Dominance)}");
                sb.AppendLine($"Kindness: {GetWord.Kindness(person.Personality.Kindness)}");
                sb.AppendLine($"Promiscuity: {GetWord.Promiscuity(person.Personality.Promiscuity)}");
                sb.AppendLine($"Sex Drive: {GetWord.SexDrive(person.Personality.SexDrive)}");
            }
            sb.AppendLine($"Preferred Clothing: {person.Personality.PreferredClothing}");
            sb.AppendLine($"Cheats on partner: {person.Personality.CheatOnPartner}");
            sb.AppendLine($"Cheat Acceptance: {person.Personality.CheatAcceptance}");

            sb.AppendLine($" ");
            sb.AppendLine($"<b>Vore Personality:</b>");
            if (Config.DebugViewPreciseValues)
            {
                if (person.VoreController.CapableOfVore())
                {
                    sb.AppendLine($"Voracity: {GetWord.Voracity(person.Personality.Voracity)} ({Math.Round(person.Personality.Voracity, 3)})");
                    sb.AppendLine($"Pred Willingness: {GetWord.PredWillingness(person.Personality.PredWillingness)} ({Math.Round(person.Personality.PredWillingness, 3)})");
                    sb.AppendLine($"Pred Loyalty: {GetWord.PredLoyalty(person.Personality.PredLoyalty)} ({Math.Round(person.Personality.PredLoyalty, 3)})");
                }
                sb.AppendLine($"Prey Willingness: {GetWord.PreyWillingness(person.Personality.PreyWillingness)} ({Math.Round(person.Personality.PreyWillingness, 3)})");
                sb.AppendLine($"Voraphilia: {GetWord.Voraphilia(person.Personality.Voraphilia)} ({Math.Round(person.Personality.Voraphilia, 3)})");
                sb.AppendLine($"Prey Digestion Interest: {GetWord.DigestionWillingness(person.Personality.PreyDigestionInterest)} ({Math.Round(person.Personality.PreyDigestionInterest, 3)})");
                if (person.VoreController.HasBellyPrey())
                    sb.AppendLine($"Current Belly Size: ({Math.Round(person.VoreController.BellySize(), 3)})");
                if (person.VoreController.HasBallsPrey())
                    sb.AppendLine($"Current Belly Size: ({Math.Round(person.VoreController.BallsSize(), 3)})");
            }
            else
            {
                if (person.VoreController.CapableOfVore())
                {
                    sb.AppendLine($"Voracity: {GetWord.Voracity(person.Personality.Voracity)}");
                    sb.AppendLine($"Pred Willingness: {GetWord.PredWillingness(person.Personality.PredWillingness)}");
                    sb.AppendLine($"Pred Loyalty: {GetWord.PredLoyalty(person.Personality.PredLoyalty)}");
                }
                sb.AppendLine($"Prey Willingness: {GetWord.PreyWillingness(person.Personality.PreyWillingness)}");
                sb.AppendLine($"Voraphilia: {GetWord.Voraphilia(person.Personality.Voraphilia)}");
                sb.AppendLine($"Prey Digestion Interest: {GetWord.DigestionWillingness(person.Personality.PreyDigestionInterest)}");
            }
            if (person.VoreController.CapableOfVore())
            {
                sb.AppendLine($"Vore Preference: {person.Personality.VorePreference}");
                if (person.Personality.EndoDominator)
                    sb.AppendLine($"Likes Endo Domination");
            }
            if (person.VoreController.TotalDigestions > 0)
                sb.AppendLine($"Total Digestions: {person.VoreController.TotalDigestions}");


            if (person.Quirks.Any())
            {
                sb.AppendLine($" ");
                sb.AppendLine("<b>Quirks:</b>");
                foreach (var trait in person.Quirks)
                {
                    sb.AppendLine(trait.ToString());
                }
            }
            if (person.Traits.Any())
            {
                sb.AppendLine($" ");
                sb.AppendLine("<b>Traits:</b>");
                foreach (var trait in person.Traits)
                {
                    sb.AppendLine(trait.ToString());
                }
            }


        }

        void AddMisc(Person person, ref StringBuilder sb)
        {
            sb.AppendLine(" ");
            sb.AppendLine("<b>Miscellaneous Info:</b>");
            sb.AppendLine($"Turns in game {State.World.Turn - person.MiscStats.TurnAdded}");
            if (person.MiscStats.TotalWeightGain > 0)
            {
                sb.AppendLine($"Total weight gain: {MiscUtilities.ConvertedWeight(person.MiscStats.TotalWeightGain)}");
            }
            if (person.MiscStats.TotalWeightLoss > 0)
            {
                sb.AppendLine($"Total weight loss: {MiscUtilities.ConvertedWeight(person.MiscStats.TotalWeightLoss)}");
            }
            List("Started Swallowing", person.MiscStats.TimesSwallowedOtherStart, ref sb);
            List("Finished Swallowing", person.MiscStats.TimesSwallowedOther, ref sb);
            List("Started Unbirthing", person.MiscStats.TimesUnbirthedOtherStart, ref sb);
            List("Finished Unbirthing", person.MiscStats.TimesUnbirthedOther, ref sb);
            List("Started Cockvoring", person.MiscStats.TimesCockVoredOtherStart, ref sb);
            List("Finished Cockvoring", person.MiscStats.TimesCockVoredOther, ref sb);
            List("Started Analvoring", person.MiscStats.TimesAnalVoredOtherStart, ref sb);
            List("Finished Analvoring", person.MiscStats.TimesAnalVoredOther, ref sb);
            List("Digested Others", person.MiscStats.TimesDigestedOther, ref sb);

            List("Started Being Swallowed", person.MiscStats.TimesBeenSwallowedStart, ref sb);
            List("Finished Being Swallowed", person.MiscStats.TimesBeenSwallowed, ref sb);
            List("Started Being Unbirthed", person.MiscStats.TimesBeenUnbirthedStart, ref sb);
            List("Finished Being Unbirthed", person.MiscStats.TimesBeenUnbirthed, ref sb);
            List("Started Being Cockvored", person.MiscStats.TimesBeenCockVoredStart, ref sb);
            List("Finished Being Cockvored", person.MiscStats.TimesBeenCockVored, ref sb);
            List("Started Being Analvored", person.MiscStats.TimesBeenAnalVoredStart, ref sb);
            List("Finished Being Analvored", person.MiscStats.TimesBeenAnalVored, ref sb);
            List("Been Digested", person.MiscStats.TimesBeenDigested, ref sb);


            void List(string desc, int stat, ref StringBuilder sb2)
            {
                if (stat > 0)
                {
                    sb2.AppendLine($"{desc}: {stat}");
                }
            }
        }

        void GetDescription(Person person, ref StringBuilder sb)
        {
            if (person == null || person.PartList == null)
            {
                UnityEngine.Debug.LogWarning("This shouldn't have triggered");
                return;
            }

            person.PartList.GetDescription(person, ref sb);
            var dummy = new Interaction(person, person, true, InteractionType.None);
            if (person.VoreController.BellySize() > 0)
            {
                string bellyDesc = MessageManager.GetText(dummy, Assets.Scripts.TextGeneration.BodyPartDescriptionType.BellySizeDescription);
                if (bellyDesc != "")
                    sb.AppendLine(bellyDesc);
            }
            if (person.VoreController.BallsSize() > 0)
            {
                string ballsDesc = MessageManager.GetText(dummy, Assets.Scripts.TextGeneration.BodyPartDescriptionType.BallsSizeDescription);
                if (ballsDesc != "")
                    sb.AppendLine(ballsDesc);
            }

        }
    }


    public void MakeSameTileLine(StringBuilder sb, string text, Person A = null, Person B = null, string Color = ColorInteraction, string OutOfRangeColor = ColorOutOfRange)
    {
        A ??= this.ClickedPerson;
        B ??= this.PlayerControlled;
        if (ClickedPerson.Position != State.World.ControlledPerson.Position)
        {
            sb.AppendLine($"<color={OutOfRangeColor}>{text}</color> (out of range)");
        }
        else
        {
            sb.AppendLine($"<color={Color}>{text}</color>");
        }
    }
    public void MakeInteractionLines(StringBuilder sb, IEnumerable<SelfActionBase> list, Person Person = null, string Color = ColorInteraction)
    {
        Person ??= this.PlayerControlled;
        foreach (var i in list) MakeInteractionLine(sb, i, Person, Color);
    }
    public void MakeInteractionLines(StringBuilder sb, IEnumerable<SexInteractionBase> list, Person Person = null, string Color = ColorSexInteraction)
    {
        Person ??= this.ClickedPerson;
        foreach (var i in list) MakeInteractionLine(sb, i, Person, Color);
    }
    public void MakeInteractionLines(StringBuilder sb, IEnumerable<InteractionBase> list, Person Person = null, string Color = ColorInteraction, string OutOfRangeColor = ColorOutOfRange)
    {
        Person ??= this.ClickedPerson;
        foreach (var i in list) MakeInteractionLine(sb, i, Person, Color, OutOfRangeColor);
    }
    public void MakeInteractionLine(StringBuilder sb, SexInteractionBase Interaction, Person Person, string Color = ColorSexInteraction)
    {
        if (!Interaction.PositionAllows(State.World.ControlledPerson)) return;
        if (!Interaction.AppearConditional(State.World.ControlledPerson, Person)) return;

        string ChanceText = "";
        if (SexInteractiondsOddsTypeMap.ContainsKey(Interaction.Type))
        {
            var type = SexInteractiondsOddsTypeMap[Interaction.Type];
            ChanceText = $" ({GetChanceText(InteractionList.List[type].SuccessOdds(State.World.ControlledPerson, Person))})";
        }

        sb.AppendLine($"<color={Color}>{Interaction.Name}</color>{ChanceText}");

    }
    public void MakeInteractionLine(StringBuilder sb, SelfActionBase Interaction, Person Person, string Color = ColorInteraction)
    {
        if (Interaction.AppearConditional != null && !Interaction.AppearConditional(Person)) return;
        float? odds = Interaction?.SuccessOdds(Person);
        string ChanceText = odds.HasValue && odds.Value != 1 ? $" ({GetChanceText(Interaction.SuccessOdds(Person))})" : "";
        sb.AppendLine($"<color={Color}>{Interaction.Name}</color>{ChanceText}");

    }
    public void MakeInteractionLine(StringBuilder sb, InteractionBase Interaction, Person Person, string Color = ColorInteraction, string OutOfRangeColor = ColorOutOfRange)
    {
        if (Interaction.AppearConditional != null && !Interaction.AppearConditional(State.World.ControlledPerson, Person)) return;
        if (Interaction.Range < State.World.ControlledPerson.Position.GetNumberOfMovesDistance(Person.Position))
        {
            sb.AppendLine($"<color={OutOfRangeColor}>{Interaction.Name}</color> (out of range)");
            return;
        }
        string ChanceText = Interaction.SuccessOdds == null ? "" : $" ({GetChanceText(Interaction.SuccessOdds(State.World.ControlledPerson, Person))})";
        sb.AppendLine($"<color={Color}>{Interaction.Name}</color>{ChanceText}");
    }
    public string GetChanceText(float chance)
    {
        return $"{100 * Math.Round(chance, 2)}%";
    }
    public string GetManaColor(Person Person = null, float ComparisonValue = 1)
    {
        Person ??= this.PlayerControlled;
        return Person.Magic.Mana >= ComparisonValue ? ColorHasMana : ColorOutOfMana;
    }
    public string GetManaAmountText(Person Person = null)
    {
        Person ??= this.PlayerControlled;
        if (Config.DebugViewPreciseValues)
        {
            return $"({Math.Round(Person.Magic.Mana, 3)}/{Math.Floor(Person.Magic.MaxMana + Person.Boosts.MaxMana)} mana)";
        }
        return $"({Math.Floor(Person.Magic.Mana)}/{Math.Floor(Person.Magic.MaxMana + Person.Boosts.MaxMana)} mana)";
    }
}
