﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



static class Sound
{
    static internal List<Vec2> GetAffectedSquares(Vec2 start, int volume)
    {
        if (volume < 1)
        {
            var list = new List<Vec2>() { start };
            return list;
        }

        int h = State.World.Zones.GetLength(0);
        int w = State.World.Zones.GetLength(1);

        List<Vec2> visited = new List<Vec2>();
        Dictionary<Vec2, int> volumeAtSquare = new Dictionary<Vec2, int>();
        Zone zone;
        Stack<Vec2> stack = new Stack<Vec2>();
        stack.Push(new Vec2(start.x, start.y));
        volumeAtSquare[new Vec2(start.x, start.y)] = volume;
        while (stack.Count > 0)
        {
            Vec2 p = stack.Pop();
            int x = p.x;
            int y = p.y;
            int vol = volumeAtSquare[new Vec2(x, y)] - 1;
            if (y < 0 || y > h - 1 || x < 0 || x > w - 1 || vol < 0)
                continue;
            if (visited.Contains(p))
            {
                continue;
            }
            zone = State.World.GetZone(new Vec2(x, y));
            if (zone == null)
                continue;           
            visited.Add(p);
            Vec2 newSquare = new Vec2(x + 1, y);
            stack.Push(newSquare);
            volumeAtSquare[newSquare] = vol - State.World.BorderController.GetBorder(p, newSquare).SoundBuffer;
            newSquare = new Vec2(x - 1, y);
            stack.Push(newSquare);
            volumeAtSquare[newSquare] = vol - State.World.BorderController.GetBorder(p, newSquare).SoundBuffer;
            newSquare = new Vec2(x, y + 1);
            stack.Push(newSquare);
            volumeAtSquare[newSquare] = vol - State.World.BorderController.GetBorder(p, newSquare).SoundBuffer;
            newSquare = new Vec2(x, y - 1);
            stack.Push(newSquare);
            volumeAtSquare[newSquare] = vol - State.World.BorderController.GetBorder(p, newSquare).SoundBuffer;
        }
        return visited;
    }
}

