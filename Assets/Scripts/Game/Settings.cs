using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum DigestType
{
    Both,
    DigestionOnly,
    EndoOnly,
    OnlyPlayerCanDigest,
}

internal enum DigestionAlias
{
    CanVore,
    CanSwitch,
    CanEndo,
    Any
}

internal enum LoadCharacterType
{
    Random,    
    Saved,
    Templates,
}

public enum MagicAvailability
{
    Default,
    Everyone,
    NoMagic,
}

public class Settings
{
    [OdinSerialize, ProperName("World Frozen")]
    [Description("A Debug/testing option that causes only the currently selected player to be able to move. Pauses needs and digestion, though performing consuming actions (like swallowing) will perform normal digestion on all prey inside on any turn its performed.")]
    internal bool WorldFrozen = false;

    [OdinSerialize, ProperName("Vore Knowledge")]
    [Description("How well vore is known about, mostly just cosmetic, affects how surprised people act when they see someone being eaten.")]    
    public bool VoreKnowledge = true;
    [OdinSerialize, ProperName("Secretive Vore")]
    [Description("AI predators will never vore outside of a dorm room, and ai prey will never use the area prey tease.  Note that characters with a vendetta will ignore this rule, also that this will greatly reduce the number of vore attempts.")]
    public bool SecretiveVore = true;
    [OdinSerialize, ProperName("Vore Hunting Bias")]
    [FloatRange(0, 2)]
    [Description("How often preds will enter hunting mode when hungry.  Note that is the main source of forced vore, there is only vendettas and vore during sex besides hunting for prey.")]
    internal float VoreHuntingBias = 1;
    [OdinSerialize, ProperName("Pred Ask Bias")]
    [FloatRange(0, 8)]
    [Description("How much predators value asking prey to be eaten.  Setting it higher will also increase the amount of total vore.")]
    internal float AskPredWillingness = 1;
    [OdinSerialize, ProperName("Pred Forced Bias")]
    [FloatRange(0, 8)]
    [Description("How much predators like using forced vore. Hunting is the main source of this, and setting it higher means they're more likely to pick people that they see as targets.  If set below .2, they will start mixing in asking into hunting (To give them something to do if hunting is still enabled but they can't really force).  If set to 0 they will avoid it completely, except for characters with vendettas.")]
    internal float ForcedPredWillingness = 1;
    [OdinSerialize, ProperName("Digestion Bias")]
    [FloatRange(0, 2)]
    [Description("How much predators like to digest prey.  This changes both how often it happens in relation to endo, and also how common it is in general")]
    internal float DigestionBias = 1;
    [OdinSerialize, ProperName("Endo Bias")]
    [FloatRange(0, 2)]
    [Description("How much predators like to have prey that they don't digest.  This changes both how often it happens in relation to digestion, and also how common it is in general")]
    internal float EndoBias = 1;
    [OdinSerialize, ProperName("Vore Anger")]
    [FloatRange(0, 1)]
    [Description("How upset people will be witnessing vore actions, particularly with people they care about or themselves, at 0 there will be no relationship drops from vore.  ")]
    internal float VoreAnger = 1;
    [OdinSerialize, ProperName("Help Prey Bias")]
    [FloatRange(0, 4)]
    [Description("How often people will try to help prey.  Setting it to 0 disables this entirely.  The actual result depends on a few factors, including how well they know the predator and prey.")]
    internal float HelpPreyBias = 1;
    [OdinSerialize, ProperName("Escape Rate")]
    [FloatRange(0, 4)]
    [Description("How adept prey are at escaping using the struggle commands.  Set it to 0 to make those completely ineffective.")]
    internal float EscapeRate = 1;
    [OdinSerialize, ProperName("Freeing Odds")]
    [FloatRange(0, 4)]
    [Description("A straight multiplier to the odds of someone freeing prey from someone else.   Note that partially consumed prey are significantly easier to free.")]
    internal float FreeingOdds = 1;
    [OdinSerialize, ProperName("Pred Conviction")]
    [Description("How firmly predators are in their intent to digest.  At 1 they won't consider those requests, at 0 they're easily persuaded, the default is .5")]
    internal float PredConviction = 1;
    [OdinSerialize, ProperName("Fully Willing Threshold")]
    [Description("Anyone with a prey willingness of at least this value will always be willing when eaten (Previous default was .7)")]
    public float WillingThreshold = .7f;
    [OdinSerialize, ProperName("Vore Follows Orientation")]
    [Description("If enabled, people will only eat people that they are attracted to (I.e. straight females would only eat males.)")]
    internal bool VoreFollowsOrientiation = false;
    [OdinSerialize, ProperName("Willing offer Odds")]
    [FloatRange(0, 30)]
    [Description("Modifies the random chance for a willing prey to just spontaneously offer themselves to someone.  1 is the default as somewhat rare.")]
    internal float WillingOfferOdds = 1;
    [OdinSerialize, ProperName("Friendship Multiplier"), FloatRange(.1f, 4.0f)]
    [Description("How fast friendships increase.  Faster speeds reduce grinding, but may be seen as too fast.  The default is 1.  This only affects smaller changes, big changes like saving someone are unaffected.")]    
    internal float FriendshipMultiplier = 1;
    [OdinSerialize, ProperName("Romantic Multiplier"), FloatRange(.1f, 4.0f)]
    [Description("How fast romantic feelings increase.  Faster speeds reduce grinding, but may be seen as too fast.  The default is 2.")]
    internal float RomanticMultiplier = 2;
    [OdinSerialize, ProperName("Relationship Decay")]
    [Description("If active, friendship and relationship values will slowly decay over time, requiring regular interactions to keep the values up.")]
    internal bool RelationshipDecay = false;
    [OdinSerialize, ProperName("Nurse Station Resurrection")]
    [Description("If active, the nurse's office will serve as a resurrection point, bringing anyone who was digested back to life.  If off, digested people are gone.")]
    internal bool NursesActive = false;
    [OdinSerialize, ProperName("New People Arrive")]
    [Description("If active, new people will occasionally show up to replace eaten people")]
    internal bool NewArrivals = false;
    [OdinSerialize, ProperName("New Arrival Delay")]
    [Description("New people will arrive on average every this many turns (the actual number of turns will be 50-150% of this).")]
    [IntegerRange(3, 1000)]
    internal int NewArrivalAverageTime = 40;
    [OdinSerialize, ProperName("New Arrival Maximum")]
    [Description("New arrivals will only spawn if the world has less than this many people in it.  Note that the maximum is also limited by the number of dorm rooms")]
    [IntegerRange(4, 250)]
    internal int MaxPopulation = 100;
    [OdinSerialize, ProperName("New Arrival Type")]
    [Description("The type of new characters that will spawn - Random is just pure random and Saved will pick from the saved characters, and only them.  Templates will generate a semi-random character from your saved templates. Note that saved characters are eligible for being re-added as soon as they're gone.")]
    internal LoadCharacterType NewType = LoadCharacterType.Random;

    [OdinSerialize, ProperName("New Arrival Tag Specifier")]
    [Description("Only has an effect if New Arrival type is set to named.  Leave blank to pick from all saved characters, but you can also add tags, that work the same way as they do in the saved character screen, i.e. 'main' would get characters that use that in their tags.")]
    internal string NewTypeTag = "";

    [OdinSerialize, ProperName("Randomize Dorm Placement")]
    [Description("If enabled, characters will be placed in a random dorm room instead of the next dorm room internally.")]
    internal bool RandomizeDormPlacement = true;


    [OdinSerialize, ProperName("Escape Stun")]
    [Description("Causes predators to be stunned for a turn after prey forcibly escapes, to allow them to flee easier.")]
    public bool EscapeStun = false;

    /// <summary>
    /// Use CheckDigestion for most purposes
    /// </summary>
    [OdinSerialize, ProperName("Digestion Type")]
    [Description("The digestion types available to the player / AI.")]
    internal DigestType DigestType = DigestType.Both;

    [OdinSerialize, ProperName("Weight Gain Enabled")]
    [Description("Enables / Disables all weight gain (the next four options)")]
    public bool WeightGain = false;

    [OdinSerialize, ProperName("Body Weight Gain Rate")]
    [FloatRange(0, 10)]
    [Description("Affects the rate at which characters gain / lose body weight (They slowly lose weight while extremely hungry).  A value of 1 is based on 'realistic gain'.   0 disables this entirely.")]
    public float WeightGainBody = 0;

    [OdinSerialize, ProperName("Max Weight Gain Limit")]
    [IntegerRange(0, 4000)]
    [Description("Weight gain slows down in relation to this, and will never exceed this value.")]
    public int TheoreticalMaxWeight = 800;

    [OdinSerialize, ProperName("Height Gain Rate")]
    [FloatRange(0, 10)]
    [Description("Affects the rate at which characters gain height through vore.  0 disables this entirely.")]
    public float WeightGainHeight = 0;

    [OdinSerialize, ProperName("Max Height Gain Limit")]
    [IntegerRange(0, 300)]
    [Description("Height gain slows down in relation to this, and will never exceed this value.")]
    public int TheoreticalMaxHeight = 96;

    [OdinSerialize, ProperName("Boob Size Gain Rate")]
    [FloatRange(0, 10)]
    [Description("Affects the rate at which characters gain breast size through vore.  0 disables this entirely.")]
    public float WeightGainBoob = 0;
    
    [OdinSerialize, ProperName("Dick Size Gain Rate")]
    [FloatRange(0, 10)]
    [Description("Affects the rate at which characters gain dick size through vore.  0 disables this entirely.")]
    public float WeightGainDick = 0;

    [OdinSerialize, ProperName("Hunger Rate")]
    [Description("Change how fast characters get hungry.  High values mean characters get hungry faster.")]
    [FloatRange(.1f, 10)]
    internal float HungerRate = 1;

    [OdinSerialize, ProperName("Digestion Speed")]
    [Description("Change how fast digestion happens. A value of 1 gives digestion in about 17 turns on a full health target. Higher values mean less time.")]
    [FloatRange(.01f, 4)]
    internal float DigestionSpeed = 0.5f;

    [OdinSerialize, ProperName("Absorption Speed")]
    [Description("Change how fast absorption happens (the time from when a prey dies until they're gone). At the default settings absorption takes about 13 turns. Higher values mean less time.")]
    [FloatRange(.01f, 4)]
    internal float AbsorptionSpeed = 1;

    [OdinSerialize, ProperName("Endo Desired Length")]
    [Description("Controls how long characters prefer to hold their prey in endosoma mode.  The pred will start wanting to release the prey on their own after this many turns.  (This number is doubled for endo dominators)")]
    [IntegerRange(1, 1000)]
    internal int EndoPreferredTurns = 150;

    [OdinSerialize, ProperName("Endo Dominators Hold Forever")]
    [Description("If this is set, endo dominators will never release their prey voluntarily, prey can only struggle out, or be freed")]    
    internal bool EndoDominatorsHoldForever = false;

    [OdinSerialize, ProperName("Max Sight Range")]
    [Description("At longer than this distance, events won't be witnessed (show up the in the log), or trigger responses, note that the ai's 'discovered vore' interaction has its own max length set to 6 tiles.")]
    [IntegerRange(10, 100)]
    internal int MaxSightRange = 20;

    [OdinSerialize, ProperName("See Through Walls Enabled")]
    [Description("Whether events will be witnessed through walls (show up the in the log), or trigger responses, note that the ai's 'discovered vore' interaction has its own max length set to 6 tiles.")]
    internal bool SeeThroughWallsEnabled = false;

    [OdinSerialize, ProperName("Flexible Stats")]
    [Description("Whether or not personality and stats can change during the game. Some examples are people getting better at being a predator with experience, or gaining strength from digesting prey.  ")]
    internal bool FlexibleStats = true;
    [OdinSerialize, ProperName("Flexble Stat Decay Speed")]
    [Description("Affects how fast flexible stats fall over time, setting to 0 disables this entirely.   Note that stats don't decay if there is no relevant building to raise them, or if flexible stats is disabled.")]
    [FloatRange(0, 4)]
    internal float FlexibleStatDecaySpeed = 1;

    [OdinSerialize, ProperName("Predator Tier Also Restricts Endo")]
    [Description("Characters will also be incapable of using endo on higher tiers, i.e. there will be no vore actions at all against higher tiers.")]
    internal bool PredatorTierAlsoRestrictsEndo = true;


    [OdinSerialize, ProperName("Oral Vore Enabled")]
    [Description("Whether oral vore in general is enabled")]
    internal bool OralVoreEnabled = true;
    [OdinSerialize, ProperName("Oral Vore Difficulty")]
    [Description("Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this.")]
    [FloatRange(.25f, 4)]
    internal float OralVoreDifficulty = 1;
    [OdinSerialize, ProperName("Cock Vore Enabled")]
    [Description("Whether cock vore in general is enabled")]
    internal bool CockVoreEnabled = true;
    [OdinSerialize, ProperName("Cock Vore Difficulty")]
    [Description("Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this.")]
    [FloatRange(.25f, 4)]
    internal float CockVoreDifficulty = 1;
    [OdinSerialize, ProperName("Unbirth Enabled")]
    [Description("Whether unbirth in general is enabled")]
    internal bool UnbirthEnabled = true;
    [OdinSerialize, ProperName("Unbirth Difficulty")]
    [Description("Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this.")]
    [FloatRange(.25f, 4)]
    internal float UnbirthDifficulty = 1;
    [OdinSerialize, ProperName("Anal Vore Enabled")]    
    [Description("Whether anal vore in general is enabled")]
    internal bool AnalVoreEnabled = false;
    [OdinSerialize, ProperName("Anal Vore Difficulty")]
    [Description("Affects the relative difficulty of performing this action, higher values lower the odds of success.  It's recommended to only make minor tweaks to this.")]
    [FloatRange(.25f, 4)]
    internal float AnalVoreDifficulty = 1;
    [OdinSerialize, ProperName("Anal Vore goes directly to stomach.")]
    [Description("If disabled, anal voring someone will put them in a unique location, the bowels.  That has its own text, though it still shares capacity with the stomach.  People there will have the option to be moved to the stomach.  If enabled, anal vored prey just go directly to the stomach.")]
    internal bool AnalVoreGoesDirectlyToStomach = false;

    [OdinSerialize, ProperName("Endo is Only Romantic")]
    [Description("Whether Endo is considered a romantic option instead of just a Friendly option (Endo dominators ignore this)")]
    internal bool EndoisOnlyRomantic = true;

    [OdinSerialize, ProperName("Disposal Enabled")]
    [Description("Whether Disposal (scat) in general is enabled")]
    public bool DisposalEnabled = false;

    [OdinSerialize, ProperName("Cock Disposal Enabled")]
    [Description("Whether cock disposal in general is enabled")]
    public bool DisposalCockEnabled = false;

    [OdinSerialize, ProperName("Unbirth Disposal Enabled")]
    [Description("Whether unbirth disposal in general is enabled")]
    public bool DisposalUnbirthEnabled = false;

    [OdinSerialize, ProperName("Bones Disposal Enabled")]
    [Description("Whether bones can appear in disposal")]
    public bool DisposalBonesEnabled = false;

    [OdinSerialize, ProperName("Public Disposal Rate")]
    [Description("Affects how often the AI will do disposals in public compared to bathrooms.   0 means always bathrooms, 1 means never bathrooms.")]
    [FloatRange(0, 1)]
    internal float PublicDisposalRate = 0.2f;

    [OdinSerialize, ProperName("Disable AI - Ask to Digest")]
    [Description("Prevents the AI from using the ask to digest interactions.")]
    internal bool PreventAIAskToDigest = false;

    [OdinSerialize, ProperName("Disable AI - Ask to be Digested")]
    [Description("Prevents the AI from using the ask to be digested interactions.")]
    internal bool PreventAIAskToBeDigested = false;

    [OdinSerialize, ProperName("Allow Navelfuck")]
    [Description("Allows the Player / AI to use the navelfuck action during sex.")]
    internal bool EnableNavelfuck = true;

    [OdinSerialize, ProperName("Allow Rimjob")]
    [Description("Allows the Player / AI to use the rimjob action during sex.")]
    internal bool EnableRimjob = true;

    [OdinSerialize, ProperName("Allow Partner Kinky Sex Acts")]
    [Description("Allows a character to perform kinky sex actions (footjob, kiss armpit, etc) if their partner has the associated trait.")]
    internal bool PartnerKinkySex = true;

    [OdinSerialize, ProperName("Unbirth Lactation Enabled")]
    [Description("If a character has womb prey, enables the breastfeeding sex action and text describing lactation.")]
    public bool UnbirthLactation = true;

    [OdinSerialize, ProperName("Allow Dating Dorm Entry")]
    [Description("Allows characters who are dating to enter each other's dorm rooms freely.")]
    internal bool DatingEntry = true;

    [OdinSerialize, ProperName("Size Factor")]
    [Description("Affects the bonus/penalty of size differences.  0 disables size differences, 1 gives size difference a linear bonus and is fairly balanced, 2 gives a squared bonus and is fairly realistic, 3 gives an extreme cubic bonus.")]
    [FloatRange(.0f, 3)]
    internal float SizeFactor = 1;

    [OdinSerialize, ProperName("Magic Setting")]
    [Description("If Default, characters can use magic if it is enabled in their character settings. If Everyone, magic can be perfomed by all characters regardless of settings. If NoMagic, magic is disabled in this world.")]
    public MagicAvailability MagicType = MagicAvailability.Default;

    [OdinSerialize, ProperName("Fizzle Mana Consumption")]
    [Description("If disabled, failed spellcasting will not consume mana.")]
    public bool FizzleMana = true;

    [OdinSerialize, ProperName("Perma Size Change")]
    [Description("If enabled, size changing spells will last forever (or until they are counteracted with the opposing spell).")]
    public bool PermaSizeChange = false;

    [OdinSerialize, ProperName("Mana Regen Rate")]
    [Description("A multiplier for how quickly a character generates mana. High values mean faster mana generation. A value of 0 means mana will not regenerate naturally and must be gained from digestion.")]
    [FloatRange(0, 4)]
    internal float ManaRegenMod = 1f;

    [OdinSerialize, ProperName("Spell Duration")]
    [Description("A multiplier for how long spell effects last. High values mean longer lasting spell effects. Setting to 0 disables duration spells.")]
    [FloatRange(0, 4)]
    internal float SpellDurationMod = 1f;

    [OdinSerialize, ProperName("Size Change Modifier")]
    [Description("A multiplier for how effective the grow and shrink spells are. Higher values mean larger changes. Changing this mid-game may have unintended effects. Setting to 1 disables the grow/shrink spells.")]
    [FloatRange(1, 10)]
    internal float SizeChangeMod = 1.5f;

    [OdinSerialize, ProperName("Log All Text")]
    [Description("Causes the game to log all text to a file as it happens.  Note, these can take up a fair bit of space.  They're stored in the StreamingAssets/Logs Directory.")]
    internal bool LogAllText = false;
    [OdinSerialize, ProperName("Log File Name")]
    [Description("The file name the logs will be saved to (you don't need any extension)")]
    internal string LogFileName = "LogName";

    [OdinSerialize, VariableEditorIgnores]
    string version;


    internal bool CheckDigestion(bool player, DigestionAlias type)
    {
        if (type == DigestionAlias.Any)
            return true;
        if (player)
        {
            switch (type)
            {
                case DigestionAlias.CanVore:
                    if (DigestType == DigestType.Both || DigestType == DigestType.DigestionOnly || DigestType == DigestType.OnlyPlayerCanDigest)
                        return true;
                    break;
                case DigestionAlias.CanSwitch:                    
                    if (DigestType == DigestType.Both || DigestType == DigestType.OnlyPlayerCanDigest)
                        return true;
                    break;
                case DigestionAlias.CanEndo:
                    if (DigestType == DigestType.Both || DigestType == DigestType.EndoOnly || DigestType == DigestType.OnlyPlayerCanDigest)
                        return true;
                    break;
            }
        }
        else
        {
            switch (type)
            {
                case DigestionAlias.CanVore:
                    if (DigestType == DigestType.Both || DigestType == DigestType.DigestionOnly)
                        return true;
                    break;
                case DigestionAlias.CanSwitch:
                    if (DigestType == DigestType.Both)
                        return true;
                    break;
                case DigestionAlias.CanEndo:
                    if (DigestType == DigestType.Both || DigestType == DigestType.EndoOnly || DigestType == DigestType.OnlyPlayerCanDigest)
                        return true;
                    break;
            }
        }
        return false;
    }

    internal bool CheckDigestion(Person actor, DigestionAlias type)
    {
        return CheckDigestion(actor == State.World.ControlledPerson, type);
    }    

    internal void CheckUpdateVersion()
    {
        if (string.Compare(version, "7D") < 0)
        {
            VoreHuntingBias = 1;
            ForcedPredWillingness = 1;
            AskPredWillingness = 1;
            VoreAnger = 1;
            HelpPreyBias = 1;
            WillingThreshold = .7f;
        } 
        if (string.Compare(version, "7E") < 0)
        {
            PredConviction = .5f;
            EscapeRate = 1;
        }
        if (string.Compare(version, "8") < 0)
        {
            DigestionBias = 1;
            EndoBias = 1;
            TheoreticalMaxWeight = 800;
            TheoreticalMaxHeight = 96;
        }
        if (string.Compare(version, "8C") < 0)
        {
            FreeingOdds = 1;
        } 
        if (string.Compare(version, "9B") < 0)
        {
            if (AbsorptionSpeed <= 0.06f)
                AbsorptionSpeed = DigestionSpeed;
            if (WillingOfferOdds == 0)
                WillingOfferOdds = 1;
        }

        version = State.Version;
    }

}
