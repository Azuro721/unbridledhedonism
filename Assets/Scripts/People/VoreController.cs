﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum VoreLocation
{
    Any,
    Stomach,
    Womb,
    Balls,
    Bowels
}

internal enum VoreType
{
    Oral,
    Unbirth,
    Cock,
    Anal
}


public class VoreController
{
    [OdinSerialize, VariableEditorIgnores]
    readonly Person Self;
    [OdinSerialize, VariableEditorIgnores]
    internal bool StomachDigestsPrey;
    [OdinSerialize, VariableEditorIgnores]
    internal bool WombAbsorbsPrey;
    [OdinSerialize, VariableEditorIgnores]
    internal bool BallsAbsorbPrey;
    [OdinSerialize, VariableEditorIgnores]
    internal bool BowelsDigestPrey;
    [OdinSerialize, ProperName("Total Digestions")]
    [IntegerRange(0, 9999)]
    [Description("The total number of people this character has digested.  It's largely just flavor text, aside from lowering how likely people are to try and rescue someone from you.")]
    internal int TotalDigestions;
    [OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> AllVoredTargets;
    [OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> StomachTargets;
    [OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> BowelTargets;
    [OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> WombTargets;
    [OdinSerialize, VariableEditorIgnores]
    List<VoreProgress> BallsTargets;
    [OdinSerialize, ProperName("Vore Capable")]
    [Description("Controls whether this character is capable of vore or not.  Disabling will disable all vore types.")]
    public bool GeneralVoreCapable;
    [OdinSerialize, ProperName("Oral Vore Capable")]
    [Description("Controls whether this character is capable of oral vore.")]
    public bool OralVoreCapable;
    [OdinSerialize, ProperName("Cock Vore Capable")]
    [Description("Controls whether this character is capable of cock vore.")]
    public bool CockVoreCapable;
    [OdinSerialize, ProperName("Unbirth Capable")]
    [Description("Controls whether this character is capable of unbirth.")]
    public bool UnbirthCapable;
    [OdinSerialize, ProperName("Anal Vore Capable")]
    [Description("Controls whether this character is capable of anal vore.")]
    public bool AnalVoreCapable;
    [OdinSerialize, ProperName("Vore Power")]
    [IntegerRange(-5, 5)]
    [Description("Positive values will boost vore and escape odds, negative lowers.  Is only manually set.  Can be used to simulate size differences, or whatever you want.  ")]
    internal int VorePower;

    public bool CapableOfVore()
    {
        if (GeneralVoreCapable == false)
            return false;
        if (Self.GenderType.VoreDisabled)
            return false;
        return true;
    }


    public VoreController(Person self, bool canVore)
    {
        Self = self;
        AllVoredTargets = new List<VoreProgress>();
        StomachTargets = new List<VoreProgress>();
        WombTargets = new List<VoreProgress>();
        BallsTargets = new List<VoreProgress>();
        BowelTargets = new List<VoreProgress>();
        GeneralVoreCapable = canVore;
        OralVoreCapable = true;
        CockVoreCapable = true;
        UnbirthCapable = true;
        AnalVoreCapable = true;
    }

    internal void CheckInitialization()
    {
        if (BowelTargets == null)
            BowelTargets = new List<VoreProgress>();
    }

    VoreLocation GetRespectiveStartingPoint(VoreType voreType)
    {
        switch (voreType)
        {
            case VoreType.Oral:
                return VoreLocation.Stomach;
            case VoreType.Unbirth:
                return VoreLocation.Womb;
            case VoreType.Cock:
                return VoreLocation.Balls;
            case VoreType.Anal:
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    return VoreLocation.Stomach;
                else
                    return VoreLocation.Bowels;
        }
        return VoreLocation.Stomach;
    }

    List<VoreProgress> GetRespectiveList(VoreLocation location)
    {
        switch (location)
        {
            case VoreLocation.Stomach:
                return StomachTargets;
            case VoreLocation.Womb:
                return WombTargets;
            case VoreLocation.Balls:
                return BallsTargets;
            case VoreLocation.Bowels:
                return BowelTargets;
            case VoreLocation.Any:
                return AllVoredTargets;
        }
        UnityEngine.Debug.LogWarning("Missing Part Reference!");
        return StomachTargets;
    }

    /// <summary>
    /// Returns true or false, throws an exception if the target isn't present.  
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool TargetIsBeingDigested(Person target)
    {
        var ret = GetProgressOf(target);

        if (target.Health <= 0)
            return true;
        if (target.DigestionImmune)
            return false;
        if (Self.Team != 0 && Self.Team == target.Team)
            return false;
        if (target.PredatorTier > Self.PredatorTier)
            return false;

        return PartCurrentlyDigests(ret.Location);
    }

    internal bool PartCurrentlyDigests(VoreType voreType) => PartCurrentlyDigests(GetRespectiveStartingPoint(voreType));

    /// <summary>
    /// Accounts for bowel redirection
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    internal bool PartCurrentlyDigests(VoreLocation location)
    {
        switch (location)
        {
            case VoreLocation.Stomach:
                return StomachDigestsPrey;
            case VoreLocation.Womb:
                return WombAbsorbsPrey;
            case VoreLocation.Balls:
                return BallsAbsorbPrey;
            case VoreLocation.Bowels:
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    return StomachDigestsPrey;
                return BowelsDigestPrey;
        }
        return false;
    }

    internal void SetPartDigest(VoreType voreType, bool active)
    {
        SetPartDigest(GetRespectiveStartingPoint(voreType), active);
    }

    internal void SetPartDigest(VoreLocation location, bool active)
    {
        if (State.World.Settings.CheckDigestion(Self, DigestionAlias.CanEndo) == false && active == false)
        {
            UnityEngine.Debug.Log("Tried to set a unit that can only digest to endo");
            active = true;
        }
        if (State.World.Settings.CheckDigestion(Self, DigestionAlias.CanVore) == false && active)
        {
            UnityEngine.Debug.Log("Tried to set a unit that can only endo to digest");
            active = false;
        }
        switch (location)
        {
            case VoreLocation.Stomach:
                StomachDigestsPrey = active;
                return;
            case VoreLocation.Womb:
                WombAbsorbsPrey = active;
                return;
            case VoreLocation.Balls:
                BallsAbsorbPrey = active;
                return;
            case VoreLocation.Bowels:
                BowelsDigestPrey = active;
                return;
        }
        UnityEngine.Debug.LogWarning("Missing Part Reference for setting digest!");
        return;
    }


    public bool IsDigesting()
    {
        foreach (var progress in AllVoredTargets)
        {
            if (TargetIsBeingDigested(progress.Target))
                return true;
        }
        return false;

    }

    public bool IsAbsorbing()
    {
        foreach (var progress in AllVoredTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }

    public bool IsAbsorbingBellyPrey()
    {
        foreach (var progress in StomachTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        foreach (var progress in BowelTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        foreach (var progress in WombTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }

    public bool IsAbsorbingStomachPrey()
    {
        foreach (var progress in StomachTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }

    public bool IsAbsorbingBowelsPrey()
    {
        foreach (var progress in BowelTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }
    public bool IsAbsorbingWombPrey()
    {
        foreach (var progress in WombTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }

    public bool IsAbsorbingBallsPrey()
    {
        foreach (var progress in BallsTargets)
        {
            if (progress.Target.Health < 0)
                return true;
        }
        return false;

    }



    internal void CheckIfShouldDigest(VoreLocation part = VoreLocation.Any)
    {
        if (State.World.Settings.CheckDigestion(false, DigestionAlias.CanSwitch) == false)
            return;
        if (part == VoreLocation.Any || part == VoreLocation.Stomach)
            CheckPart(VoreLocation.Stomach);
        if (part == VoreLocation.Any || part == VoreLocation.Balls)
            CheckPart(VoreLocation.Balls);
        if (part == VoreLocation.Any || part == VoreLocation.Womb)
            CheckPart(VoreLocation.Womb);
        if (part == VoreLocation.Any || part == VoreLocation.Bowels)
            CheckPart(VoreLocation.Bowels);



        void CheckPart(VoreLocation location)
        {
            if (PartCurrentlyDigests(location) == false && GetRespectiveList(location).Any())
            {
                if (GetRespectiveList(location).Where(s => (s.Target.Dead == false) && (s.ExpectingEndosoma || Self.AI.Desires.DesireToDigestTarget(s.Target) < 0.04f)).Any() == false)
                    SetPartDigest(location, true);
            }
        }

    }

    internal void MovePrey(VoreProgress progress, VoreLocation location)
    {
        var target = progress.Target;
        StomachTargets.RemoveAll(s => s.Target == target);
        WombTargets.RemoveAll(s => s.Target == target);
        BallsTargets.RemoveAll(s => s.Target == target);
        BowelTargets.RemoveAll(s => s.Target == target);
        GetRespectiveList(location).Add(progress);
        progress.Location = location;
        switch (location)
        {
            case VoreLocation.Stomach:
                progress.OriginalType = VoreType.Oral;
                break;
            case VoreLocation.Womb:
                progress.OriginalType = VoreType.Unbirth;
                break;
            case VoreLocation.Balls:
                progress.OriginalType = VoreType.Cock;
                break;
            case VoreLocation.Bowels:
                progress.OriginalType = VoreType.Anal;
                break;
        }
    }

    internal void AddPrey(VoreProgress progress, VoreType voreType)
    {
        VoreLocation location = GetRespectiveStartingPoint(voreType);
        GetRespectiveList(location).Add(progress);
        progress.Location = location;
        progress.OriginalType = voreType;
        State.World.EndOfTurnCallBacks.Add(() => progress.Target.EndStreamingActions());
        AllVoredTargets.Add(progress);
    }

    internal void AddPrey(VoreProgress progress, VoreLocation location)
    {
        VoreType assignedType = VoreType.Oral;
        switch (location)
        {
            case VoreLocation.Stomach:
                if (OralVoreCapable == false || State.World.Settings.OralVoreEnabled == false)
                    assignedType = VoreType.Anal;
                else
                    assignedType = VoreType.Oral;
                break;
            case VoreLocation.Womb:
                assignedType = VoreType.Unbirth;
                break;
            case VoreLocation.Balls:
                assignedType = VoreType.Cock;
                break;
            case VoreLocation.Bowels:
                assignedType = VoreType.Anal;
                break;
        }
        GetRespectiveList(location).Add(progress);
        progress.Location = location;
        progress.OriginalType = assignedType;
        State.World.EndOfTurnCallBacks.Add(() => progress.Target.EndStreamingActions());
        AllVoredTargets.Add(progress);
    }

    internal void RemovePrey(VoreProgress progress, VoreLocation location)
    {
        if (location == VoreLocation.Any)
        {
            StomachTargets.Remove(progress);
            WombTargets.Remove(progress);
            BallsTargets.Remove(progress);
            BowelTargets.Remove(progress);
        }
        else
            GetRespectiveList(location).Remove(progress);
        AllVoredTargets.Remove(progress);
    }

    internal void RemovePrey(Person person, VoreLocation location)
    {
        if (location == VoreLocation.Any)
        {
            StomachTargets.RemoveAll(s => s.Target == person);
            WombTargets.RemoveAll(s => s.Target == person);
            BallsTargets.RemoveAll(s => s.Target == person);
            BowelTargets.RemoveAll(s => s.Target == person);
        }
        else
            GetRespectiveList(location).RemoveAll(s => s.Target == person);
        AllVoredTargets.RemoveAll(s => s.Target == person);
    }
    internal List<VoreProgress> GetAllProgress(VoreLocation location) => GetRespectiveList(location).ToList();


    internal bool HasPrey(VoreLocation location) => GetRespectiveList(location).Any();

    internal bool HasPrey(VoreType voreType) => GetRespectiveList(GetRespectiveStartingPoint(voreType)).Any();

    internal bool HasBellyPrey()
    {
        if (GetRespectiveList(VoreLocation.Stomach).Any() || GetRespectiveList(VoreLocation.Womb).Any() || GetRespectiveList(VoreLocation.Bowels).Any())
            return true;
        else
            return false;
    }





    internal VoreProgress GetLiving(VoreLocation location) => GetRespectiveList(location).FirstOrDefault(s => s.IsAlive());

    internal bool ContainsPerson(Person target, VoreLocation location) => GetRespectiveList(location).Any(s => s.Target == target);

    public VoreProgress GetProgressOf(Person target) => AllVoredTargets.FirstOrDefault(s => s.Target == target);
    public VoreProgress GetProgressOf(Person target, VoreLocation location) => GetRespectiveList(location).FirstOrDefault(s => s.Target == target);

    public bool TargetIsInMyStomach(Person target) //Done this way because the interpreter can't handle enums
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Stomach;
    }
    public bool TargetIsInMyBalls(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Balls;
    }
    public bool TargetIsInMyWomb(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Womb;
    }
    public bool TargetIsInMyBowels(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.Location == VoreLocation.Bowels;
    }
    public bool TargetOralVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Oral;
    }
    public bool TargetUnbirthed(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Unbirth;
    }
    public bool TargetCockVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Cock;
    }
    public bool TargetAnalVored(Person target)
    {
        var progress = GetProgressOf(target);
        if (progress == null)
            return false;
        return progress.OriginalType == VoreType.Anal;
    }

    internal bool CouldVoreTarget(Person target, VoreType type, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return false;
        if (Self.VoreController.HasPrey(VoreLocation.Any) && Self.HasTrait(Quirks.LimitedCapacity))
            return false;
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (digestion != DigestionAlias.CanEndo || State.World.Settings.PredatorTierAlsoRestrictsEndo)
                return false;
        }
        if (digestion != DigestionAlias.CanEndo && Self.GetRelationshipWith(target).KnowledgeAbout.KnowsDigestionImmune)
            return false;
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return false;
        }
        if (State.World.Settings.VoreFollowsOrientiation && Self.Romance.DesiresGender(target.GenderType) == false)
            return false;
        bool can = false;
        VoreLocation location = VoreLocation.Stomach;
        switch (type)
        {
            case VoreType.Oral:
                can = OralVoreCapable && State.World.Settings.OralVoreEnabled;
                location = VoreLocation.Stomach;
                break;
            case VoreType.Unbirth:
                can = UnbirthCapable && State.World.Settings.UnbirthEnabled && Self.GenderType.HasVagina;
                location = VoreLocation.Womb;
                break;
            case VoreType.Cock:
                can = CockVoreCapable && State.World.Settings.CockVoreEnabled && Self.GenderType.HasDick;
                location = VoreLocation.Balls;
                break;
            case VoreType.Anal:
                can = AnalVoreCapable && State.World.Settings.AnalVoreEnabled;
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    location = VoreLocation.Stomach;
                else
                    location = VoreLocation.Bowels;
                break;
        }
        return HasRoomFor(target, location) && State.World.Settings.CheckDigestion(Self, digestion) && can;
    }

    internal bool TargetVoreImmune(Person target, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return true;
        if (State.World.Settings.VoreFollowsOrientiation && Self.Romance.DesiresGender(target.GenderType) == false)
            return true;
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (digestion != DigestionAlias.CanEndo || State.World.Settings.PredatorTierAlsoRestrictsEndo)
                return true;
        }
        if (digestion != DigestionAlias.CanEndo && Self.GetRelationshipWith(target).KnowledgeAbout.KnowsDigestionImmune)
            return true;
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return true;
        }
        return false;
    }

    internal string GetReason(Person target, VoreType type, DigestionAlias digestion)
    {
        if (target.VoreImmune)
            return "Target is Vore Immune";
        if (CapableOfVore() == false)
            return "You're playing as someone incapable of vore";
        if (State.World.Settings.VoreFollowsOrientiation && Self.Romance.DesiresGender(target.GenderType) == false)
            return "You have it set that vore is based on attraction, and your character is not interested in the target's gender.";
        if (target.PredatorTier > Self.PredatorTier)
        {
            if (digestion != DigestionAlias.CanEndo)
                return "That character has a higher vore tier and it's set to digestion.";
            if (State.World.Settings.PredatorTierAlsoRestrictsEndo)
                return "That character has a higher vore tier and it's set to endo with the option preventing higher tier endo enabled.";
        }
        if (Self.Team != 0 && Self.Team == target.Team && digestion != DigestionAlias.CanEndo)
        {
            return "You're on the same team as them and trying to digest them.";
        }
        bool can = false;
        VoreLocation location = VoreLocation.Stomach;
        switch (type)
        {
            case VoreType.Oral:
                can = OralVoreCapable && State.World.Settings.OralVoreEnabled;
                location = VoreLocation.Stomach;
                break;
            case VoreType.Unbirth:
                can = UnbirthCapable && State.World.Settings.UnbirthEnabled && Self.GenderType.HasVagina;
                location = VoreLocation.Womb;
                break;
            case VoreType.Cock:
                can = CockVoreCapable && State.World.Settings.CockVoreEnabled && Self.GenderType.HasDick;
                location = VoreLocation.Balls;
                break;
            case VoreType.Anal:
                can = AnalVoreCapable && State.World.Settings.AnalVoreEnabled;
                if (State.World.Settings.AnalVoreGoesDirectlyToStomach)
                    location = VoreLocation.Stomach;
                else
                    location = VoreLocation.Bowels;
                break;
        }
        if (HasRoomFor(target, location) == false)
            return "Not enough room in that location, or the prey is too big to fit.";

        if (can == false)
            return "Disabled by character / world setting, or character lacks the appropriate part";
        if (State.World.Settings.CheckDigestion(Self, digestion) == false)
            return "Can't use that digestion type on that target.";
        return "Couldn't find the reason";

    }

    /// <summary>
    /// Gets free space - Any returns the highest space of the 3, and excludes impossible types, specific types don't check for impossible types.  
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    internal float FreeSpace(VoreLocation location)
    {
        if (location == VoreLocation.Any)
        {
            float maxFree = 0;
            float space = Self.Boosts.Capacity * (1 + (Self.Personality.Voracity * 3f));
            if ((OralVoreCapable && State.World.Settings.OralVoreEnabled) || AnalVoreCapable && State.World.Settings.AnalVoreEnabled)
            {
                maxFree = Math.Max(maxFree, space - PreySize(VoreLocation.Stomach) - PreySize(VoreLocation.Bowels));
            }
            if (UnbirthCapable && State.World.Settings.UnbirthEnabled && Self.GenderType.HasVagina)
            {
                maxFree = Math.Max(maxFree, space - PreySize(VoreLocation.Womb));
            }
            if (CockVoreCapable && State.World.Settings.CockVoreEnabled && Self.GenderType.HasDick)
            {
                maxFree = Math.Max(maxFree, space - PreySize(VoreLocation.Balls));
            }
            return maxFree;
        }
        if (location == VoreLocation.Bowels || location == VoreLocation.Stomach)
            return Self.Boosts.Capacity * (1 + (Self.Personality.Voracity * 3f)) - PreySize(VoreLocation.Bowels) - PreySize(VoreLocation.Stomach);
        return Self.Boosts.Capacity * (1 + (Self.Personality.Voracity * 3f)) - PreySize(location);
    }
    
    internal bool HasRoomFor(Person target, VoreLocation location) => CapableOfVore() ? (FreeSpace(location) >= target.VoreController.Bulk()) : false;

    internal bool Swallowing(VoreLocation location) => GetRespectiveList(location).Any((s) => s.IsSwallowing());

    internal VoreProgress CurrentSwallow(VoreLocation location) => GetRespectiveList(location).FirstOrDefault((s) => s.IsSwallowing());

    /// <summary>
    /// Only living, non-swallowed prey
    /// </summary>
    /// <param name="prey"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    /// 
    public bool AreSisterPrey(Person prey, Person other, bool includeSwallowing = false)
    {
        if (prey.Dead || other.Dead)
            return false;
        var location = GetProgressOf(prey).Location;
        return GetRespectiveList(location).Where(s => s.Target == other && (includeSwallowing || s.IsSwallowing() == false)).Any();
    }

    internal void ClearData()
    {
        AllVoredTargets.Clear();
        BallsTargets.Clear();
        BowelTargets.Clear();
        StomachTargets.Clear();
        WombTargets.Clear();
    }

    internal List<VoreProgress> GetSisterPrey(Person prey, bool onlyLiving = true, bool includeBeingSwallowed = false)
    {
        List<VoreProgress> ret = new List<VoreProgress>();
        var location = GetProgressOf(prey).Location;
        ret.AddRange(GetRespectiveList(location));
        if (onlyLiving)
            ret.RemoveAll(s => s.Target.Dead);
        if (includeBeingSwallowed == false)
            ret.RemoveAll(s => s.IsSwallowing());
        ret.RemoveAll(s => s.Target == prey);
        return ret;

    }

    /// <summary>
    /// Get self size + size of all prey and sub-prey
    /// </summary>
    /// <returns></returns>
    internal float Bulk() => GetModifiedSize(Self) + PreySize();

    public float BellySize()
    {
        float bulk = 0;
        foreach (var digestion in StomachTargets.Concat(WombTargets).Concat(BowelTargets))
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize();
        }
        return bulk;
    }

    public float BallsSize()
    {
        float bulk = 0;
        foreach (var digestion in BallsTargets)
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize();
        }
        return bulk;
    }

    /// <summary>
    /// Includes all sub-prey
    /// </summary>
    /// <returns></returns>
    internal float PreySize(VoreLocation location = VoreLocation.Any, int depth = 0)
    {
        if (depth > 10)
        {
            UnityEngine.Debug.LogWarning("Depth exceeds 10, error likely");
            return 0;
        }
        float bulk = 0;
        foreach (var digestion in GetRespectiveList(location))
        {
            bulk += GetModifiedSize(digestion.Target);
            bulk += digestion.Target.VoreController.PreySize(depth: depth + 1);
        }
        return bulk;
    }

        /// <summary>
        /// Gets a person's current size based on their health
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        float GetModifiedSize(Person person)
    {
        var bulk = person.PartList.Height / Self.PartList.Height;
        if (person.Health < 0)
            return bulk * (1 - Math.Abs((float)person.Health / Constants.EndHealth));
        else
            return bulk;
    }

    internal void GetPredInfo(ref StringBuilder sb)
    {
        ProcessType(ref sb, VoreLocation.Stomach, "swallowing", "stomach");
        ProcessType(ref sb, VoreLocation.Womb, "unbirthing", "womb");
        ProcessType(ref sb, VoreLocation.Balls, "cock voring", "balls");
        ProcessType(ref sb, VoreLocation.Bowels, "anal voring", "anus");
        return;

        void ProcessType(ref StringBuilder sbb, VoreLocation type, string action, string location)
        {
            if (CurrentSwallow(type) != null)
                sbb.AppendLine($"Is currently {action} {CurrentSwallow(type).Target.FirstName}.");
            else
            {
                if (GetRespectiveList(type).Count() > 0)
                    sbb.AppendLine($"Has {ListPrey(type)} in their {location}.");
                return;
            }

            if (GetRespectiveList(type).Count() > 1)
            {
                sbb.AppendLine($"Has {ListPrey(type)} in their {location}.");
            }
        }

        string ListPrey(VoreLocation location)
        {
            return string.Join(" , ", GetRespectiveList(location).Select(s => s.Target.FirstName));
        }
    }

    //These are designed to be used by Flee
    public bool HasLivingBellyPrey() => GetLiving(VoreLocation.Stomach) != null || GetLiving(VoreLocation.Womb) != null;
    public bool HasLivingBallsPrey() => GetLiving(VoreLocation.Balls) != null;

    public bool HasAnyPrey() => HasPrey(VoreLocation.Any);
    public bool HasStomachPrey() => HasPrey(VoreLocation.Womb);
    public bool HasWombPrey() => HasPrey(VoreLocation.Womb);
    public bool HasBowelsPrey() => HasPrey(VoreLocation.Bowels);
    public bool HasBallsPrey() => HasPrey(VoreLocation.Balls);
    public bool IsDigestingAnyPrey() => PartCurrentlyDigests(VoreLocation.Any);
    public bool IsDigestingStomachPrey() => PartCurrentlyDigests(VoreLocation.Womb);
    public bool IsDigestingWombPrey() => PartCurrentlyDigests(VoreLocation.Womb);
    public bool IsDigestingBowelsPrey() => PartCurrentlyDigests(VoreLocation.Bowels);
    public bool IsDigestingBallsPrey() => PartCurrentlyDigests(VoreLocation.Balls);

    internal void AdvanceStages()
    {
        foreach (var digestion in AllVoredTargets.ToList())
        {
            digestion.AdvanceStage();
        }
        //if (Swallowing == false && Self.StreamingAction == InteractionType.Vore)
        //    Self.EndStreamingActions();
    }

    internal List<VoreProgress> GetAllSubPrey(int depth = 0)
    {
        depth++;
        if (depth > 150)
        {
            return new List<VoreProgress>();
        }
        List<VoreProgress> list = new List<VoreProgress>();
        foreach (var prey in AllVoredTargets)
        {
            list.AddRange(prey.Target.VoreController.GetAllSubPrey(depth));
        }
        list.AddRange(AllVoredTargets);
        return list;
    }
}
