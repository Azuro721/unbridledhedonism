﻿using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class DisposalData
{
    [OdinSerialize]
    internal Person Person;
    [OdinSerialize]
    internal int TurnCreated;
    [OdinSerialize]
    internal int TurnsDone;
    [OdinSerialize]
    internal VoreLocation Location;

    public DisposalData(Person person, int turnCreated, VoreLocation location)
    {
        Person = person;
        TurnCreated = turnCreated;
        Location = location;
    }
}
