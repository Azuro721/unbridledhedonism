﻿using Assets.Scripts.People;
using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class GenderList
{
    [OdinSerialize]
    internal int Version;

    [OdinSerialize]
    internal GenderType[] List;

    [OdinSerialize]
    internal int[,] OrientationWeights;

    internal string TextList()
    {
        string str = "";
        for (int i = 0; i < List.Length; i++)
        {
            str += $"{i} - {List[i].Name}\n";
        }
        return str;
    }

    public GenderList()
    {
        Standard();
        Version = 1;
    }

    internal void AllFemale()
    {
        List = new GenderType[6]
        {
            new GenderType("Male", false, false, true, false, false, PronounSet.He, Orientations.Male | Orientations.Androsexual | Orientations.ExclusiveBi, 0, false, "FF2020"),
            new GenderType("Female", true, true, false, true, true, PronounSet.She, Orientations.Female | Orientations.Gynesexual | Orientations.ExclusiveBi, 200, false, "2020FF"),
            new GenderType("Male Herm", false, true, true, false, false, PronounSet.They, Orientations.Androsexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
            new GenderType("Female Herm", true, true, true, true, true, PronounSet.They, Orientations.Gynesexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
            new GenderType("Cunt Boy", false, true, false, false, false, PronounSet.He, Orientations.Androsexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
            new GenderType("Dick Girl", true, false, true, true, true, PronounSet.She, Orientations.Gynesexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
        };

        OrientationWeights = new int[6, 8]
        {
            { 2,200,0,20,0,20,0,0},
            { 2,200,0,0,0,0,0,0},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
        };
    }

    internal void AllMale()
    {
        List = new GenderType[6]
        {
            new GenderType("Male", false, false, true, false, false, PronounSet.He, Orientations.Male | Orientations.Androsexual | Orientations.ExclusiveBi, 200,  false,"FF2020"),
            new GenderType("Female", true, true, false, true, true, PronounSet.She, Orientations.Female | Orientations.Gynesexual | Orientations.ExclusiveBi, 0,  false,"2020FF"),
            new GenderType("Male Herm", false, true, true, false, false, PronounSet.They, Orientations.Androsexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
            new GenderType("Female Herm", true, true, true, true, true, PronounSet.They, Orientations.Gynesexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
            new GenderType("Cunt Boy", false, true, false, false, false, PronounSet.He, Orientations.Androsexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
            new GenderType("Dick Girl", true, false, true, true, true, PronounSet.She, Orientations.Gynesexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
        };

        OrientationWeights = new int[6, 8]
        {
            { 2,0,0,200,0,0,0,0},
            { 2,20,0,200,0,20,0,0},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
        };
    }

    internal void Standard()
    {
        List = new GenderType[6]
       {
            new GenderType("Male", false, false, true, false, false, PronounSet.He, Orientations.Male | Orientations.Androsexual | Orientations.ExclusiveBi, 200, false, "FF2020"),
            new GenderType("Female", true, true, false, true, true, PronounSet.She, Orientations.Female | Orientations.Gynesexual | Orientations.ExclusiveBi, 200, false, "2020FF"),
            new GenderType("Male Herm", false, true, true, false, false, PronounSet.They, Orientations.Androsexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
            new GenderType("Female Herm", true, true, true, true, true, PronounSet.They, Orientations.Gynesexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
            new GenderType("Cunt Boy", false, true, false, false, false, PronounSet.He, Orientations.Androsexual | Orientations.Skoliosexual, 0, false, "FF20FF"),
            new GenderType("Dick Girl", true, false, true, true, true, PronounSet.She, Orientations.Gynesexual | Orientations.Skoliosexual, 0,  false, "FF20FF"),
       };

        OrientationWeights = new int[6, 8]
        {
            { 2,200,0,20,0,20,0,0},
            { 2,20,0,200,0,20,0,0},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
        };
    }

    internal void CheckStatus()
    {
        if (Version == 0)
        {
            foreach (var list in List)
            {
                list.FeminineName = list.Feminine;
            }
            Version = 1;
        }
        if (List.Length != 6)
        {
            Standard();
        }
    }

    internal void Complex()
    {
        List = new GenderType[6]
       {
            new GenderType("Male", false, false, true, false, false, PronounSet.He, Orientations.Male | Orientations.Androsexual | Orientations.ExclusiveBi, 160, false, "FF2020"),
            new GenderType("Female", true, true, false, true, true, PronounSet.She, Orientations.Female | Orientations.Gynesexual | Orientations.ExclusiveBi, 160, false, "2020FF"),
            new GenderType("Male Herm", false, true, true, false, false, PronounSet.They, Orientations.Androsexual | Orientations.Skoliosexual, 20, false, "FF20FF"),
            new GenderType("Female Herm", true, true, true, true, true, PronounSet.They, Orientations.Gynesexual | Orientations.Skoliosexual, 20, false, "FF20FF"),
            new GenderType("Cunt Boy", false, true, false, false, false, PronounSet.He, Orientations.Androsexual | Orientations.Skoliosexual, 20, false, "FF20FF"),
            new GenderType("Dick Girl", true, false, true, true, true, PronounSet.She, Orientations.Gynesexual | Orientations.Skoliosexual, 20, false, "FF20FF"),
       };

        OrientationWeights = new int[6, 8]
        {
            { 2,10,160,10,40,10,10,20},
            { 2,10,40,10,160,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
            { 2,10,60,10,60,10,10,20},
        };
    }


    internal int GetRandomGender()
    {
        WeightedList<int> rand = new WeightedList<int>();
        for (int i = 0; i < 6; i++)
        {
            rand.Add(i, List[i].RandomWeight);
        }
        return rand.GetResult();

    }

    internal int GetRandomOrientation(int gender)
    {
        WeightedList<int> rand = new WeightedList<int>();
        for (int i = 0; i < 8; i++)
        {
            rand.Add(i, OrientationWeights[gender, i]);
        }
        return rand.GetResult();
    }
}
