﻿using OdinSerializer;
using System;
using static HelperFunctions;

public class Needs
{
    [OdinSerialize]
    private Person Self;
    [OdinSerialize]
    private float _hunger;
    [OdinSerialize]
    private float _cleanliness;
    [OdinSerialize]
    private float _energy;
    [OdinSerialize]
    private float _horniness;

    public Needs(Person person)
    {
        _cleanliness = Rand.NextFloat(0, 0.4f);
        _hunger = Rand.NextFloat(0, 0.4f);
        _energy = Rand.NextFloat(0, 0.5f);
        _horniness = 0;
        Self = person;
    }

    [Description("A measure of the character's cleanliness.  The variable editor views these as I originally envisioned them, as needs.  A 0 need means there's no need")]
    [FloatRange(0, 2)]
    public float Cleanliness
    {
        get => _cleanliness;
        set { _cleanliness = value; if (_cleanliness < 0) _cleanliness = 0; }
    }

    public int CleanlinessScore(float minValue = .4f, int pointsPerUnit = 60) => (int)Math.Max(0, (Cleanliness - minValue) * pointsPerUnit);
    public float CleanlinessScoreFloat(float minValue = .4f, float pointsPerUnit = 60) => Math.Max(0, (Cleanliness - minValue) * pointsPerUnit);

    [Description("A measure of the character's hunger.  The variable editor views these as I originally envisioned them, as needs.  A 0 need means there's no need, 1 is starving")]
    [FloatRange(0, 2)]
    public float Hunger
    {
        get => _hunger;
        set { _hunger = value; if (_hunger < 0) _hunger = 0; }
    }
    [Description("A measure of the character's energy.  The variable editor views these as I originally envisioned them, as needs.  A 0 need means there's no need, 1 means pretty tired")]
    [FloatRange(0, 2)]
    public float Energy
    {
        get => _energy;
        private set { _energy = value; if (_energy < 0) _energy = 0; }
    }
    internal void ChangeEnergy(float change)
    {
        if (change > 0)
            Energy += change * Self.Boosts.EnergyUseRate;
        else
            Energy += change;
    }
    [Description("A measure of the character's horniness.  0 means not at all horny, 1 is maximum horniness")]
    [FloatRange(0, 1)]
    public float Horniness
    {
        get => _horniness;
        set { _horniness = value;
            if (_horniness < 0)
                _horniness = 0;
            if (_horniness > 1)
                SelfActionList.List[SelfActionType.Orgasm].OnDo(Self);
        }
    }

    public void Update()
    {
        if (Self.Dead)
            return;
        Cleanliness += 0.001f;
        Hunger += 0.002f * State.World.Settings.HungerRate * Self.Boosts.HungerRate;
        if (Hunger > .5f)
        {
            float rate = 2 * (Hunger - .5f);
            if (rate > 3)
                rate = 3;
            if (rate > Rand.NextFloat(0, 120))
                SelfActionList.List[SelfActionType.StomachGrowl].OnDo(Self);
        }
        if (Hunger >= 1f && Self.Health > Constants.HealthMax/2)
        {
            Self.Health -= Constants.HealthMax / 40;
        }
        ProcessWeightLoss();
        Energy += 0.001f;
        var horninessChange = Self.Personality.SexDrive * .004f - 0.002f;
        if (horninessChange < 0)
            Horniness += horninessChange;
        else if (Horniness < .85f) //Only passively increase if below this level
            Horniness += horninessChange;
        if (Self.BeingSwallowed)
            Horniness += Self.Personality.Voraphilia * .01f;
        else if (Self.BeingEaten)
            Horniness += Self.Personality.Voraphilia * .003f;
        if (Self.BeingEaten && Self.FindMyPredator().HasTrait(Quirks.AphrodisiacInsides))
            Horniness += .005f;
        if (Self.BeingEaten && Self.HasTrait(Traits.StockholmSyndrome))
        {
            IncreaseFriendship(Self, Self.FindMyPredator(), 0.002f);
            if (Self.Romance.CanSafelyRomance(Self.FindMyPredator()))
                IncreaseRomantic(Self, Self.FindMyPredator(), 0.004f);
        }

    }

    void ProcessWeightLoss()
    {
        if (State.World.Settings.WeightGainBody <= 0)
            return;
        if (Hunger > .9f && Self.PartList.Weight > HelperFunctions.HealthyWeight(Self))
        {
            Self.PartList.Weight -= .02f * State.World.Settings.WeightGainBody;
            Self.MiscStats.TotalWeightLoss += .02f * State.World.Settings.WeightGainBody;
            Hunger -= .001f * State.World.Settings.WeightGainBody;
        }
    }

}

