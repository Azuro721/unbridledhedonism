﻿using OdinSerializer;
using System;
using static HelperFunctions;

public class Magic
{
    [OdinSerialize]
    private Person Self;
    [OdinSerialize]
    private float _maxmana;
    [OdinSerialize]
    private float _mana;
    [OdinSerialize]
    private float _manaregen;
    [OdinSerialize]
    private float _potency;
    [OdinSerialize]
    private float _proficiency;
    [OdinSerialize]
    private float _willpower;
    [OdinSerialize]
    private int _duration_passdoor;
    [OdinSerialize]
    private int _duration_small1;
    [OdinSerialize]
    private int _duration_small2;
    [OdinSerialize]
    private int _duration_big1;
    [OdinSerialize]
    private int _duration_big2;
    [OdinSerialize]
    private int _duration_freeze;
    [OdinSerialize]
    private int _duration_hunger;

    public Magic(Person person)
    {
        _maxmana = 3;
        _mana = _maxmana;
        _manaregen = 1;
        _potency = Rand.NextFloat(0, 1f);
        _proficiency = Rand.NextFloat(0, 0.3f);
        _willpower = Rand.NextFloat(0, 0.3f);
        _duration_passdoor = 0;
        Self = person;
    }

    [OdinSerialize]
    [ProperName("Can Cast Spells")]
    [Description("Allows the character to cast spells and use magic. Can be overridden by world settings.")]
    public bool CanCast = false;

    [Description("The maximum mana this character can contain at once, not including trait bonuses.")]
    [FloatRange(0, 10)]
    public float MaxMana
    {
        get => _maxmana;
        set { _maxmana = value; if (_maxmana < 0) _maxmana = 0; }
    }

    [VariableEditorIgnores]
    [Description("The current mana this character has available for spellcasting.")]
    [FloatRange(0, 99)]
    public float Mana
    {
        get => _mana;
        set { _mana = value; if (_mana < 0) _mana = 0; if (_mana > (Self.Magic.MaxMana + Self.Boosts.MaxMana)) _mana = Self.Magic.MaxMana + Self.Boosts.MaxMana; }
    }

    [Description("How quickly the character generates mana.")]
    [FloatRange(0, 5)]
    public float ManaRegen
    {
        get => _manaregen;
        set { _manaregen = value; if (_manaregen < 0) _manaregen = 0; }
    }

    [Description("The potency of a character's spells. Affects duration and magnitude of healing/charm effects.")]
    [FloatRange(0, 1)]
    public float Potency
    {
        get => _potency;
        set { _potency = value; if (_potency < 0) _potency = 0; if (_potency > 1) _potency = 1; }
    }

    [Description("How adept a character is at casting spells. Boosted by studying, decays over time.")]
    [FloatRange(0, 1)]
    public float Proficiency
    {
        get => _proficiency;
        set { _proficiency = value; if (_proficiency < 0) _proficiency = 0; if (_proficiency > 1) _proficiency = 1; }
    }

    [Description("How adept a character is at resisting spells. Boosted by meditating, decays over time.")]
    [FloatRange(0, 1)]
    public float Willpower
    {
        get => _willpower;
        set { _willpower = value; if (_willpower < 0) _willpower = 0; if (_willpower > 1) _willpower = 1; }
    }

    // HACKY METHOD TO MAKE TEXTS WORK

    [VariableEditorIgnores]
    [OdinSerialize]
    [ProperName("Self Spell Succeeded")]
    [Description("A hacky way to make texts work for self-casts which can fail sometimes.")]
    public bool SelfCastSuccess = false;

    // DURATIONS

    [Description("How long the passwall effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Passdoor
    {
        get => _duration_passdoor;
        set { _duration_passdoor = value; if (_duration_passdoor < 0) _duration_passdoor = 0; }
    }
    [Description("How long the large effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Big1
    {
        get => _duration_big1;
        set { _duration_big1 = value; if (_duration_big1 < 0) _duration_big1 = 0; }
    }
    [Description("How long the giant effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Big2
    {
        get => _duration_big2;
        set { _duration_big2 = value; if (_duration_big2 < 0) _duration_big2 = 0; }
    }
    [Description("How long the small effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Small1
    {
        get => _duration_small1;
        set { _duration_small1 = value; if (_duration_small1 < 0) _duration_small1 = 0; }
    }
    [Description("How long the tiny effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Small2
    {
        get => _duration_small2;
        set { _duration_small2 = value; if (_duration_small2 < 0) _duration_small2 = 0; }
    }
    [Description("How long the freeze effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Freeze
    {
        get => _duration_freeze;
        set { _duration_freeze = value; if (_duration_freeze < 0) _duration_freeze = 0; }
    }
    [Description("How long the unnatural hunger effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Hunger
    {
        get => _duration_hunger;
        set { _duration_hunger = value; if (_duration_hunger < 0) _duration_hunger = 0; }
    }

    public void Update()
    {
        if (Self.Dead)
            return;
        
        if (State.World.Settings.FlexibleStats) 
        {
            Proficiency -= State.World.Settings.FlexibleStatDecaySpeed * 0.0001f;
            Willpower -= State.World.Settings.FlexibleStatDecaySpeed * 0.0001f;
        }

        if (CanUseMagic(State.World.ControlledPerson))
        {
            float boost = 0.01f;
            if (Self.VoreController.HasAnyPrey())
            {
                boost += 0.03f;
                if (Self.VoreController.IsDigestingAnyPrey())
                    boost += 0.03f;
            }
            
            if (Self.BeingEaten == false)
                Self.Magic.Mana += (boost * State.World.Settings.ManaRegenMod * (Self.Magic.ManaRegen * Self.Boosts.ManaRegen));
            
            if (Self.VoreController.IsAbsorbing())
                Self.Magic.Mana += 0.2f * State.World.Settings.AbsorptionSpeed;
        }

        // RESET HEIGHT & WEIGHT
        if (Self.Magic.Duration_Small2 == 1 || Self.Magic.Duration_Small1 == 1)
        {
            Self.PartList.Height *= State.World.Settings.SizeChangeMod;
            Self.PartList.Weight *= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.ResetShrink].OnDo(Self);
        }
        if (Self.Magic.Duration_Big2 == 1 || Self.Magic.Duration_Big1 == 1)
        {
            Self.PartList.Height /= State.World.Settings.SizeChangeMod;
            Self.PartList.Weight /= State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod * State.World.Settings.SizeChangeMod;
            SelfActionList.List[SelfActionType.ResetGrow].OnDo(Self);
        }

        // REDUCE DURATIONS

        Self.Magic.Duration_Passdoor -= 1;
        Self.Magic.Duration_Freeze -= 1;
        Self.Magic.Duration_Hunger -= 1;

        if (State.World.Settings.PermaSizeChange == false)
        {
            if (Self.Magic.Duration_Big1 > 0 && Self.Magic.Duration_Big2 > 0)
                Self.Magic.Duration_Big2 -= 1;
            else
                Self.Magic.Duration_Big1 -= 1;

            if (Self.Magic.Duration_Small1 > 0 && Self.Magic.Duration_Small2 > 0)
                Self.Magic.Duration_Small2 -= 1;
            else
                Self.Magic.Duration_Small1 -= 1;
        }
    }

}

