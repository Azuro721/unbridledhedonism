﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


static class PersonUpdater
{
    internal static void Update(string oldVersion, Person person, bool clearToPurify)
    {
        if (person.Personality == null)
            person.Personality = new Personality();
        if (person.VoreTracking == null)
            person.VoreTracking = new List<VoreTrackingRecord>();
        if (string.Compare(oldVersion, "7") < 0)
        {
            person.Personality.OralVoreInterest = 1;
            person.Personality.AnalVoreInterest = 1;
            person.Personality.CockVoreInterest = 1;
            person.Personality.UnbirthInterest = 1;
            person.Personality.Extroversion = Rand.NextFloat(0, 1);
            person.Quirks = new List<Quirks>();
            person.Traits = new List<Traits>();
        }
        if (string.Compare(oldVersion, "8") < 0)
        {
            person.Personality.PreyDigestionInterest = Rand.NextFloat(0, 1);
            if (clearToPurify)
            {
                person.Purify();
            }
        }
        if (person.MiscStats == null)
            person.MiscStats = new Assets.Scripts.People.MiscStats();
        if (person.Magic == null)
            person.Magic = new Magic(person);
    }

    internal static void Update(string oldVersion, Personality personality)
    {
        if (string.Compare(oldVersion, "7") < 0)
        {
            personality.OralVoreInterest = 1;
            personality.AnalVoreInterest = 1;
            personality.CockVoreInterest = 1;
            personality.UnbirthInterest = 1;
            personality.Extroversion = Rand.NextFloat(0, 1);
        }

        if (string.Compare(oldVersion, "7D") < 0 && personality.OralVoreInterest == 0 && personality.AnalVoreInterest == 0 && personality.CockVoreInterest == 0 && personality.UnbirthInterest == 0)
        {
            personality.OralVoreInterest = 1;
            personality.AnalVoreInterest = 1;
            personality.CockVoreInterest = 1;
            personality.UnbirthInterest = 1;
            personality.Extroversion = Rand.NextFloat(0, 1);
        }

        if (string.Compare(oldVersion, "8") < 0)
        {
            personality.PreyDigestionInterest = Rand.NextFloat(0, 1);

        }

    }


}
