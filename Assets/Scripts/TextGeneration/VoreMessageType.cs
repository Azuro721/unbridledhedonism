﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum VoreMessageType
{
    OralBegin,
    UnbirthBegin,
    CockVoreBegin,
    AnalVoreBegin,
    InStomach,
    InStomachEndo,
    InWomb,
    InWombEndo,
    InBalls,
    InBallsEndo,
    InAnus,
    InAnusEndo,
    VoreHunger,
    VoreClothing,
    VoreCleanliness,
    VoreAfterSex,
    OralVoreRefused,
    CockVoreRefused,
    UnbirthRefused,
    AnalVoreRefused,

}
