﻿public enum BodyPartType
{
    Head,
    Shoulder,
    Torso,
    Waist,
    Leg,
    Feet
}

