﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.TextGeneration
{
    static class BodyPartLinker
    {
        internal static BodyPartDescriptionType GetType( BodyPartType part, BodyPartDescription description)
        {
            return (BodyPartDescriptionType)Enum.Parse(typeof(BodyPartDescriptionType), $"{part}{description}");
        }
    }
}
