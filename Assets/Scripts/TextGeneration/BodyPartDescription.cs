﻿namespace Assets.Scripts.TextGeneration
{
    public enum BodyPartDescription
    {
        Description,
        OralVore,
        Unbirth,
        CockVore,
        AnalVore,
    }
}

