﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.TextGeneration
{
    public enum BodyPartDescriptionType
    {
        HeadDescription,
        HeadOralVore,
        HeadUnbirth,
        HeadCockVore,
        HeadAnalVore,
        ShoulderDescription,
        ShoulderOralVore,
        ShoulderUnbirth,
        ShoulderCockVore,
        ShoulderAnalVore,
        TorsoDescription,
        TorsoOralVore,
        TorsoUnbirth,
        TorsoCockVore,
        TorsoAnalVore,
        WaistDescription,
        WaistOralVore,
        WaistUnbirth,
        WaistCockVore,
        WaistAnalVore,
        LegDescription,
        LegOralVore,
        LegUnbirth,
        LegCockVore,
        LegAnalVore,
        FeetDescription,
        FeetOralVore,
        FeetUnbirth,
        FeetCockVore,
        FeetAnalVore,
        BellySizeDescription,
        BallsSizeDescription
    }

}