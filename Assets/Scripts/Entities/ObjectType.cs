﻿using System.Collections;
using System.Collections.Generic;

public enum ObjectType
{
    None,
    Bed,
    Clothes,
    Food,
    Shower,
    Bathroom,
    NurseOffice,
    Gym,
    Library,
}