﻿using Assets.Scripts.Race.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OdinSerializer;


public class PartList
{
    [OdinSerialize]
    [VariableEditorIgnores]
    public string Race { get; set; } = "Human";
   
    private List<Part> parts;

    internal List<Part> Parts
    {
        get
        {
            if (parts == null)
                parts = Create.GetParts();
            return parts; 
        }
        private set => parts = value;
    }

    [OdinSerialize]
    [FloatRange(2, 108)]
    [Description("Height in inches.")]
    public float Height { get; set; }
    [OdinSerialize]
    [FloatRange(10, 800)]
    [Description("Weight in pounds.")]
    public float Weight { get; set; }
    //[OdinSerialize]
    //public string SkinColor { get; set; }

    [OdinSerialize, ProperName("Hair Color")]
    public string HairColor { get; set; }
    [OdinSerialize, ProperName("Hair Length")]
    public string HairLength { get; set; }
    [OdinSerialize, ProperName("Hair Style")]
    public string HairStyle { get; set; }
    [OdinSerialize, ProperName("Eye Color")]
    public string EyeColor { get; set; }

    [OdinSerialize, ProperName("Shoulder Description")]
    public string ShoulderDescription { get; set; }

    [OdinSerialize, ProperName("Breast Size")]
    [FloatRange(0, 10)]
    [Description("Size of the character's breasts - ignored if they're not present.")]
    public float BreastSize { get; set; }

    [OdinSerialize, ProperName("Waist Description")]
    public string HipDescription { get; set; }

    [OdinSerialize, ProperName("Dick Size")]
    [FloatRange(0, 5)]
    [Description("Size of the character's dick - ignored if it is not present.")]
    public float DickSize { get; set; }
    [OdinSerialize, ProperName("Ball Size")]
    [FloatRange(0, 5)]
    [Description("Size of the character's balls - ignored if they're not present.")]
    public float BallSize { get; set; }

    [OdinSerialize]
    internal Dictionary<string, string> Tags = new Dictionary<string, string>();



    internal void GetDescription(Person person, ref StringBuilder sb)
    {
        sb.AppendLine($" ");
        sb.AppendLine($"<b>Description:</b>");
        sb.AppendLine($"Height: {MiscUtilities.FancyHeight(Height)} Weight: {MiscUtilities.ConvertedWeight(Weight)}");
        foreach (var part in Parts)
        {
            var desc = part.GetDescription(person);
            if (desc != "")
                sb.AppendLine(desc);
        }

    }


}

